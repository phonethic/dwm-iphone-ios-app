//
//  DMDSongDetailObj.h
//  DancewithMD
//
//  Created by Rishi on 12/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDSongDetailObj : NSObject {
    
}
@property (nonatomic, copy) NSString *songId;
@property (nonatomic, copy) NSString *songName;
@property (nonatomic, copy) NSString *songClip_Part_ID;
@property (nonatomic, copy) NSString *songSequence;
@property (nonatomic, copy) NSString *songShare_ID;
@property (nonatomic, copy) NSString *songclip_Title;
@end
