//
//  DMDDisclaimerViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMDDisclaimerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
