//
//  DMDLearnDanceMainViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 21/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>

#import "DMDLearnDanceMainViewController.h"

#import "DMDAppDelegate.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDSongCategoryObj.h"
#import "DMDLearnDanceViewController.h"
#import "CommonCallBack.h"
#import "PlayVideoViewController.h"

#define SONG_CATEGORY_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/category",LIVE_DWM_SERVER]

@interface DMDLearnDanceMainViewController ()

@end

@implementation DMDLearnDanceMainViewController
@synthesize songCategoryTableView;
@synthesize songCategoryArray;
@synthesize moviePlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_LearnDance"];
    [[LocalyticsSession shared] tagScreen:@"DanceStyles"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];

    if([DMD_APP_DELEGATE networkavailable])
    {
        [self categoryListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    if (categorylistConnection != nil) {
        [categorylistConnection cancel];
        categorylistConnection = nil;
    }
    if(self.moviePlayer != nil)
    {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}
- (void)viewDidUnload {
    [self setSongCategoryTableView:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}


#pragma Internal Callbacks
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (categorylistConnection != nil) {
                             [categorylistConnection cancel];
                             categorylistConnection = nil;
                         }
                     }];
    
}

-(void)categoryListAsynchronousCall
{
    [self showProgressHudWithCancel:@"Songs" subtitle:@"Loading"];
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_CATEGORY_LIST_LINK] cachePolicy:NO timeoutInterval:15.0];
	categorylistConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)playThumnailVideo:(UITableViewCell *)cell index:(NSIndexPath *)lindexPath
{
    DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:lindexPath.row];
    //DebugLog(@"LearnDanceMainView: VIDEO URL=%@",tempsongObj.videoLink);
    
    PlayVideoViewController *playVideoController = [[PlayVideoViewController alloc] initWithNibName:@"PlayVideoViewController" bundle:nil];
    playVideoController.urlString = [DMD_APP_DELEGATE URLEncode:tempCategObj.styleVideoLink];
    [self.navigationController pushViewController:playVideoController animated:YES];
    return;
    
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[DMD_APP_DELEGATE URLEncode:tempCategObj.styleVideoLink]]];
    self.moviePlayer.view.frame = CGRectMake(10, 3, 97, 89);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    //working
    moviePlayer.view.tag = 420;
    [cell.contentView addSubview:moviePlayer.view];
    [cell bringSubviewToFront:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

-(void)stopVideo
{
    if(self.moviePlayer != nil) {
        close = 1;
        [moviePlayer stop];
        //DebugLog(@"LearnDanceMainView: close=%d",close);
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //DebugLog(@"LearnDanceMainView: moviePlayBackDidFinish close=%d",close);
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    //DebugLog(@"LearnDanceMainView: moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    //DebugLog(@"LearnDanceMainView: moviePlayExitFullScreen");
    fullscreen = 0;
    [moviePlayer play];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"LearnDanceMainView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    if (connection == categorylistConnection) {
        if(responseAsyncData != nil)
        {
            NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            DebugLog(@"LearnDanceMainView: \n result:%@\n\n", result);
            NSError* error;
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"LearnDanceMainView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (songCategoryArray == nil) {
                        songCategoryArray = [[NSMutableArray alloc] init];
                    }else{
                        [songCategoryArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray) {
                        songCatObj = [[DMDSongCategoryObj alloc] init];
                        songCatObj.styleId      = [songattributeDict objectForKey:@"Style_ID"];
                        songCatObj.styleName    = [songattributeDict objectForKey:@"Style_Name"];
                        songCatObj.styleThumbnail   = [songattributeDict objectForKey:@"Thumbnail"];
                        songCatObj.styleVideoLink   = [songattributeDict objectForKey:@"Video_Link"];
                        songCatObj.styleImageLink   = [songattributeDict objectForKey:@"ImageLink"];
                        songCatObj.styleCategory    = [songattributeDict objectForKey:@"Category"];
                        //                    DebugLog(@"LearnDanceMainView: styleId: %@ , styleName: %@  , styleThumbnail: %@  , styleVideoLink: %@  , styleImageLink: %@  , styleCategory: %@\n", songCatObj.styleId,songCatObj.styleName,songCatObj.styleThumbnail,songCatObj.styleVideoLink,songCatObj.styleImageLink,songCatObj.styleCategory);
                        if(![[songattributeDict objectForKey:@"Style_ID"] isEqualToString:@"5"])
                        {
                            [songCategoryArray addObject:songCatObj];
                        }
                        songCatObj = nil;
                    }
                }
            }
        }
        categorylistConnection = nil;
        responseAsyncData = nil;
        DebugLog(@"LearnDanceMainView: array count: %d", [songCategoryArray count]);
        [songCategoryTableView reloadData];
    }
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 97;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [songCategoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIImageView *thumbImg;
    UILabel *lblCatgName;
    static NSString *CellIdentifier = @"CatgCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 97, 89)];
        thumbImg.tag = 1;
        thumbImg.userInteractionEnabled = TRUE;
        thumbImg.multipleTouchEnabled = TRUE;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        [cell.contentView addSubview:thumbImg];
        
        lblCatgName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+57,30,140,40)];
        lblCatgName.text = @"lblCatgName";
        lblCatgName.font = KABEL_FONT(20);
        lblCatgName.adjustsFontSizeToFitWidth = YES;
        lblCatgName.tag = 2;
        lblCatgName.textAlignment = UITextAlignmentLeft;
        lblCatgName.textColor = [UIColor whiteColor];
        lblCatgName.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblCatgName];
        
        UIButton *videobutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [videobutton setFrame:CGRectMake(10, 3, 97, 89)];
        [videobutton setBackgroundColor:[UIColor clearColor]];
        videobutton.tag = 100;
        [videobutton addTarget:self action:@selector(songIntroBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:videobutton];
        
        UIImageView *videoImg = [[UIImageView alloc] init];
        videoImg.image = [UIImage imageNamed:@"playbutton.png"];
        videoImg.contentMode = UIViewContentModeScaleAspectFit;
        videoImg.tag = 3;
        [videoImg setHidden:TRUE];
        videoImg.frame = CGRectMake(40.0,18,50.0,50.0);
        [cell.contentView addSubview:videoImg];
        
        
        UIImageView *arrowImg = [[UIImageView alloc] init];
        arrowImg.image = [UIImage imageNamed:@"arrow.png"];
        arrowImg.contentMode = UIViewContentModeScaleAspectFit;
        arrowImg.tag = 5;
        arrowImg.frame = CGRectMake(295,44,15,17);
        [cell.contentView addSubview:arrowImg];

    }
    DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:indexPath.row];
    thumbImg = (UIImageView *)[cell viewWithTag:1];
    lblCatgName = (UILabel *)[cell viewWithTag:2];
    lblCatgName.text =  tempCategObj.styleName;
    //DebugLog(@"LearnDanceMainView: %@ %@",tempCategObj.styleThumbnail,[DMDDelegate URLEncode:tempCategObj.styleThumbnail]);
    [thumbImg setImageWithURL:[NSURL URLWithString:[DMD_APP_DELEGATE URLEncode:tempCategObj.styleThumbnail]]
             placeholderImage:nil
                      success:^(UIImage *image) {
                          //DebugLog(@"LearnDanceMainView: success");
                      }
                      failure:^(NSError *error) {
                          DebugLog(@"LearnDanceMainView: write error %@", error);
                      }];
    
    UIImageView *playImgview = (UIImageView *)[cell viewWithTag:3];
    if(tempCategObj.styleVideoLink == nil || [tempCategObj.styleVideoLink isEqualToString:@""])
    {
        [playImgview setHidden:TRUE];
    } else {
        [playImgview setHidden:FALSE];
    }
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
    bg.image = [UIImage imageNamed:@"tab_off.png"];
    bgon.image = [UIImage imageNamed:@"tab_on.png"];
    cell.backgroundView = bg;
    cell.selectedBackgroundView = bgon;
    

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:indexPath.row];
    DMDLearnDanceViewController *categdetailViewController = [[DMDLearnDanceViewController alloc] initWithNibName:@"DMDLearnDanceViewController" bundle:nil] ;
    categdetailViewController.selectedStyleID = tempCategObj.styleId;
    //DebugLog(@"LearnDanceMainView: selectedStyleID %@",tempCategObj.styleId);
    categdetailViewController.title = tempCategObj.styleName;
    categdetailViewController.selectedStyleName = tempCategObj.styleName;
    [self.navigationController pushViewController:categdetailViewController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
    [self stopVideo];
}

#pragma UIButton delegate Methods
- (void)songIntroBtnTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.songCategoryTableView];
	NSIndexPath *indexPath = [self.songCategoryTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.songCategoryTableView cellForRowAtIndexPath:indexPath];
        DMDSongCategoryObj *tempCategObj  = [songCategoryArray objectAtIndex:indexPath.row];
        //DebugLog(@"LearnDanceMainView: ----IS VIDEO--->%@",tempCategObj.styleVideoLink);
        if(tempCategObj.styleVideoLink == nil || [tempCategObj.styleVideoLink isEqualToString:@""])
        {
            //DebugLog(@"LearnDanceMainView: ----IS VIDEO--->%@",tempsongObj.isVideo);
        } else {
            [self playThumnailVideo:cell index:indexPath];
        }
    }
}
@end
