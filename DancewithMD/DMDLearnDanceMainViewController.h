//
//  DMDLearnDanceMainViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 21/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class DMDSongCategoryObj;
@interface DMDLearnDanceMainViewController : UIViewController{
    
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    int status;
    
    NSMutableData *responseAsyncData;
    DMDSongCategoryObj *songCatObj;
    NSURLConnection *categorylistConnection;
    
    int close;
    int fullscreen;
}
@property (strong, nonatomic) NSMutableArray *songCategoryArray;
@property (strong, nonatomic) IBOutlet UITableView *songCategoryTableView;



@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;

@end
