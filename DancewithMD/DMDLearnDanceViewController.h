//
//  DMDLearnDanceViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class DMDSongObj;

@interface DMDLearnDanceViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSMutableData *responseAsyncData;
    
    DMDSongObj *songObj;
    
    NSURLConnection *songsListConnection;
    int status;

    int close;
    int fullscreen;
}
@property (nonatomic, copy) NSString *selectedStyleID;
@property (nonatomic, copy) NSString *selectedStyleName;
@property (strong, nonatomic) NSMutableArray *songArray;
@property (nonatomic, strong) NSIndexPath *openCellIndexPath;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;

@property (strong, nonatomic) IBOutlet UITableView *songsTableView;
@end
