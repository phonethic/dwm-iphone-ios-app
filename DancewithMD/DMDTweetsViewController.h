//
//  DMDTweetsViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDTweetObj;
@interface DMDTweetsViewController : UIViewController
{
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *connection1;
    NSURLConnection *connection2;
    DMDTweetObj *tweetObj;
}
@property (nonatomic, copy) NSString *dataFilePath;
@property (nonatomic, copy) NSString *bearerToken;
@property (strong, nonatomic) IBOutlet UITableView *tweetsTableView;
@property (strong, nonatomic) NSMutableArray *tweetsArray;
@end
