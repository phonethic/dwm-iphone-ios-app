//
//  DMDLeaderboardViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 07/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLeaderboardViewController.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDLeaderboardObj.h"
#import "CommonCallBack.h"

#define BOARD_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/leaderboard",LIVE_DWM_SERVER]

@interface DMDLeaderboardViewController ()

@end

@implementation DMDLeaderboardViewController
@synthesize dancersString,rankString,pointsString;
@synthesize dancerslbl,ranklbl,pointslbl;
@synthesize userNamelbl;
@synthesize leaderboardArray;
@synthesize leaderBoardTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_LeaderBoard"];
    [[LocalyticsSession shared] tagScreen:@"LeaderBoard"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];

    if([DMD_APP_DELEGATE networkavailable])
    {
        [self leaderboardListAsynchronousCall];

    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
        
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    if (leaderBoardConnection != nil) {
        [leaderBoardConnection cancel];
        leaderBoardConnection = nil;
    }
}
- (void)viewDidUnload {
    [self setUserNamelbl:nil];
    [self setDancerslbl:nil];
    [self setRanklbl:nil];
    [self setPointslbl:nil];
    [self setLeaderBoardTableView:nil];
    [self setDancerslblBackImageView:nil];
    [self setRanklblBackImageView:nil];
    [self setPointslblBackImageView:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}



#pragma Internal Callbacks
-(void)setUI{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:LOGIN_USERNAME];
    userNamelbl.text = [DMD_APP_DELEGATE doCapitalFirstLetter:userName];
    userNamelbl.numberOfLines = 2;

    ranklbl.font    = KABEL_FONT(18);
    pointslbl.font  = KABEL_FONT(18);
    dancerslbl.font = KABEL_FONT(18);
    
    userNamelbl.font= KABEL_FONT(20);

    ranklbl.textColor    = [UIColor whiteColor];
    pointslbl.textColor  = [UIColor whiteColor];
    dancerslbl.textColor = [UIColor whiteColor];
    
    userNamelbl.textColor= [UIColor whiteColor];

    dancerslbl.text     = dancersString;
    ranklbl.text        = rankString;
    pointslbl.text      = pointsString;
    
    leaderBoardTableView.backgroundColor = [UIColor clearColor];
}
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (leaderBoardConnection != nil) {
                             [leaderBoardConnection cancel];
                             leaderBoardConnection = nil;
                         }
                     }];
    
}
-(void)leaderboardListAsynchronousCall
{
    [self showProgressHudWithCancel:@"Leaderboard" subtitle:@"Loading"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:BOARD_LIST_LINK] cachePolicy:NO timeoutInterval:15.0];
    leaderBoardConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
        //DebugLog(@"LeaderBoardView: URL-%@-",connection.currentRequest.URL);
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"LeaderBoardView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if(connection==leaderBoardConnection)
    {
        //DebugLog(@"LeaderBoardView: con 1 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"LeaderBoardView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (leaderboardArray == nil) {
                        leaderboardArray = [[NSMutableArray alloc] init];
                    }else
                    {
                        [leaderboardArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        boardObj = [[DMDLeaderboardObj alloc] init];
                        boardObj.userId = [songattributeDict objectForKey:@"User_ID"];
                        boardObj.userRank = [songattributeDict objectForKey:@"rank"];
                        boardObj.userName = [songattributeDict objectForKey:@"User_Name"];
                        boardObj.userPoints = [songattributeDict objectForKey:@"points"];
                        ////DebugLog(@"LeaderBoardView: \n songid: %@ , name: %@  , clipid: %@  , sequence: %@  , shareid: %@ \n", songDetailObj.songId, songDetailObj.songName, songDetailObj.songClip_Part_ID, songDetailObj.songSequence, songDetailObj.songShare_ID);
                        [leaderboardArray addObject:boardObj];
                        boardObj = nil;
                    }
                    DebugLog(@"LeaderBoardView: detail array count: %d", [leaderboardArray count]);
                    [leaderBoardTableView reloadData];
                }
            }
        }
        leaderBoardConnection = nil;
        responseAsyncData = nil;
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 45;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,45)];
//    customView.backgroundColor = [UIColor blackColor];
//    
//    UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel1.backgroundColor = [UIColor clearColor];
//    headerLabel1.frame = CGRectMake(10.0,0,50.0,45.0);
//    headerLabel1.font = KABEL_FONT(18);
//    headerLabel1.text = @"Rank";
//    headerLabel1.textAlignment = UITextAlignmentCenter;
//    headerLabel1.textColor = [UIColor whiteColor];
//    [customView addSubview:headerLabel1];
//    
//    UILabel *headerLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel2.backgroundColor = [UIColor clearColor];
//    headerLabel2.frame = CGRectMake(CGRectGetMaxX(headerLabel1.frame)+10,0,160.0,45.0);
//    headerLabel2.font = KABEL_FONT(18);
//    headerLabel2.text = @"Name";
//    headerLabel2.textAlignment = UITextAlignmentCenter;
//    headerLabel2.textColor = [UIColor whiteColor];
//    [customView addSubview:headerLabel2];
//    
//    UILabel *headerLabel3 = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel3.backgroundColor = [UIColor clearColor];
//    headerLabel3.frame = CGRectMake(CGRectGetMaxX(headerLabel2.frame)+10,0,70,45.0);
//    headerLabel3.font = KABEL_FONT(18);
//    headerLabel3.text = @"Points";
//    headerLabel3.textAlignment = UITextAlignmentCenter;
//    headerLabel3.textColor = [UIColor whiteColor];
//    [customView addSubview:headerLabel3];
// 
//    return customView;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [leaderboardArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblRank;
    UILabel *lblName;
    UILabel *lblPoints;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.textLabel.textColor = [UIColor whiteColor];
        //        cell.textLabel.font = [UIFont systemFontOfSize:24.0];
        //        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        //        cell.detailTextLabel.font = [UIFont systemFontOfSize:20.0];
        
        //Initialize Label with tag 2.(Title Label)
        lblRank = [[UILabel alloc] initWithFrame:CGRectMake(10.0,0.0,50.0,50.0)];
        lblRank.tag = 1;
        lblRank.text = @"Rank";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblRank.font = KABEL_FONT(18);
        lblRank.textAlignment = UITextAlignmentCenter;
        lblRank.textColor = [UIColor whiteColor];
        lblRank.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblRank];
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblRank.frame)+10,0.0,158.0,50.0)];
        lblName.tag = 2;
        lblName.text = @"Name";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(18);
        lblName.adjustsFontSizeToFitWidth = YES;
        lblName.textAlignment = UITextAlignmentCenter;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblName];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblPoints = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblName.frame)+5,0.0,70,50.0)];
        lblPoints.tag = 3;
        lblPoints.text = @"Points";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblPoints.font = KABEL_FONT(18);
        lblPoints.adjustsFontSizeToFitWidth = YES;
        lblPoints.textAlignment = UITextAlignmentLeft;
        lblPoints.textColor = [UIColor whiteColor];
        lblPoints.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblPoints];
        
        UIImageView *seperatorImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, 320, 4)];
        seperatorImg.image = [UIImage imageNamed:@"leaderboard_table_line.png"];
        seperatorImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:seperatorImg];
    }
    
    cell.userInteractionEnabled = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    DMDLeaderboardObj *tempboardObj  = [leaderboardArray objectAtIndex:indexPath.row];
    lblRank = (UILabel *)[cell viewWithTag:1];
    lblName = (UILabel *)[cell viewWithTag:2];
    lblPoints = (UILabel *)[cell viewWithTag:3];
    lblRank.text = tempboardObj.userRank;
    lblName.text = tempboardObj.userName;
    lblPoints.text = tempboardObj.userPoints;
    return cell;
}

#pragma UITableView Delegates Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

@end
