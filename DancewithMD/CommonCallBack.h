//
//  CommonCallBack.h
//  DancewithMD
//
//  Created by Kirti Nikam on 23/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMProgressHUD.h"
#import "MMProgressHUDOverlayView.h"

@interface CommonCallBack : NSObject
+(void)showProgressHud:(NSString *)title subtitle:(NSString *)subTitle;
+(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle;

+(void)hideProgressHudWithSuccess;
+(void)hideProgressHudWithError;
+(void)hideProgressHud;
@end
