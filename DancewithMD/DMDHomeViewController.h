//
//  DMDHomeViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface DMDHomeViewController : UIViewController{
    int status;
    int fullscreen;
}
@property (strong, nonatomic) IBOutlet UIImageView *infoscreen;

@property (strong, nonatomic) IBOutlet UIImageView *videoImageView;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UIButton *englishBtn;
@property (strong, nonatomic) IBOutlet UIButton *hindiBtn;
@property (strong, nonatomic) IBOutlet UITextView *helptextView;
@property (strong, nonatomic) IBOutlet UILabel *welcomelbl;
@property (strong, nonatomic) IBOutlet UIView *blackView;

- (IBAction)englishBtnPressed:(id)sender;
- (IBAction)hindiBtnPressed:(id)sender;

- (IBAction)playVideoBtnClicked:(id)sender;
@end
