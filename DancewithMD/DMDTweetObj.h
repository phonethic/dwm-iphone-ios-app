//
//  DMDTweetObj.h
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDTweetObj : NSObject <NSCoding>{
    
}

@property (nonatomic, copy) NSString *fromuserName;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *tweetText;

@end
