//
//  DMDMyProfileViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>

#import "DMDMyProfileViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDMyBadge.h"
#import "DMDLeaderboardViewController.h"
#import "CommonCallBack.h"

#define USER_BADGES(USERID) [NSString stringWithFormat:@"%@/api/retrieve/mybadge/%@",LIVE_DWM_SERVER,USERID]
#define USER_RANK_LINK(USERID) [NSString stringWithFormat:@"%@/api/retrieve/rank/%@",LIVE_DWM_SERVER,USERID]
#define COUNTER_LINK [NSString stringWithFormat:@"%@/api/retrieve/counter",LIVE_DWM_SERVER]

#define USER_PIC(USERID) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",USERID]

@interface DMDMyProfileViewController ()

@end

@implementation DMDMyProfileViewController
@synthesize mybadgeArray;
@synthesize mybadgeTableView;
@synthesize badgescountlbl;
@synthesize specialbadgescountlbl;
@synthesize userPicImgView;
@synthesize userNamelbl;
@synthesize welcomelbl;
@synthesize downloadedVideoBtn;
@synthesize leaderboardBtn;
@synthesize ranklbl;
@synthesize pointslbl;
@synthesize dancerslbl;
@synthesize scrollView;
@synthesize dancersString,rankString,pointsString;
@synthesize statsBtn,badgesSummaryBtn;

@synthesize ranklblBackImageView,pointslblBackImageView,badgescountlblBackImageView,spclbadgescountlblBackImageView,dancerslblBackImageView,bdgeTableBackImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_MyProfile"];
    [[LocalyticsSession shared] tagScreen:@"MyProfile"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];
    [self setUI];

    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 820);
    
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self myBadgesAsynchronousCall];
        [self myRankAsynchronousCall];
        [self dancersAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    if (badgesConnection != nil) {
        [badgesConnection cancel];
        badgesConnection = nil;
    }
    if (rankConnection != nil) {
        [rankConnection cancel];
        rankConnection = nil;
    }
    if (dancersCountConnection != nil) {
        [dancersCountConnection cancel];
        dancersCountConnection = nil;
    }
}

- (void)viewDidUnload {
    [self setUserPicImgView:nil];
    [self setWelcomelbl:nil];
    [self setUserNamelbl:nil];
    [self setRanklbl:nil];
    [self setPointslbl:nil];
    [self setBadgescountlbl:nil];
    [self setSpecialbadgescountlbl:nil];
    [self setDancerslbl:nil];
    [self setLeaderboardBtn:nil];
    [self setDownloadedVideoBtn:nil];
    [self setMybadgeTableView:nil];
    [self setScrollView:nil];
    [self setRanklblBackImageView:nil];
    [self setPointslblBackImageView:nil];
    [self setBadgescountlblBackImageView:nil];
    [self setSpclbadgescountlblBackImageView:nil];
    [self setDancerslblBackImageView:nil];
    [self setBdgeTableBackImageView:nil];
    [self setStatsBtn:nil];
    [self setBadgesSummaryBtn:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}



#pragma Internal Callbacks
-(void)setUI{
    
    welcomelbl.text = @"welcome";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    NSString *userName = [defaults objectForKey:LOGIN_USERNAME];
    userNamelbl.text = [DMD_APP_DELEGATE doCapitalFirstLetter:userName];
    userNamelbl.numberOfLines = 2;
    [userPicImgView setImageWithURL:[NSURL URLWithString:USER_PIC(userID)]
                   placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];

    
    badgescountlbl.text = @"";
    specialbadgescountlbl.text = @"";
    ranklbl.text = @"";
    pointslbl.text = @"";
    dancerslbl.text =@"";
    
    dancersString = @"";
    rankString = @"";
    pointsString = @"";
    
    badgescountlbl.font         = KABEL_FONT(18);
    specialbadgescountlbl.font  = KABEL_FONT(18);
    
    ranklbl.font    = KABEL_FONT(18);
    pointslbl.font  = KABEL_FONT(18);
    dancerslbl.font = KABEL_FONT(18);
    
    userNamelbl.font= KABEL_FONT(18);
    welcomelbl.font = KABEL_FONT(15);
    
    badgescountlbl.textColor         = [UIColor whiteColor];
    specialbadgescountlbl.textColor  = [UIColor whiteColor];
    
    ranklbl.textColor    = [UIColor whiteColor];
    pointslbl.textColor  = [UIColor whiteColor];
    dancerslbl.textColor = [UIColor whiteColor];
    
    userNamelbl.textColor= [UIColor whiteColor];
    welcomelbl.textColor = [UIColor whiteColor];
    
    downloadedVideoBtn.titleLabel.font = KABEL_FONT(18);
    leaderboardBtn.titleLabel.font     = KABEL_FONT(18);
    
    mybadgeTableView.backgroundColor = [UIColor clearColor];
    
    statsBtn.titleLabel.font             = KABEL_FONT(15);
    badgesSummaryBtn.titleLabel.font     = KABEL_FONT(15);
    [self statsBtnClicked:statsBtn];
}

-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (badgesConnection != nil) {
                             [badgesConnection cancel];
                             badgesConnection = nil;
                         }
                         if (rankConnection != nil) {
                             [rankConnection cancel];
                             rankConnection = nil;
                         }
                         if (dancersCountConnection != nil) {
                             [dancersCountConnection cancel];
                             dancersCountConnection = nil;
                         }
                     }];
}

-(void)myBadgesAsynchronousCall
{
    [self showProgressHudWithCancel:@"Myprofile" subtitle:@"Loading"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_BADGES(userID)] cachePolicy:NO timeoutInterval:15.0];
    badgesConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)myRankAsynchronousCall
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_RANK_LINK(userID)] cachePolicy:NO timeoutInterval:5.0];
    rankConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)dancersAsynchronousCall
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:COUNTER_LINK] cachePolicy:NO timeoutInterval:15.0];
    dancersCountConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"MyProfileView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if(connection==badgesConnection)
    {
        //DebugLog(@"MyProfileView: con 1 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"MyProfileView: json: %@", json);
            NSString *statusValue = [json objectForKey:@"success"];
            if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                if (mybadgeArray == nil) {
                    mybadgeArray = [[NSMutableArray alloc] init];
                }else{
                    [mybadgeArray removeAllObjects];
                }
                NSArray *dataArray = [json objectForKey:@"data"];
                for (NSDictionary* mybadgeattributeDict in dataArray)
                {
                    //DebugLog(@"MyProfileView: count : %@", [mybadgeattributeDict objectForKey:@"count"]);
                    if([mybadgeattributeDict valueForKey:@"badge_name"] != nil)
                    {
                        mybadgeObj = [[DMDMyBadge alloc] init];
                        mybadgeObj.mybadge_name = [mybadgeattributeDict objectForKey:@"badge_name"];
                        mybadgeObj.mybadge_count = [mybadgeattributeDict objectForKey:@"count"];
                        mybadgeObj.mybadge_songs = [mybadgeattributeDict objectForKey:@"songs"];
                        mybadgeObj.mybadge_icon =  [mybadgeattributeDict objectForKey:@"image"];
                        ////DebugLog(@"MyProfileView: \n  badge_name: %@  , badge_count: %d  , badge_songs: %@  \n", mybadgeObj.mybadge_name, mybadgeObj.mybadge_count,  mybadgeObj.mybadge_songs);
//                        if(![[NSString stringWithFormat:@"%@",mybadgeObj.mybadge_count] isEqualToString:@"0"])
//                        {
                            [mybadgeArray addObject:mybadgeObj];
//                        }
                        mybadgeObj = nil;
                    } else {
                        if([mybadgeattributeDict valueForKey:@"badge"] != nil)
                        {
                            badgescountlbl.text = [NSString stringWithFormat:@"%@ / 8", [mybadgeattributeDict objectForKey:@"badge"]];
                            specialbadgescountlbl.text = [NSString stringWithFormat:@"%@ / 4", [mybadgeattributeDict objectForKey:@"spbadge"]];
                        }
                    }
                }
            }
            DebugLog(@"MyProfileView: mybadgeArray array count: %d", [mybadgeArray count]);
            [mybadgeTableView reloadData];
        }
        responseAsyncData = nil;
        badgesConnection = nil;
    }else if (connection == rankConnection)
    {
        //DebugLog(@"MyProfileView: con 2 length = %d",[responseAsyncData length]);
        if (responseAsyncData != nil && [responseAsyncData length] > 0) {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"MyProfileView: json: %@", json);
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* attributeDict in dataArray)
                    {
                        rankString =  [attributeDict objectForKey:@"rank"];
                        pointsString =  [attributeDict objectForKey:@"points"];
                        ranklbl.text = [NSString stringWithFormat:@"%@",rankString];
                        pointslbl.text = [NSString stringWithFormat:@"%@",pointsString];
                    }
                }
            }
        }
        rankConnection = nil;
        responseAsyncData = nil;
    }else if (connection == dancersCountConnection){
        
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"MyProfileView: json: %@", json);
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSDictionary *datadict = [json objectForKey:@"data"];
                    dancersString = [datadict objectForKey:@"count"];
                    dancerslbl.text = [NSString stringWithFormat:@"%@",dancersString];
                }
            }
        }
        dancersCountConnection = nil;
        responseAsyncData = nil;
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mybadgeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MyBadgeCell";
    UILabel *lblName;
    UILabel *lblCount;
    UILabel *lblSongs;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
//        cell.textLabel.textColor = [UIColor whiteColor];
//        cell.textLabel.font = [UIFont systemFontOfSize:24.0];
//        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
//        cell.detailTextLabel.font = [UIFont systemFontOfSize:20.0];
//        cell.textLabel.text = @"Badge";
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 50, 50)];
        thumbImg.tag = 4;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        [cell.contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,20.0,150.0,40.0)];
        lblName.tag = 1;
        lblName.text = @"Badge_Name";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(17);
        lblName.adjustsFontSizeToFitWidth = YES;
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        lblName.numberOfLines = 2;
        [cell.contentView addSubview:lblName];
        
        
        UIImageView *countImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblName.frame)+5,15.0,50.0,50.0)];
        countImg.image = [UIImage imageNamed:@"badges_counter_field.png"];
        countImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:countImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblName.frame)+5,25.0,50.0,30.0)];
        lblCount.tag = 2;
        lblCount.text = @"Badge_Count";
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblCount.font = KABEL_FONT(30);
        lblCount.adjustsFontSizeToFitWidth = YES;
        lblCount.textAlignment = UITextAlignmentCenter;
        lblCount.textColor = [UIColor whiteColor];
        lblCount.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblCount];
        
        UIImageView *seperatorImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,75,260,10)];
        seperatorImg.image = [UIImage imageNamed:@"badges_table_row_line.png"];
        seperatorImg.tag = 5;
        seperatorImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:seperatorImg];
    }
    
    DMDMyBadge *tempMybadgeObj  = [mybadgeArray objectAtIndex:indexPath.row];
    lblName = (UILabel *)[cell viewWithTag:1];
    lblCount = (UILabel *)[cell viewWithTag:2];
    lblSongs = (UILabel *)[cell viewWithTag:3];
    lblName.text = tempMybadgeObj.mybadge_name;
    lblCount.text = [NSString stringWithFormat:@"%@",tempMybadgeObj.mybadge_count];
    lblSongs.text = tempMybadgeObj.mybadge_songs;
    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:4];
    [thumbImgview setImageWithURL:[NSURL URLWithString:tempMybadgeObj.mybadge_icon]
                 placeholderImage:nil
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [mybadgeTableView reloadData];
}

#pragma UIButton delegate Method
- (IBAction)leaderboardBtnClicked:(id)sender {
    if([DMD_APP_DELEGATE networkavailable])
    {
        DMDLeaderboardViewController *profileController = [[DMDLeaderboardViewController alloc] initWithNibName:@"DMDLeaderboardViewController" bundle:nil];
        profileController.title = @"Leaderboard";
        profileController.dancersString = self.dancersString;
        profileController.rankString = self.rankString;
        profileController.pointsString = self.pointsString;
        [self.navigationController pushViewController:profileController animated:YES];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

- (IBAction)downloadedVideoBtnClicked:(id)sender {
}

- (IBAction)statsBtnClicked:(id)sender {
    statsBtn.selected = TRUE;
    badgesSummaryBtn.selected = FALSE;

    [ranklbl setHidden:NO];
    [pointslbl setHidden:NO];
    [badgescountlbl setHidden:NO];
    [specialbadgescountlbl setHidden:NO];
    [dancerslbl setHidden:NO];
    
    [ranklblBackImageView setHidden:NO];
    [pointslblBackImageView setHidden:NO];
    [badgescountlblBackImageView setHidden:NO];
    [spclbadgescountlblBackImageView setHidden:NO];
    [dancerslblBackImageView setHidden:NO];
    
    [mybadgeTableView setHidden:YES];
    [bdgeTableBackImageView setHidden:YES];
}

- (IBAction)badgesSummaryBtnClicked:(id)sender {
    
    badgesSummaryBtn.selected = TRUE;
    statsBtn.selected = FALSE;

    [ranklbl setHidden:YES];
    [pointslbl setHidden:YES];
    [badgescountlbl setHidden:YES];
    [specialbadgescountlbl setHidden:YES];
    [dancerslbl setHidden:YES];
    
    [ranklblBackImageView setHidden:YES];
    [pointslblBackImageView setHidden:YES];
    [badgescountlblBackImageView setHidden:YES];
    [spclbadgescountlblBackImageView setHidden:YES];
    [dancerslblBackImageView setHidden:YES];
    
    [mybadgeTableView setHidden:NO];
    [bdgeTableBackImageView setHidden:NO];
}
@end
