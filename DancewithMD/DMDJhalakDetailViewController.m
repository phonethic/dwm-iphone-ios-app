//
//  DMDJhalakDetailViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDJhalakDetailViewController.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDSongDetailObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CommonCallBack.h"

#define SONG_DETAIL_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/retrieve/songs/%@",LIVE_DWM_SERVER,SONGID]

#define SONG_SEEN_LINK(USERID,SONGID) [NSString stringWithFormat:@"%@/api/retrieve/points?where={\"User_ID\":\"%@\",\"Song_ID\":%@}",LIVE_DWM_SERVER,USERID,SONGID]

#define SONG_CLIP_ID_LINK(CLIPID) [NSString stringWithFormat:@"%@/api/retrieve/clips/%@",LIVE_DWM_SERVER,CLIPID]

#define SONG_PLAYED_LINK(USERID,SONGID,CLIPID) [NSString stringWithFormat:@"%@/point.php?uid=%@&sid=%@&cid=%d", LIVE_DWM_SERVER,USERID,SONGID,CLIPID]

@interface DMDJhalakDetailViewController ()

@end

@implementation DMDJhalakDetailViewController
@synthesize songID;
@synthesize jhalakSongDetailArray;
@synthesize seenCount;
@synthesize carousel;
@synthesize songLockType;
@synthesize selectedsongindex;
@synthesize closeVideoBtn;
@synthesize songName;
@synthesize movieController;
@synthesize videoView;
@synthesize testimageView;
@synthesize videowebView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma view lifeCycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DebugLog(@"JhalakDetail: viewWillAppear");
    if (jhalakSongDetailArray.count > 0) {
        [self hideVideoView];
        [UIView animateWithDuration:0.5 animations:^{
            [videoView setHidden:TRUE];
            videoView.alpha = 0.0;
        }];
        [self.carousel reloadData];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     @"Jhalak" ,@"DanceStyle",
    [NSString stringWithFormat:@"Jhalak - %@",songName],@"SongName",
     nil];
    [Flurry logEvent:@"View_Lessons" withParameters:storeParams];
    [[LocalyticsSession shared] tagEvent:@"View_Lessons" attributes:storeParams];
    [[LocalyticsSession shared] tagScreen:@"Lesson_Clips"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    carousel.type = iCarouselTypeCoverFlow;
    fullscreen = 0;
    self.seenCount = @"";
    [self setUI];
    
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self songDetailListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    if (songDetailConnection != nil) {
        [songDetailConnection cancel];
        songDetailConnection = nil;
    }
    if (songDetailLockConnection != nil) {
        [songDetailLockConnection cancel];
        songDetailLockConnection = nil;
    }
    if (songStreamRequestConnection != nil) {
        [songStreamRequestConnection cancel];
        songStreamRequestConnection = nil;
    }
    if (unlockSongConnection != nil) {
        [unlockSongConnection cancel];
        unlockSongConnection = nil;
    }
    if(self.movieController != nil)
    {
        [movieController.moviePlayer stop];
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
}
- (void)viewDidUnload {

    [self setCarousel:nil];
    [self setCloseVideoBtn:nil];
    [self setVideoView:nil];
    [self setTestimageView:nil];
    [self setVideowebView:nil];
    [super viewDidUnload];
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;//(interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;//UIInterfaceOrientationMaskLandscape;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
//     DebugLog(@"%f-%f-%f-%f",testimageView.frame.origin.x,testimageView.frame.origin.y,testimageView.frame.size.width,testimageView.frame.size.height);
    closeVideoBtn.frame = CGRectMake(CGRectGetMaxX(videowebView.frame)-20, closeVideoBtn.frame.origin.y, closeVideoBtn.frame.size.width, closeVideoBtn.frame.size.height);
}

#pragma Internal Callbacks
-(void)setUI
{
    closeVideoBtn.layer.cornerRadius = 20;
    closeVideoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    closeVideoBtn.layer.borderWidth = 3.0;
    videoView.backgroundColor = [UIColor blackColor];
    videoView.hidden = TRUE;
    videowebView.backgroundColor = [UIColor blackColor];
}

-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (songDetailConnection != nil) {
                             [songDetailConnection cancel];
                             songDetailConnection = nil;
                         }
                         if (songDetailLockConnection != nil) {
                             [songDetailLockConnection cancel];
                             songDetailLockConnection = nil;
                         }
                         if (songStreamRequestConnection != nil) {
                             [songStreamRequestConnection cancel];
                             songStreamRequestConnection = nil;
                         }
                         if (unlockSongConnection != nil) {
                             [unlockSongConnection cancel];
                             unlockSongConnection = nil;
                         }
                     }];
    
}

-(void)songDetailListAsynchronousCall
{
    [self showProgressHudWithCancel:[NSString stringWithFormat:@"%@\n Clips",songName] subtitle:@"Loading"];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_DETAIL_LIST_LINK(self.songID)] cachePolicy:NO timeoutInterval:5.0];
    //DebugLog(@"JhalakDetailView: connection1=%@",urlRequest.URL);
    //Get list of all songs
    songDetailConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)songLockAsynchronousCall
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSMutableString *requestString = [NSMutableString stringWithString:[DMD_APP_DELEGATE URLEncode:SONG_SEEN_LINK(userID,self.songID)]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    //DebugLog(@"JhalakDetailView: connection2=%@",urlRequest.URL);
    //Get count of total unlocked videos
    songDetailLockConnection  = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
-(void)songUrlAsynchronousCall:(NSString*) clipId
{
    NSString *requestString = [NSMutableString stringWithString:SONG_CLIP_ID_LINK(clipId)];
    //DebugLog(@"JhalakDetailView: CLIP LINK=%@",requestString);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    songStreamRequestConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)unlockNextSongToPlayAsynchronousCall:(int)clipID
{
    DebugLog(@"JhalakDetailView: unlockNextSongToPlayAsynchronousCall %d",clipID);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSMutableString *requestString = [NSMutableString stringWithString:SONG_PLAYED_LINK(userID,self.songID,clipID)];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    unlockSongConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
-(void)playVideoInWebView:(NSString *)videoUrl
{
    DebugLog(@"JhalakDetailView: link===%@",videoUrl);
    //http://dancewithmadhuri.com/jhalak/play-youtube.php?link=http://www.youtube.com/embed/AjIdywUOj2U&width=320&height=210
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@&width=280&height=300", @"http://dancewithmadhuri.com/jhalak/play-youtube.php?link=",videoUrl];
    
    //DebugLog(@"JhalakDetailView: urlAddress===%@",urlAddress);
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [videowebView loadRequest:requestObj];
    [videowebView setOpaque:NO];
    [videowebView setHidden:FALSE];
    videowebView.backgroundColor = [UIColor blackColor];
    [videowebView.scrollView setBounces:FALSE];
    [videowebView.scrollView setScrollEnabled:FALSE];
//    videowebView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin| UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin;
//    [self.videoView addSubview:videowebView];
//    [self.videoView insertSubview:self.closeVideoBtn aboveSubview:videowebView];
//    [self.closeVideoBtn setHidden:FALSE];
    [self.videoView setHidden:FALSE];
    videoView.alpha = 1.0;
    close = 0;
}

-(void)playVideofromUrl:(NSString *)videoUrl
{
    DebugLog(@"JhalakDetailView: playVideofromUrl %@",videoUrl);
    if(self.movieController != nil) {
        [movieController.moviePlayer stop];
        // Remove the movie player view controller from the "playback did finish" notification observers
        [[NSNotificationCenter defaultCenter] removeObserver:movieController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
    
    // Initialize the movie player view controller with a video URL string
    UIGraphicsBeginImageContext(CGSizeMake(1,1));
    movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    UIGraphicsEndImageContext();
    
    // Register this class as an observer instead
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieController.moviePlayer];
    
    // Set the modal transition style of your choice
    movieController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    // Present the movie player view controller
    //    [self presentModalViewController:movieController animated:YES];
    [self presentMoviePlayerViewControllerAnimated:movieController];
    
    // Start playback
    [movieController.moviePlayer prepareToPlay];
    [movieController.moviePlayer play];
}

#pragma MPMoviePlayerController callbacks
- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    DebugLog(@"LearnDanceDetailView: moviePlayBackDidFinish");
    // Obtain the reason why the movie playback finished
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([finishReason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonPlaybackEnded");
            [self unlockNextSongToPlayAsynchronousCall:selectedsongindex+1]; //Unlock next video
        }
            break;
        case MPMovieFinishReasonPlaybackError:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonPlaybackError");
            
        }
            break;
        case MPMovieFinishReasonUserExited:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonUserExited");
            
        }
            break;
        default:
            break;
    }
    // Remove this class from the observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:movieController.moviePlayer];
    
    //       [self dismissModalViewControllerAnimated:NO];
    [self dismissMoviePlayerViewControllerAnimated];
    movieController = nil;
    [self hideVideoView];
    [self.videoView setHidden:TRUE];
}

-(void)hideVideoView {
    //    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.8f;
    //    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionFade;
    //    [backgroundImageView.layer addAnimation:transition forKey:nil];
    //
    //    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    //    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
    //    unshrink.toValue = [NSNumber numberWithDouble:1.5];
    //    unshrink.duration = 0.8;
    //    unshrink.fillMode = kCAFillModeForwards;
    //    unshrink.removedOnCompletion = NO;
    //    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
    //
    //    [UIView beginAnimations:nil context:nil];
    //    [UIView setAnimationDuration:0.6];
    //    [videoView setAlpha:0.0];
    //    [UIView commitAnimations];
    //
    //    [UIView beginAnimations:nil context:nil];
    //    [UIView setAnimationDuration:0.5];
    //    [lightspotImageView setAlpha:1.0];
    //    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    small = 0;
    //[videoView setHidden:TRUE];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"JhalakDetailView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if(connection==songDetailConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"JhalakDetailView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (jhalakSongDetailArray == nil) {
                        jhalakSongDetailArray = [[NSMutableArray alloc] init];
                    }else{
                        [jhalakSongDetailArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        songDetailObj = [[DMDSongDetailObj alloc] init];
                        songDetailObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                        songDetailObj.songName = [songattributeDict objectForKey:@"Song_Name"];
                        songDetailObj.songClip_Part_ID = [songattributeDict objectForKey:@"Clip_Part_ID"];
                        songDetailObj.songSequence = [songattributeDict objectForKey:@"Sequence"];
                        songDetailObj.songShare_ID = [songattributeDict objectForKey:@"Share_ID"];
                        songDetailObj.songclip_Title = [songattributeDict objectForKey:@"Clip_Title"];
                        ////DebugLog(@"JhalakDetailView: \n songid: %@ , name: %@  , clipid: %@  , sequence: %@  , shareid: %@ \n", songDetailObj.songId, songDetailObj.songName, songDetailObj.songClip_Part_ID, songDetailObj.songSequence, songDetailObj.songShare_ID);
                        [jhalakSongDetailArray addObject:songDetailObj];
                        songDetailObj = nil;
                    }
                }
            }
        }
        responseAsyncData = nil;
        songDetailConnection=nil;
        ////DebugLog(@"JhalakDetailView: detail array count: %d", [songDetailArray count]);
        [carousel reloadData];
        if([self.songLockType isEqualToString:@"1"])
            [self songLockAsynchronousCall];
        else
            self.seenCount = [NSString stringWithFormat:@"%d",[jhalakSongDetailArray count]];
    }else if (connection==songDetailLockConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"JhalakDetailView: result=%@", json ) ;
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* lockattributeDict in dataArray)
                    {
                        if([lockattributeDict valueForKey:@"User_ID"] != nil) {
                            self.seenCount = [lockattributeDict objectForKey:@"Seen_Clips"] ;
                        } else {
                            NSString *dataString = [lockattributeDict objectForKey:@"data"];
                            DebugLog(@"JhalakDetailView: %@",dataString);
                        }
                    }
                }
            }
        }
        songDetailLockConnection = nil;
        responseAsyncData = nil;
        [carousel reloadData];
    }else if(connection == songStreamRequestConnection){
        
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"JhalakDetailView: json: %@", json);
            
            NSString *videoType;
            NSString *urlString;
            
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        videoType = [songattributeDict objectForKey:@"Type"];
                        if([videoType isEqualToString:@"embedd"])
                        {
                            urlString = [songattributeDict objectForKey:@"URL"];
                            [self playVideoInWebView:urlString];
                        } else {
                            urlString = [songattributeDict objectForKey:@"Video_URL"];
                            [self playVideofromUrl:[DMD_APP_DELEGATE URLEncode:urlString]];
                        }
                    }
                }
            }
        }
        songStreamRequestConnection=nil;
        responseAsyncData = nil;
    }else if (connection==unlockSongConnection)
    {
        if(responseAsyncData != nil)
        {
            NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                        length:[responseAsyncData length] encoding:NSASCIIStringEncoding];
            DebugLog(@"JhalakDetailView: unlockSongconnection  result = %@", result ) ;
            if(result!=nil && [result isEqualToString:@"1"])
            {
                self.seenCount = [NSString stringWithFormat:@"%d",[self.seenCount intValue] + 1];
//                [carousel reloadData];
                [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
                
            } else if (result!=nil && [result isEqualToString:@"2"]) {
                [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
            }
        }
        unlockSongConnection = nil;
        responseAsyncData = nil;
        [carousel reloadData];
    }
}
#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [jhalakSongDetailArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f,200.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, 200, 20)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = GRAY_TEXT_COLOR;
        label.font = KABEL_FONT(18);
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    ////DebugLog(@"JhalakDetailView: %f %f",((UIImageView *)view).frame.size.width,((UIImageView *)view).frame.size.height);
    
    ////DebugLog(@"JhalakDetailView: irow=%d icount=%d",index,[self.seenCount intValue]);
    if([self.songLockType isEqualToString:@"1"])
    {
        if([self.seenCount intValue] >= index)
        {
            ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_unlock.png"];
            //DebugLog(@"JhalakDetailView: unlock");
        } else {
            ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
            //DebugLog(@"JhalakDetailView: locked");
        }
    } else {
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_unlock.png"];
    }
    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }else if(small==0){
        self.carousel.userInteractionEnabled = TRUE;
    }
    
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    DMDSongDetailObj *tempdetailsongObj  = [jhalakSongDetailArray objectAtIndex:index];
//    NSArray *arr = [tempdetailsongObj.songClip_Part_ID componentsSeparatedByString:@"_"];
//    ////DebugLog(@"JhalakDetailView: array = %@", arr);
//    label.text = [NSString stringWithFormat:@"Lesson %@  Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]];
    label.text = tempdetailsongObj.songclip_Title;
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
   
    if([self.seenCount intValue] >= index)
    {
        DMDSongDetailObj *tempdetailsongObj  = [jhalakSongDetailArray objectAtIndex:index];
        selectedsongindex = index;
        //    [self showVideoView];
        if (small==0 || small==2) {
            small = 1;
        } else if(small==1){
            small = 2;
        }
        [self.carousel reloadData];
        [self songUrlAsynchronousCall:tempdetailsongObj.songClip_Part_ID];
        NSDictionary *storeParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
        @"Jhalak" ,@"DanceStyle",
         [NSString stringWithFormat:@"Jhalak - %@",songName],@"SongName",
         [NSString stringWithFormat:@"Jhalak - %@ - %@", songName,tempdetailsongObj.songclip_Title],@"ClipTitle",
         nil];
        [Flurry logEvent:@"View_Clips" withParameters:storeParams];
        [[LocalyticsSession shared] tagEvent:@"View_Clips" attributes:storeParams];
    }
//        DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:index];
//        selectedsongindex = index;
//        selectedsongclipID = tempdetailsongObj.songClip_Part_ID;
//        selectedsongName =  tempdetailsongObj.songName;
//        //[self showbadgeView:self.songID clipPartId:selectedsongclipID];
//        //DebugLog(@"JhalakDetailView: songID=%@ , clipID=%@ songName=%@",self.songID, self.selectedsongclipID,self.selectedsongName);
//        titlelbl.text = [NSString stringWithFormat:@"%@",tempdetailsongObj.songName];
//        NSArray *arr = [tempdetailsongObj.songClip_Part_ID componentsSeparatedByString:@"_"];
//        //DebugLog(@"JhalakDetailView: %@", arr);
//        namelbl.text = [NSString stringWithFormat:@"Lesson %@      Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]];
//        NSString * fileName = [NSString stringWithFormat:@"%@#%@.mp4",tempdetailsongObj.songName,[NSString stringWithFormat:@"Lesson %@ Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]]];
//        fileName = [fileName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
//        //DebugLog(@"JhalakDetailView: filename=%@",fileName);
//        
//        NSArray *dirPaths;
//        NSString *fullPath;
//        NSString *docsDir;
//        NSFileManager *filemgr =[NSFileManager defaultManager];
//        // Get the documents directory
//        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        docsDir = [dirPaths objectAtIndex:0];
//        //DebugLog(@"JhalakDetailView: song id = %@",self.songID);
//        fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@",self.selectedsongName]];
//        NSString *filepath = [fullPath stringByAppendingPathComponent:fileName];
//        //DebugLog(@"JhalakDetailView: filepath = %@" , filepath);
//        if ([filemgr fileExistsAtPath:filepath])
//        {
//            //DebugLog(@"JhalakDetailView: file exists");
//            progressBar.hidden = FALSE;
//            [progressBar setProgress:1.0];
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateNormal];
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateHighlighted];
//            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateNormal];
//            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateHighlighted];
//            downloadBtn.userInteractionEnabled = FALSE;
//            [self showVideoView];
//            if (small==0 || small==2) {
//                small = 1;
//            } else if(small==1){
//                small = 2;
//            }
//            self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            progressBar.hidden = FALSE;
//            [videoView setHidden:FALSE];
//            screenshotView.hidden = FALSE;
//            
//            UIGraphicsBeginImageContext(videoboxImageView.frame.size);
//            [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
//            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//            screenshotView.image = viewImage;
//            UIGraphicsEndImageContext();
//            //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
//            
//            
//            // determine the size of the reflection to create
//            NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
//            
//            // create the reflection image and assign it to the UIImageView
//            self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
//            //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
//            self.reflectionView.alpha = kDefaultReflectionOpacity;
//            
//            screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            screenshotView.hidden = TRUE;
//            
//            [self.carousel reloadData];
//            [self performSelector:@selector(loadLocalDocument:) withObject:filepath afterDelay:0.5];
//            
//        } else {
//            //DebugLog(@"JhalakDetailView: file not exists");
//            downloadBtn.userInteractionEnabled = TRUE;
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateNormal];
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateHighlighted];
//            [downloadBtn setTitle:@"" forState:UIControlStateNormal];
//            [downloadBtn setTitle:@"" forState:UIControlStateHighlighted];
//            [progressBar setProgress:0];
//            [self showVideoView];
//            if (small==0 || small==2) {
//                small = 1;
//            } else if(small==1){
//                small = 2;
//            }
//            self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            progressBar.hidden = TRUE;
//            [videoView setHidden:FALSE];
//            screenshotView.hidden = FALSE;
//            
//            UIGraphicsBeginImageContext(videoboxImageView.frame.size);
//            [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
//            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//            screenshotView.image = viewImage;
//            UIGraphicsEndImageContext();
//            //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
//            
//            
//            // determine the size of the reflection to create
//            NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
//            
//            // create the reflection image and assign it to the UIImageView
//            self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
//            //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
//            self.reflectionView.alpha = kDefaultReflectionOpacity;
//            
//            screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            screenshotView.hidden = TRUE;
//            
//            [self.carousel reloadData];
//            //DebugLog(@"JhalakDetailView: ===ID===%@",tempdetailsongObj.songClip_Part_ID);
//            [self songUrlAsynchronousCall:tempdetailsongObj.songClip_Part_ID];
//        }
//        //DebugLog(@"JhalakDetailView: unlock");
//    } else {
//        //DebugLog(@"JhalakDetailView: locked");
//    }
    
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}

#pragma UIButton delegate Method

- (IBAction)closeVideoBtnClicked:(id)sender {
    if(self.movieController != nil)
    {
        [movieController.moviePlayer stop];
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
    [self hideVideoView];
    [self.videoView setHidden:TRUE];
}
@end
