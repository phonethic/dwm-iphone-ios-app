//
//  CommonCallBack.m
//  DancewithMD
//
//  Created by Kirti Nikam on 23/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CommonCallBack.h"


@implementation CommonCallBack
+(void)showProgressHud:(NSString *)title subtitle:(NSString *)subTitle{
    //random color
    //    CGFloat red =  arc4random_uniform(256)/255.f;
    //    CGFloat blue = arc4random_uniform(256)/255.f;
    //    CGFloat green = arc4random_uniform(256)/255.f;
    //    CGColorRef color = CGColorRetain([UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor);
    //    [[[MMProgressHUD sharedHUD] overlayView] setOverlayColor:color];
    //    CGColorRelease(color);
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title status:subTitle];
}
+(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         NSLog(@"Task was cancelled!");
                     }];

}

+(void)hideProgressHudWithSuccess
{
    [MMProgressHUD dismissWithSuccess:@"Success!"];
}

+(void)hideProgressHudWithError
{
    [MMProgressHUD dismissWithError:@"Error!"];
}
+(void)hideProgressHud
{
    [MMProgressHUD dismiss];
}
@end
