//
//  DMDMyProfileViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDMyBadge;
@interface DMDMyProfileViewController : UIViewController <UIScrollViewDelegate>
{
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *badgesConnection;
    NSURLConnection *rankConnection;
    NSURLConnection *dancersCountConnection;
    DMDMyBadge *mybadgeObj;
}
@property (strong, nonatomic) NSMutableArray *mybadgeArray;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (copy, nonatomic) NSString *dancersString;
@property (copy, nonatomic) NSString *rankString;
@property (copy, nonatomic) NSString *pointsString;

@property (strong, nonatomic) IBOutlet UIImageView *userPicImgView;
@property (strong, nonatomic) IBOutlet UILabel *welcomelbl;
@property (strong, nonatomic) IBOutlet UILabel *userNamelbl;

@property (strong, nonatomic) IBOutlet UILabel *ranklbl;
@property (strong, nonatomic) IBOutlet UILabel *pointslbl;
@property (strong, nonatomic) IBOutlet UILabel *badgescountlbl;
@property (strong, nonatomic) IBOutlet UILabel *specialbadgescountlbl;
@property (strong, nonatomic) IBOutlet UILabel *dancerslbl;

@property (strong, nonatomic) IBOutlet UIImageView *ranklblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *pointslblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *badgescountlblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *spclbadgescountlblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *dancerslblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *bdgeTableBackImageView;

@property (strong, nonatomic) IBOutlet UIButton *statsBtn;
@property (strong, nonatomic) IBOutlet UIButton *badgesSummaryBtn;



@property (strong, nonatomic) IBOutlet UIButton *leaderboardBtn;
@property (strong, nonatomic) IBOutlet UIButton *downloadedVideoBtn;

@property (strong, nonatomic) IBOutlet UITableView *mybadgeTableView;


- (IBAction)leaderboardBtnClicked:(id)sender;
- (IBAction)downloadedVideoBtnClicked:(id)sender;
- (IBAction)statsBtnClicked:(id)sender;

- (IBAction)badgesSummaryBtnClicked:(id)sender;
@end
