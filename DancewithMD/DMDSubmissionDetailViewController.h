//
//  DMDSubmissionDetailViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 07/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MediaPlayer/MediaPlayer.h>

@class DMDSubmittedVideosObj;
@interface DMDSubmissionDetailViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    int status;
    int small;
    int fullscreen;
    int close;
    
    NSMutableData *responseAsyncData;
    NSURLConnection *submissionConnection;
    NSURLConnection *voteConnection;
    
    DMDSubmittedVideosObj *videosObj;
}
@property (nonatomic, copy) NSString *selectedSongId;
@property (nonatomic, copy) NSString *selectedSongName;

@property (nonatomic, copy) NSString *selectedvideoUserId;
@property (nonatomic, copy) NSString *selectedvideoId;

@property (strong, nonatomic) NSMutableArray *videoArray;

@property (strong, nonatomic) MPMoviePlayerViewController *movieController;

@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIButton *closeVideoBtn;
@property (strong, nonatomic) IBOutlet UIButton *voteBtn;
@property (strong, nonatomic) IBOutlet UIView *voteView;
@property (strong, nonatomic) IBOutlet UIImageView *voteImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblvoteHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblVoteUsername;



- (IBAction)closeVideoBtnClicked:(id)sender;
- (IBAction)voteBtnClicked:(id)sender;
@end
