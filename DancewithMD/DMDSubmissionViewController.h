//
//  DMDSubmissionViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@class DMDSongObj;
@interface DMDSubmissionViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    int status;
    NSMutableData *responseAsyncData;
    
//    DMDSubmittedVideosObj *videosObj;
    DMDSongObj *songObj;
    
    UIPopoverController *popover;
    NSURLConnection *songListConnection;
    NSURLConnection *uploadVideoConnection;

}

@property (strong, nonatomic) NSMutableArray *songsArray;
@property (nonatomic, copy) NSString *selectedSongId;
@property (nonatomic, copy) NSString *selectedSongName;

@property (strong, nonatomic) NSURL *uploadVideoURL;


@property (strong, nonatomic) IBOutlet UILabel *submissionInfoLabel;
@property (strong, nonatomic) IBOutlet UIButton *selectedsongBtn;
@property (strong, nonatomic) IBOutlet UITableView *submissionTableView;
@property (strong, nonatomic) IBOutlet UILabel *uploadInfoLabel;
@property (strong, nonatomic) IBOutlet UIButton *uploadBtn;
@property (strong, nonatomic) IBOutlet UITableView *uploadTableView;

- (IBAction)selectedsongBtnClicked:(id)sender;
- (IBAction)uploadBtnClicked:(id)sender;
@end
