//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "DMDAppDelegate.h"
#import "constants.h"

#import "DMDHomeViewController.h"
#import "DMDMyProfileViewController.h"
#import "DMDLearnDanceMainViewController.h"
#import "DMDSubmissionViewController.h"
#import "DMDExpertsViewController.h"
#import "DMDDanceFloorViewController.h"
#import "DMDTweetsViewController.h"
#import "DMDFBViewController.h"
#import "DMDFeedbackViewController.h"
#import "DMDJhalakViewController.h"
#import "DMDDisclaimerViewController.h"


#define HOME @" Home"
#define PROFILE @" My Profile"
#define LEARNDANCE @"  Learn Dance"
#define SUBMISSIONS @" Submissions"
#define LEADERBOARD @" Leaderboard"
#define GURUSPEAK @" Star Speak"
#define DANCEFLOOR @"Dancefloor"
#define TWEETER @" #DancewithMadhuri"
#define FACEBOOK @" Facebook"
#define FEEDBACK @" Feedback"
#define JHALAK @" Jhalak"
#define DISCLAIMER @" Disclaimer"
#define LOGOUT @" Logout"

#define LOGOUT_LINK [NSString stringWithFormat:@"%@/api/retrieve/logout",LIVE_DWM_SERVER]

@interface SideMenuViewController()
@property(nonatomic, strong) UISearchBar *searchBar;
@end

@implementation SideMenuViewController

@synthesize sideMenu;
@synthesize searchBar;
@synthesize sideMenuArray;


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1];
    self.tableView.separatorColor = [UIColor colorWithPatternImage:[self separatorImage]];
    self.tableView.scrollEnabled = NO;
        
    sideMenuArray = [[NSMutableArray alloc] init];
    [sideMenuArray addObject:HOME];
    [sideMenuArray addObject:PROFILE];
    [sideMenuArray addObject:LEARNDANCE];
    [sideMenuArray addObject:SUBMISSIONS];
    //[sideMenuArray addObject:LEADERBOARD];
    [sideMenuArray addObject:GURUSPEAK];
//    [sideMenuArray addObject:DANCEFLOOR];
    [sideMenuArray addObject:TWEETER];
    [sideMenuArray addObject:FACEBOOK];
    [sideMenuArray addObject:FEEDBACK];
    [sideMenuArray addObject:JHALAK];
    [sideMenuArray addObject:DISCLAIMER];
    [sideMenuArray addObject:LOGOUT];
    
//    CGRect searchBarFrame = CGRectMake(0, 0, self.tableView.frame.size.width, 45.0);
//    self.searchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
//    self.searchBar.delegate = self;
//    
//    self.tableView.tableHeaderView = self.searchBar;
    
    UINavigationBar *tempNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [tempNavigationBar setBarStyle:UIBarStyleDefault];
    tempNavigationBar.tintColor = BLACK_COLOR;
    
    UINavigationItem *navItem = [UINavigationItem alloc];
    navItem.title = @"Menu";
    [tempNavigationBar pushNavigationItem:navItem animated:false];
    self.tableView.tableHeaderView = tempNavigationBar;

    currentIndex = 0;
    preIndex = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationResponse:) name:JHALAK_NOTIFICATION object:nil];
}

#pragma mark -
#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", section];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    DebugLog(@"SideMenu: %d %f %f %u", [sideMenuArray count], self.view.frame.size.width,self.view.frame.size.height,(416 / self.sideMenuArray.count));
    if (self.sideMenuArray.count == 11) {
        return 35;
    }
   return 42;
//    return 416 / self.sideMenuArray.count ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sideMenuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"sideMenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
        
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.shadowColor = [UIColor blackColor];
        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        cell.textLabel.font = KABEL_FONT(22);
        cell.textLabel.minimumFontSize = 18;
        //cell.textLabel.highlightedTextColor = [UIColor colorWithRed:128/255.0 green:126/255.0 blue:124/255.0 alpha:1];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1];
        cell.selectedBackgroundView = bgColorView;
        
        //cell.imageView.frame = CGRectMake(0,0,50,50);
    }
    NSString *text = [sideMenuArray objectAtIndex:indexPath.row];
    
    if ([text isEqualToString:HOME]) { //Home
        cell.imageView.image = [UIImage imageNamed:@"Icon_home_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_home_off.png"];
    } else if ([text isEqualToString:PROFILE]) { //Profile
        cell.imageView.image = [UIImage imageNamed:@"Icon_profile_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_profile_off.png"];
    } else if ([text isEqualToString:LEARNDANCE]) { //Learn Dance
        cell.imageView.image = [UIImage imageNamed:@"Icon_learndance_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_learndance_off.png"];
    } else if ([text isEqualToString:SUBMISSIONS]) { //Submissions
        cell.imageView.image = [UIImage imageNamed:@"Icon_submission_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_submission_off.png"];
    } else if ([text isEqualToString:LEADERBOARD]) { //Leader Borad
        cell.imageView.image = [UIImage imageNamed:@"Icon_leaderboard_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_leaderboard_off.png"];
    } else if ([text isEqualToString:GURUSPEAK]) { //Gurus Speak
        cell.imageView.image = [UIImage imageNamed:@"Icon_gurusspeak_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_gurusspeak_off.png"];
    } else if ([text isEqualToString:DANCEFLOOR]) { //Dance Floor
        cell.imageView.image = [UIImage imageNamed:@"Icon_dancefloor_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_dancefloor_off.png"];
    } else if ([text isEqualToString:TWEETER]) { //Tweets
        cell.imageView.image = [UIImage imageNamed:@"Icon_tweeter_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_tweeter_off.png"];
    } else if ([text isEqualToString:FACEBOOK]) { //Facebook
        cell.imageView.image = [UIImage imageNamed:@"Icon_facebook_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_facebook_off.png"];
    } else if ([text isEqualToString:FEEDBACK]) { //Feedback
        cell.imageView.image = [UIImage imageNamed:@"Icon_feedback_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_feedback_off.png"];
    } else if ([text isEqualToString:JHALAK]) { //Jhalak
        cell.imageView.image = [UIImage imageNamed:@"jdj_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"jdj_off.png"];
    } else if ([text isEqualToString:DISCLAIMER]) { //Disclaimer
        cell.imageView.image = [UIImage imageNamed:@"Icon_disclaimer_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_disclaimer_off.png"];
    } else if ([text isEqualToString:LOGOUT]) { //Logout
        cell.imageView.image = [UIImage imageNamed:@"Icon_logout_on.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Icon_logout_off.png"];
    }
    cell.textLabel.text = [sideMenuArray objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //DebugLog(@"SideMenu: row=%d",indexPath.row);
    //DebugLog(@"SideMenu: array=%@",self.sideMenu.navigationController.viewControllers);
    
    if(currentIndex == indexPath.row)
        return;
    
    preIndex = currentIndex;
    NSString *selected = [sideMenuArray objectAtIndex:indexPath.row];
    
    if ([selected isEqualToString:HOME]) 
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDHomeViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
             obj = nil;
        }
        DMDHomeViewController *homeController = [[DMDHomeViewController alloc] initWithNibName:@"DMDHomeViewController" bundle:nil];
        homeController.title = HOME;
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:PROFILE])
    {

        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDMyProfileViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
             obj = nil;
        }
        DMDMyProfileViewController *profileController = [[DMDMyProfileViewController alloc] initWithNibName:@"DMDMyProfileViewController" bundle:nil];
        profileController.title = PROFILE;
        NSArray *controllers = [NSArray arrayWithObject:profileController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:LEARNDANCE])  //LEARN DANCE
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDLearnDanceMainViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDLearnDanceMainViewController *danceController = [[DMDLearnDanceMainViewController alloc] initWithNibName:@"DMDLearnDanceMainViewController" bundle:nil];
        danceController.title = LEARNDANCE;
        NSArray *controllers = [NSArray arrayWithObject:danceController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:SUBMISSIONS])  //SUBMISSIONS 
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDSubmissionViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDSubmissionViewController *submissionController = [[DMDSubmissionViewController alloc] initWithNibName:@"DMDSubmissionViewController" bundle:nil];
        submissionController.title = SUBMISSIONS;
        NSArray *controllers = [NSArray arrayWithObject:submissionController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:GURUSPEAK])  //GURUS SPEAK
    {
       
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDExpertsViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDExpertsViewController *profileController = [[DMDExpertsViewController alloc] initWithNibName:@"DMDExpertsViewController" bundle:nil];
        profileController.title = GURUSPEAK;
        NSArray *controllers = [NSArray arrayWithObject:profileController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:DANCEFLOOR])  //DANCE FLOOR
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDDanceFloorViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDDanceFloorViewController *dancefloorController = [[DMDDanceFloorViewController alloc] initWithNibName:@"DMDDanceFloorViewController" bundle:nil];
        dancefloorController.title = DANCEFLOOR;
        NSArray *controllers = [NSArray arrayWithObject:dancefloorController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:TWEETER])  //TWEETS
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDTweetsViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDTweetsViewController *tweetsController = [[DMDTweetsViewController alloc] initWithNibName:@"DMDTweetsViewController" bundle:nil];
        tweetsController.title = TWEETER;
        NSArray *controllers = [NSArray arrayWithObject:tweetsController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:FACEBOOK])  //FACEBOOK
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDFBViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDFBViewController *fbController = [[DMDFBViewController alloc] initWithNibName:@"DMDFBViewController" bundle:nil];
        fbController.title = FACEBOOK;
        NSArray *controllers = [NSArray arrayWithObject:fbController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:FEEDBACK])   //FEEDBACK
    {
        
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDFeedbackViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDFeedbackViewController *feedackController = [[DMDFeedbackViewController alloc] initWithNibName:@"DMDFeedbackViewController" bundle:nil];
        feedackController.title = FEEDBACK;
        NSArray *controllers = [NSArray arrayWithObject:feedackController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;

    }
    else if ([selected isEqualToString:JHALAK])  //JHALAK
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDJhalakViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDJhalakViewController *jhalakController = [[DMDJhalakViewController alloc] initWithNibName:@"DMDJhalakViewController" bundle:nil];
        jhalakController.title = JHALAK;
        NSArray *controllers = [NSArray arrayWithObject:jhalakController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:DISCLAIMER])  //DISCLAIMER
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[DMDDisclaimerViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        DMDDisclaimerViewController *disclaimerController = [[DMDDisclaimerViewController alloc] initWithNibName:@"DMDDisclaimerViewController" bundle:nil];
        disclaimerController.title = DISCLAIMER;
        NSArray *controllers = [NSArray arrayWithObject:disclaimerController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:LOGOUT])  //LOGOUT
    {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                          message:@"Are you sure you want to Sign out of this app ?"
                                                         delegate:self
                                                cancelButtonTitle:@"NO"
                                                otherButtonTitles:@"YES",nil];
        [message show];
        
    }
    
    if(self.searchBar.isFirstResponder) [self.searchBar resignFirstResponder];
     
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"NO"])
    {
        //DebugLog(@"SideMenu: New User NO was selected.");
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:preIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    else if([title isEqualToString:@"YES"])
    {
        currentIndex = 0;
        //DebugLog(@"SideMenu:  New User YES was selected.");
        DMDHomeViewController *homeController = [[DMDHomeViewController alloc] initWithNibName:@"DMDHomeViewController" bundle:nil];
        homeController.title = @"Home";
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        
        [FBSession.activeSession closeAndClearTokenInformation];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *userID = [defaults objectForKey:LOGIN_USERID];
        NSString *userPass = [defaults objectForKey:LOGIN_PASSWORD];
        NSString *userEmail = [defaults objectForKey:LOGIN_EMAIL];
         NSString *userToken = [defaults objectForKey:LOGIN_TOKEN];
        if(userID != nil && userPass != nil && userEmail != nil && userToken != nil && [userID length] > 0 && [userPass length] > 0 && [userEmail length] > 0 && [userToken length] > 0) 
        {
            //DebugLog(@"SideMenu: Logot called from SideMenuController %@ %@ %@ %@",userID,userPass,userEmail,userToken);
            if([DMD_APP_DELEGATE networkavailable]) {
               [self logoutAsynchronousCall:userEmail token:userToken];
            }
        }
        [self clearUserData];
    }
}

-(void)logoutAsynchronousCall:(NSString *)userEmail token:(NSString *)userToken
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGOUT_LINK] cachePolicy:NO timeoutInterval:15.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    [postReq setObject:userToken forKey:@"token"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        DebugLog(@"SideMenu: Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"SideMenu: ---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    logoutConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"SideMenu: Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    if (connection==logoutConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"SideMenu: json: %@", json);
            if(json != nil) {
                NSString * result = [json objectForKey:@"data"];
                //DebugLog(@"SideMenu: %@",result);
                if([result isEqualToString:@"logout"])
                {
                    [self showAlert:@"You have successfully logged out."];
                    [self clearUserData];
                } else if ([result isEqualToString:@"Already logout"]) {
                    [self showAlert:@"You are already logged out."];
                } else {
                    [self showAlert:result];
                }
            }
        }
        responseAsyncData = nil;
        logoutConnection = nil;
    }
}

-(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

-(void)clearUserData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:LOGIN_USERID];
    [defaults setObject:@"" forKey:LOGIN_TOKEN];
    [defaults setObject:@"" forKey:LOGIN_USERNAME];
    [defaults setObject:@"" forKey:LOGIN_EMAIL];
    [defaults setObject:@"" forKey:LOGIN_GENDER];
    [defaults setObject:@"" forKey:LOGIN_PASSWORD];
    [defaults setObject:@"" forKey:LOGGED_BY];
    [defaults setObject:[NSNumber numberWithBool:0] forKey:LOGIN_HELPINFOSCREEN];
    [defaults synchronize];
}

#pragma mark -
#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [UIView beginAnimations:nil context:NULL];
    [self.sideMenu setMenuWidth:320.0f animated:NO];
    [self.searchBar layoutSubviews];
    [UIView commitAnimations];
    
    [self.searchBar setShowsCancelButton:YES animated:YES];
    
    self.sideMenu.panMode = MFSideMenuPanModeNone;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [UIView beginAnimations:nil context:NULL];
    [self.sideMenu setMenuWidth:280.0f animated:NO];
    [self.searchBar layoutSubviews];
    [UIView commitAnimations];
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
    
    self.sideMenu.panMode = MFSideMenuPanModeDefault;
}

-(void)handleNotificationResponse:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    BOOL showJhalak = [[dict objectForKey:JHALAK_KEY] boolValue];
    if(NSNotFound == [sideMenuArray indexOfObject:JHALAK]) {
        //DebugLog(@"SideMenu: not found");
        if (showJhalak) {
            //DebugLog(@"SideMenu: show jhalak = yes");
            [sideMenuArray insertObject:JHALAK atIndex:sideMenuArray.count-1];
       }
    }else{
        //DebugLog(@"SideMenu: found");
        if (!showJhalak) {
            //DebugLog(@"SideMenu: show jhalak = no");
            [sideMenuArray removeObject:JHALAK];
        }
    }
    [self.tableView reloadData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
