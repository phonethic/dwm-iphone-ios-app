//
//  DMDSubmissionViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSubmissionViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDSongObj.h"
#import "DMDSubmissionDetailViewController.h"

#import "CommonCallBack.h"

#define SONG_LIST_LINK @"http://dancewithmadhuri.com/api/retrieve/songinfo"
#define UPLOAD_VIDEO_URL @"http://dancewithmadhuri.com/submit.php"

@interface DMDSubmissionViewController ()

@end

@implementation DMDSubmissionViewController
@synthesize songsArray;
@synthesize selectedSongId,selectedSongName;
@synthesize selectedsongBtn,uploadBtn;
@synthesize submissionInfoLabel,uploadInfoLabel;
@synthesize submissionTableView,uploadTableView;
@synthesize uploadVideoURL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_Submission"];
    [[LocalyticsSession shared] tagScreen:@"Submission"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];
    [self setUI];
    
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self songListAsynchronousCall];

    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    if (songListConnection != nil) {
        [songListConnection cancel];
        songListConnection = nil;
    }
    if (uploadVideoConnection != nil) {
        [uploadVideoConnection cancel];
        uploadVideoConnection = nil;
    }
}
- (void)viewDidUnload {
    [self setSelectedsongBtn:nil];
    [self setUploadBtn:nil];
    [self setSubmissionInfoLabel:nil];
    [self setUploadInfoLabel:nil];
    [self setSubmissionTableView:nil];
    [self setUploadTableView:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}



#pragma Internal Callbacks
-(void)setUI{
    selectedSongId = @"";
    
    submissionInfoLabel.font = KABEL_FONT(12);
    uploadInfoLabel.font = KABEL_FONT(15);
    
    submissionInfoLabel.numberOfLines   = 3;
    uploadInfoLabel.numberOfLines       = 3;
    
    submissionInfoLabel.textAlignment = UITextAlignmentCenter;
    uploadInfoLabel.textAlignment = UITextAlignmentCenter;
    
    submissionInfoLabel.textColor   = [UIColor whiteColor];
    uploadInfoLabel.textColor       = [UIColor whiteColor];
    
    selectedsongBtn.titleLabel.font = KABEL_FONT(18);
    uploadBtn.titleLabel.font       = KABEL_FONT(18);
    
    submissionTableView.backgroundColor = [UIColor clearColor];
    uploadTableView.backgroundColor     = [UIColor clearColor];
    
    submissionTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown.png"]];
    uploadTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown.png"]];
}

-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (songListConnection != nil) {
                             [songListConnection cancel];
                             songListConnection = nil;
                         }
                         if (uploadVideoConnection != nil) {
                             [uploadVideoConnection cancel];
                             uploadVideoConnection = nil;
                         }
                     }];
}

-(void)songListAsynchronousCall
{
    [self showProgressHudWithCancel:@"Songs" subtitle:@"Loading"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_LIST_LINK] cachePolicy:NO timeoutInterval:10.0];
    songListConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)uploadVideoToServer:(NSURL *)videoURL
{
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     selectedSongName, @"SongName",
     nil];
    [Flurry logEvent:@"Upload_Attempt" withParameters:storeParams];
    [[LocalyticsSession shared] tagEvent:@"Upload_Attempt" attributes:storeParams];
    
    [self showProgressHudWithCancel:@"Upload Video" subtitle:@"Processing"];

    NSData *webData = [NSData dataWithContentsOfURL:videoURL];
    NSString *postLength = [NSString stringWithFormat:@"%d", [webData length]];
    NSString *filename = @"video";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:UPLOAD_VIDEO_URL]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------127412016815782193391561906476";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];

    NSMutableData *postbody =[NSMutableData data];
    
    //1 "Song id"
    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"song_id\"\r\n\r\n\%@",selectedSongId] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //2 "upload_file"
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"upload_file\"; filename=\"%@.mp4\"", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[@"\r\nContent-Type: video/mp4\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:webData]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //3 "code"
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"code\"\r\n\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //4 "code"
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n%@", userID] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:postbody];
    
    uploadVideoConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}

-(void)showActionsheet:(NSString *)songName
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                        initWithTitle: [NSString stringWithFormat:@"upload video for %@ song",songName]
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Take Video",@"Choose Existing Video", nil];
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [uiActionSheetP showInView:self.view];
    }
    else
    {
        UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                        initWithTitle: [NSString stringWithFormat:@"upload video for %@ song",songName]
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Choose Existing Video", nil];
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [uiActionSheetP showInView:self.view];
    }
}

-(void)useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeMovie,nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
        imagePicker.allowsEditing = NO;
        imagePicker.videoMaximumDuration = 300;  //in Seconds
        [self presentModalViewController:imagePicker animated:YES];
    }
}

-(void)useExistingPhotoAlbum
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =  [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeMovie,nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
    }
}

#pragma UIActionSheet delegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == [actionSheet numberOfButtons]-1)
        return;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Take Video"])
    {
        [self useCamera];
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] && [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Choose Existing Video"])
    {
        [self useExistingPhotoAlbum];
    }
}

#pragma UIImagePickerController delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissModalViewControllerAnimated:YES];
    if ([mediaType isEqualToString:(NSString *) kUTTypeImage]) {
        return;
    }
    else if ([mediaType isEqualToString:(NSString *) kUTTypeMovie]) {
        NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        DebugLog(@"moviePath %@",moviePath);

        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(moviePath)) {
            UISaveVideoAtPathToSavedPhotosAlbum(moviePath, nil, nil, nil);
        }
        //upload video goes here
        uploadVideoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        DebugLog(@"videoURL %@",uploadVideoURL);
        [self performSelector:@selector(showVideoAlert) withObject:nil afterDelay:1.0];
    }
}
-(void)showVideoAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Do you want to upload this video?" delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alert.tag = 1;
    [alert show];
}

#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                DebugLog(@"SubMainView : Don't upload");
                break;
            }
            case 1:
            {
                DebugLog(@"SubMainView : upload");
                [self performSelector:@selector(uploadVideoToServer:) withObject:uploadVideoURL afterDelay:0.5];
            }
                break;
            default:
                break;
        }
        
    }
}


#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"SubMainView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];

    if(connection == songListConnection)
    {
        if(responseAsyncData != nil)
        {
            //NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            //DebugLog(@"SubMainView: \n result:%@\n\n", result);
            NSError* error;
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"SubMainView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (songsArray == nil) {
                        songsArray = [[NSMutableArray alloc] init];
                    }else{
                        [songsArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray) {
                        songObj = [[DMDSongObj alloc] init];
                        songObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                        songObj.songTitle = [songattributeDict objectForKey:@"Title"];
                        songObj.songMovie = [songattributeDict objectForKey:@"Movie"];
                        songObj.songChoreographer = [songattributeDict objectForKey:@"Choreographer"];
                        songObj.songPic = [songattributeDict objectForKey:@"Thumbnail"];
                        songObj.songLock = [songattributeDict objectForKey:@"Lock_Type"];
                        songObj.songCategory = [songattributeDict objectForKey:@"Category"];
                        songObj.songCategoryName = [songattributeDict objectForKey:@"Category_Name"];
                        //DebugLog(@"SubMainView: \nsongid: %@ , title: %@  , movie: %@  , choreographer: %@  , style: %@  , picture: %@\n", songObj.songId, songObj.songTitle, songObj.songMovie, songObj.songChoreographer, songObj.songStyle, songObj.songPic);
                        if(![[songattributeDict objectForKey:@"Category"] isEqualToString:@"5"])
                        {
                            [songsArray addObject:songObj];
                        }
                        songObj = nil;
                    }
                }
                [submissionTableView reloadData];
                [uploadTableView reloadData];
            }
        }
        responseAsyncData = nil;
        songListConnection=nil;
    }else if(connection == uploadVideoConnection)
    {
        NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                    length:[responseAsyncData length] encoding:NSUTF8StringEncoding];
        DebugLog(@"SubMainView: %@", result ) ;
        if(result != nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:result
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            if ([result hasPrefix:@"Video uploaded successfully."]) {
                NSDictionary *storeParams =
                [NSDictionary dictionaryWithObjectsAndKeys:
                 selectedSongName, @"SongName",
                 nil];
                [Flurry logEvent:@"Upload_Success" withParameters:storeParams];
                [[LocalyticsSession shared] tagEvent:@"Upload_Success" attributes:storeParams];

            }
        }
        responseAsyncData = nil;
        uploadVideoConnection=nil;
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:submissionTableView] || [tableView isEqual:uploadTableView]) {
        return [songsArray count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    static NSString *CellIdentifier;
    if ([tableView isEqual:submissionTableView])
    {
        CellIdentifier = @"submissionCell";
    }else{
        CellIdentifier = @"uploadCell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType              = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled     = YES;
        cell.selectionStyle             = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor        = [UIColor whiteColor];
        cell.textLabel.font             = KABEL_FONT(20);
        cell.textLabel.textAlignment    = UITextAlignmentCenter;
        cell.textLabel.shadowColor = [UIColor blackColor];
        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        cell.textLabel.adjustsFontSizeToFitWidth = YES;

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        cell.selectedBackgroundView = bgColorView;
        // //DebugLog(@"SubMainView: height %f width %f",cell.frame.size.height,cell.frame.size.width);
        
        UIImageView *seperatorImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,42, 320, 4)];
        seperatorImg.image = [UIImage imageNamed:@"dropdown_table_line.png"];
        seperatorImg.tag = 5;
        seperatorImg.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:seperatorImg];
        
    }
    cell.userInteractionEnabled = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    //DebugLog(@"SubMainView: songArray %@",[songArray objectAtIndex:indexPath.row]);
    DMDSongObj *tempsongObj  = [songsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",tempsongObj.songCategoryName,tempsongObj.songTitle];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:submissionTableView]) {
        DMDSongObj *tempsongObj  = (DMDSongObj *)[songsArray objectAtIndex:indexPath.row];
        DebugLog(@"SubMainView: submission : You selected %@",tempsongObj.songTitle);
        DMDSubmissionDetailViewController *songdetailController = [[DMDSubmissionDetailViewController alloc] initWithNibName:@"DMDSubmissionDetailViewController" bundle:nil];
        songdetailController.title = [NSString stringWithFormat:@"%@ - %@",tempsongObj.songCategoryName,tempsongObj.songTitle];
        songdetailController.selectedSongId = tempsongObj.songId;
        songdetailController.selectedSongName = [NSString stringWithFormat:@"%@ - %@",tempsongObj.songCategoryName,tempsongObj.songTitle];
        [self.navigationController pushViewController:songdetailController animated:YES];
    }
    else if ([tableView isEqual:uploadTableView]) {
        DMDSongObj *tempsongObj  = (DMDSongObj *)[songsArray objectAtIndex:indexPath.row];
        DebugLog(@"SubMainView: upload: You selected %@",tempsongObj.songTitle);
        selectedSongId = tempsongObj.songId;
        selectedSongName = [NSString stringWithFormat:@"%@ - %@",tempsongObj.songCategoryName,tempsongObj.songTitle];;
        [self showActionsheet:tempsongObj.songTitle];
    }
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

#pragma UIButton delegate Method

- (IBAction)selectedsongBtnClicked:(id)sender {
}

- (IBAction)uploadBtnClicked:(id)sender {
}
@end
