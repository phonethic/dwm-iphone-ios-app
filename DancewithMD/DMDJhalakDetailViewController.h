//
//  DMDJhalakDetailViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MediaPlayer/MediaPlayer.h>

@class DMDSongDetailObj;
@interface DMDJhalakDetailViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    NSMutableData *responseAsyncData;
    int status;
    
    NSURLConnection *songDetailConnection;
    NSURLConnection *songDetailLockConnection;
    NSURLConnection *songStreamRequestConnection;
    NSURLConnection *unlockSongConnection;
    
    DMDSongDetailObj *songDetailObj;
    
    int small;
    int fullscreen;
    int close;

}
@property (strong, nonatomic) MPMoviePlayerViewController *movieController;

@property (nonatomic, copy) NSString *songID;
@property (nonatomic, copy) NSString *songName;
@property (nonatomic, copy) NSString *seenCount;
@property (nonatomic, copy) NSString *songLockType;
@property (nonatomic, readwrite) int selectedsongindex;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIWebView *videowebView;

@property (strong, nonatomic) NSMutableArray *jhalakSongDetailArray;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIButton *closeVideoBtn;
@property (strong, nonatomic) IBOutlet UIImageView *testimageView;
- (IBAction)closeVideoBtnClicked:(id)sender;
@end
