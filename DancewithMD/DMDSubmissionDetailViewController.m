//
//  DMDSubmissionDetailViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 07/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import "DMDAppDelegate.h"

#import "DMDSubmissionDetailViewController.h"
#import "DMDSubmittedVideosObj.h"
#import "CommonCallBack.h"

#define LATEST_SUBMISSION_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/retrieve/submissions/%@/latest",LIVE_DWM_SERVER,SONGID]

#define VOTE_LINK [NSString stringWithFormat:@"%@/api/retrieve/setvotes",LIVE_DWM_SERVER]


@interface DMDSubmissionDetailViewController ()

@end

@implementation DMDSubmissionDetailViewController
@synthesize selectedSongId;
@synthesize videoArray;
@synthesize carousel;
@synthesize closeVideoBtn;
@synthesize selectedvideoUserId,selectedvideoId;
@synthesize voteView,voteImageView,lblvoteHeader,lblVoteUsername,voteBtn;
@synthesize selectedSongName;
@synthesize movieController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma view lifeCycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (videoArray.count > 0) {
        [self showVoteView:YES];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     selectedSongName, @"SongName",
     nil];
    [Flurry logEvent:@"View_Submission" withParameters:storeParams];
    [[LocalyticsSession shared] tagEvent:@"View_Submission" attributes:storeParams];
    [[LocalyticsSession shared] tagScreen:@"View_Submission"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self videoListAsynchronousCall:selectedSongId];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    if (submissionConnection != nil) {
        [submissionConnection cancel];
        submissionConnection = nil;
    }
    if (voteConnection != nil) {
        [voteConnection cancel];
        voteConnection = nil;
    }
    if(self.movieController != nil)
    {
        [movieController.moviePlayer stop];
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
}
- (void)viewDidUnload {

    [self setCarousel:nil];
    [self setCloseVideoBtn:nil];
    [self setVoteBtn:nil];
    [self setVoteView:nil];
    [self setVoteImageView:nil];
    [self setLblvoteHeader:nil];
    [self setLblVoteUsername:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;//(interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;//UIInterfaceOrientationMaskLandscape;
    
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        lblVoteUsername.frame = CGRectMake(lblVoteUsername.frame.origin.x, lblVoteUsername.frame.origin.y,248, lblVoteUsername.frame.size.height);
    }else{
     lblVoteUsername.frame = CGRectMake(lblVoteUsername.frame.origin.x, lblVoteUsername.frame.origin.y,145, lblVoteUsername.frame.size.height);
    }
}


#pragma Internal Callbacks
-(void)setUI
{
    fullscreen = 0;
    carousel.type = iCarouselTypeCoverFlow;
    [self.closeVideoBtn setHidden:TRUE];
    
    voteView.backgroundColor = [UIColor clearColor];
    [voteView setAlpha:0.0];
    
    voteImageView.image = [UIImage imageNamed:@"guruspeak_box.png"];
    
    lblvoteHeader.backgroundColor = [UIColor clearColor];
    lblvoteHeader.font = KABEL_FONT(25);
    lblvoteHeader.textColor = [UIColor whiteColor];
    lblvoteHeader.textAlignment = UITextAlignmentCenter;
    lblvoteHeader.text = @"Vote";
    
    lblVoteUsername.backgroundColor = [UIColor clearColor];
    lblVoteUsername.font = KABEL_FONT(20);
    lblVoteUsername.adjustsFontSizeToFitWidth = YES;
    lblVoteUsername.textColor = [UIColor whiteColor];
    lblVoteUsername.textAlignment = UITextAlignmentLeft;
    lblVoteUsername.numberOfLines = 3;
    
}
-(void)showVoteView:(BOOL)show
{
    if (show) {
        [voteBtn setHighlighted:FALSE];
        [voteBtn setUserInteractionEnabled:TRUE];
        [UIView animateWithDuration:0.5 animations:^(void) {
            [voteView setAlpha:1.0];
        }
                         completion:^(BOOL finished) {
                         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^(void) {
            [voteView setAlpha:0.0];
        }
                         completion:^(BOOL finished) {
                         }];
    }
}
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (submissionConnection != nil) {
                             [submissionConnection cancel];
                             submissionConnection = nil;
                         }
                         if (voteConnection != nil) {
                             [voteConnection cancel];
                             voteConnection = nil;
                         }
                     }];
    
}

-(void)videoListAsynchronousCall:(NSString *)songID
{
    [self showProgressHudWithCancel:[NSString stringWithFormat:@"%@\nVideos",selectedSongName] subtitle:@"Loading"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:LATEST_SUBMISSION_LIST_LINK(songID)] cachePolicy:NO timeoutInterval:10.0];
    submissionConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)voteSetAsynchronousCall:(NSString *)videoId videouserID:(NSString *)vuserId
{
    [self showProgressHudWithCancel:@"Vote" subtitle:@"Processing"];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:VOTE_LINK] cachePolicy:NO timeoutInterval:30.0];
    DebugLog(@" SubDetails: URL=%@",urlRequest.URL);
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userID forKey:@"user_id"];
    [postReq setObject:videoId forKey:@"video_id"];   //Video_ID
    [postReq setObject:vuserId forKey:@"video_user"]; //User_ID
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        DebugLog(@" SubDetails: Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@" SubDetails: post : ---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    voteConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)showVideoView
{
//    backgroundImageView.image = [UIImage imageNamed:@"BG_zoomed_out.jpg"];
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.8f;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade;
//    
//    [backgroundImageView.layer addAnimation:transition forKey:nil];
//    
//    [videoView setHidden:TRUE];
//    [videoView  setAlpha:0.0];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.5];
//    [lightspotImageView setAlpha:0.0];
//    [UIView commitAnimations];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.4];
//    [videoView setAlpha:1.0];
//    [UIView commitAnimations];
//    
//    CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//    shrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    shrink.fromValue = [NSNumber numberWithDouble:1.5];
//    shrink.toValue = [NSNumber numberWithDouble:1.0];
//    shrink.duration = 0.8;
//    shrink.fillMode=kCAFillModeForwards;
//    shrink.removedOnCompletion=NO;
//    [CATransaction setCompletionBlock:^{
//        [moviePlayer.view setHidden:FALSE];
//    }];
//    //unshrink.delegate = self;
//    [videoView.layer addAnimation:shrink forKey:@"shrink"];
}

-(void)hideVideoView {
//    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.8f;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade;
//    [backgroundImageView.layer addAnimation:transition forKey:nil];
//    
//    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
//    unshrink.toValue = [NSNumber numberWithDouble:1.5];
//    unshrink.duration = 0.8;
//    unshrink.fillMode = kCAFillModeForwards;
//    unshrink.removedOnCompletion = NO;
//    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.6];
//    [videoView setAlpha:0.0];
//    [UIView commitAnimations];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.5];
//    [lightspotImageView setAlpha:1.0];
//    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    //[videoView setHidden:TRUE];
}

-(void)playVideo:(NSString *)videoUrl
{
    if(self.movieController != nil) {
        [movieController.moviePlayer stop];
        // Remove the movie player view controller from the "playback did finish" notification observers
        [[NSNotificationCenter defaultCenter] removeObserver:movieController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
    
    // Initialize the movie player view controller with a video URL string
    UIGraphicsBeginImageContext(CGSizeMake(1,1));
    movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    UIGraphicsEndImageContext();
    
    // Register this class as an observer instead
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieController.moviePlayer];
    
    // Set the modal transition style of your choice
    movieController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    // Present the movie player view controller
    //    [self presentModalViewController:movieController animated:YES];
    [self presentMoviePlayerViewControllerAnimated:movieController];
    
    // Start playback
    [movieController.moviePlayer prepareToPlay];
    [movieController.moviePlayer play];
}

#pragma MPMoviePlayerController callbacks
- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    DebugLog(@"LearnDanceDetailView: moviePlayBackDidFinish");
    // Obtain the reason why the movie playback finished
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([finishReason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonPlaybackEnded");
        }
            break;
        case MPMovieFinishReasonPlaybackError:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonPlaybackError");
            
        }
            break;
        case MPMovieFinishReasonUserExited:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonUserExited");
            
        }
            break;
        default:
            break;
    }
    // Remove this class from the observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:movieController.moviePlayer];
    
    [self dismissMoviePlayerViewControllerAnimated];
    movieController = nil;
    [self hideVideoView];
    [self.closeVideoBtn setHidden:TRUE];
}

#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@" SubDetails: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if(connection == submissionConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@" SubDetails: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (videoArray == nil) {
                        videoArray = [[NSMutableArray alloc] init];
                    }else{
                        [videoArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* videoattributeDict in dataArray)
                    {
                        if(![[videoattributeDict valueForKey:@"data"] isEqualToString:@"0"])
                        {   videosObj = [[DMDSubmittedVideosObj alloc] init];
                            videosObj.videoId = [videoattributeDict objectForKey:@"Video_ID"];
                            videosObj.userId = [videoattributeDict objectForKey:@"User_ID"];
                            videosObj.userName = [videoattributeDict objectForKey:@"User_Name"];
                            videosObj.userPic = [videoattributeDict objectForKey:@"Picture"];
                            videosObj.songId = [videoattributeDict objectForKey:@"Song_ID"];
                            videosObj.videoLink =  [videoattributeDict objectForKey:@"Mp4_Link"];
                            videosObj.votes = [videoattributeDict objectForKey:@"Votes"];
                            ////DebugLog(@" SubDetails: \n videoid: %@ , userid: %@ , username: %@  , image: %@  , songid: %@  , link: %@ , votes: %@ \n", videosObj.videoId, videosObj.userId, videosObj.userName, videosObj.userPic, videosObj.songId, videosObj.videoLink, videosObj.votes);
                            [videoArray addObject:videosObj];
                            videosObj = nil;
                        }
                    }
                }
                DebugLog(@" SubDetails: videoArray array count: %d", [videoArray count]);
                if (videoArray.count > 0) {
                    [carousel reloadData];
                    DMDSubmittedVideosObj *tempvideoObj = (DMDSubmittedVideosObj *)[videoArray objectAtIndex:0];
                    selectedvideoId = tempvideoObj.videoId;
                    selectedvideoUserId = tempvideoObj.userId;
                    lblVoteUsername.text = tempvideoObj.userName;
                    [self showVoteView:YES];
                }else{
                    [self showVoteView:NO];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                        message:@"No videos found.\nPlease upload your video in this dance style to be the first one."
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
        }
        responseAsyncData = nil;
        submissionConnection = nil;
    }else  if(connection == voteConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@" SubDetails: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                NSString *message;
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if([json objectForKey:@"data"] != nil) {
                        message = [json objectForKey:@"data"];
                    }
                } else {
                    message = [json objectForKey:@"message"];
                }
                if(message != nil && ([message hasPrefix:@"Thank you for voting"] || [message hasPrefix:@"You already voted"]))
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                        message:message
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                    [voteBtn setHighlighted:TRUE];
                    [voteBtn setUserInteractionEnabled:FALSE];
                    if ([message hasPrefix:@"Thank you for voting"]) {
                        NSDictionary *storeParams =
                        [NSDictionary dictionaryWithObjectsAndKeys:
                         selectedSongName, @"SongName",
                         lblVoteUsername.text, @"UserNameToVote",
                         selectedvideoId, @"videoId",
                         nil];
                        [Flurry logEvent:@"Vote" withParameters:storeParams];
                        [[LocalyticsSession shared] tagEvent:@"Vote" attributes:storeParams];
                    }
                }
            }
        }
        responseAsyncData = nil;
        voteConnection = nil;
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [videoArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *nameLabel = nil;
    UILabel *votesLabel = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"users_video_icon.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 105, 135, 50)];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textAlignment = UITextAlignmentCenter;
        nameLabel.textColor = GRAY_TEXT_COLOR;
        nameLabel.font = KABEL_FONT(15);
        nameLabel.adjustsFontSizeToFitWidth = YES;
        nameLabel.tag = 1;
        [view addSubview:nameLabel];
        
        votesLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(nameLabel.frame), 105, 35, 50)];
        votesLabel.backgroundColor = [UIColor clearColor];
        votesLabel.textAlignment = UITextAlignmentRight;
        votesLabel.textColor = GRAY_TEXT_COLOR;
        votesLabel.font = KABEL_FONT(15);
        votesLabel.adjustsFontSizeToFitWidth = YES;
        votesLabel.tag = 2;
        [view addSubview:votesLabel];
    }
    else
    {
        //get a reference to the label in the recycled view
        nameLabel = (UILabel *)[view viewWithTag:1];
        votesLabel = (UILabel *)[view viewWithTag:2];
        
    }
    
    ((UIImageView *)view).image = [UIImage imageNamed:@"users_video_icon.png"];
    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }

    DMDSubmittedVideosObj *tempvideoObj = (DMDSubmittedVideosObj *)[videoArray objectAtIndex:index];
    nameLabel.text = tempvideoObj.userName;
    votesLabel.text = tempvideoObj.votes;
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    [self showVoteView:NO];

    DMDSubmittedVideosObj *tempvideoObj = (DMDSubmittedVideosObj *)[videoArray objectAtIndex:index];
    //DebugLog(@" SubDetails: video url=%@",tempvideoObj.videoLink);
    selectedvideoId = tempvideoObj.videoId;
    selectedvideoUserId = tempvideoObj.userId;
    lblVoteUsername.text = tempvideoObj.userName;

//    titlelbl.text = tempvideoObj.userName;
//    namelbl.text = [NSString stringWithFormat:@"Votes             %@",tempvideoObj.votes];

//    [self showVideoView];
    if (small==0 || small==2) {
        small = 1;
    } else if(small==1){
        small = 2;
    }
//
//    self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//    
//    [videoView setHidden:FALSE];
//    screenshotView.hidden = FALSE;
//    
//    UIGraphicsBeginImageContext(videoboxImageView.frame.size);
//    [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//    screenshotView.image = viewImage;
//    UIGraphicsEndImageContext();
//    //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
//    
//    
//    // determine the size of the reflection to create
//    NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
//    
//    // create the reflection image and assign it to the UIImageView
//    self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
//    //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
//    self.reflectionView.alpha = kDefaultReflectionOpacity;
//    
//    screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//    screenshotView.hidden = TRUE;
//    
//    [videoView setHidden:FALSE];
//    
    [self.carousel reloadData];
    [self playVideo:tempvideoObj.videoLink];
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}
- (void)carouselWillBeginDragging:(iCarousel *)lcarousel
{
    //DebugLog(@"ExpertView: carouselWillBeginDragging %d",lcarousel.currentItemIndex);
    [self showVoteView:NO];
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)lcarousel{
    //DebugLog(@"ExpertView: carouselCurrentItemIndexDidChange %d",lcarousel.currentItemIndex);
    DMDSubmittedVideosObj *tempvideoObj = (DMDSubmittedVideosObj *)[videoArray objectAtIndex:lcarousel.currentItemIndex];
    self.lblVoteUsername.text = tempvideoObj.userName;
    selectedvideoId = tempvideoObj.videoId;
    selectedvideoUserId = tempvideoObj.userId;
    [self showVoteView:YES];
}
- (void)carouselDidEndDragging:(iCarousel *)lcarousel willDecelerate:(BOOL)decelerate
{
    //DebugLog(@"ExpertView: carouselDidEndDragging %d",lcarousel.currentItemIndex);
    [self showVoteView:YES];
}

#pragma UIButton delegate Method
- (IBAction)closeVideoBtnClicked:(id)sender {
    DebugLog(@"closeVideoBtnClicked");
    [self hideVideoView];
    [self.closeVideoBtn setHidden:TRUE];
    [self showVoteView:YES];
}

- (IBAction)voteBtnClicked:(id)sender {
    DebugLog(@"voteBtnClicked");
    if(selectedSongId == nil || selectedvideoUserId == nil || [selectedvideoId isEqualToString:@""] || [selectedvideoId isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please choose a video to vote."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
        DebugLog(@" SubDetails: -%@-%@-",selectedvideoId,selectedvideoUserId);
        [self voteSetAsynchronousCall:selectedvideoId videouserID:selectedvideoUserId];
    }
}
@end
