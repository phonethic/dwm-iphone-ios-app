//
//  DMDHomeViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDHomeViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDLoginViewController.h"
#import "PlayVideoViewController.h"

#define VIDEO_ENGLISH_LINK @"http://s3.amazonaws.com/dance-with-md-new/opening/mp4/360/Site_Intro_English.mp4"
#define VIDEO_HINDI_LINK @"http://s3.amazonaws.com/dance-with-md-new/opening/mp4/360/Site_Intro_Hindi.mp4"

@implementation UITextView (DisableCopyPaste)
- (BOOL)canBecomeFirstResponder
{
    return NO;
}
@end

@interface DMDHomeViewController ()

@end

@implementation DMDHomeViewController
@synthesize englishBtn,hindiBtn;
@synthesize moviePlayer;
@synthesize helptextView;
@synthesize welcomelbl;
@synthesize blackView;
@synthesize videoImageView,infoscreen;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Home";
    }
    return self;
}
#pragma view lifeCycle
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    NSString *userPass = [defaults objectForKey:LOGIN_PASSWORD];
    
    if (FBSession.activeSession.isOpen) //User logged-in via Facebook
    {
        self.navigationController.navigationBarHidden = NO;
        [blackView setHidden:TRUE];
//        if(self.moviePlayer==nil) {
//            [self performSelector:@selector(playVideo:) withObject:VIDEO_ENGLISH_LINK afterDelay:0.0];
//        }
    }
    else if ((userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0)) //User logged-in via own server
    {
        self.navigationController.navigationBarHidden = NO;
        [blackView setHidden:TRUE];
        NSDate *oldDate = [defaults objectForKey:LOGIN_EXPIREDDATE];
        NSDate *currentDate = [NSDate date];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                            fromDate:oldDate
                                                              toDate:currentDate
                                                             options:0];
        //DebugLog(@"days = %d",[components day]);
        //DebugLog(@"-----oldDate %@ --currentDate %@--",oldDate,currentDate);
        
        //Using below check we are authenticating or verifying user on our own server daily.
        if([components day] != 0) // Compare expiration dates. if greater than one then users session timeout occurs.
        {
            DebugLog(@"Login called");
            [self dmdfbLoginPressed:nil];
        } else {
//            if(self.moviePlayer==nil) {
//                [self performSelector:@selector(playVideo:) withObject:VIDEO_ENGLISH_LINK afterDelay:0.0];
//            }
        }
    }
    else //User not logged-in via facebook or own server
    {
        //DebugLog(@"activesession called");
        [self dmdfbLoginPressed:nil];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_Home"];
    [[LocalyticsSession shared] tagScreen:@"Home"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.tintColor = BLACK_COLOR;
    self.navigationController.navigationBarHidden = YES;
    [blackView setHidden:FALSE];
    
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];
    [self setUI];
    [englishBtn setSelected:TRUE];
    videoImageView.image = [UIImage imageNamed:@"English_Intro.png"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isShown = [[defaults objectForKey:LOGIN_HELPINFOSCREEN] boolValue];
    if (isShown) {
        self.infoscreen.hidden = TRUE;
    }else{
        self.infoscreen.hidden = FALSE;
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHelpInfoScreen)];
        gestureRecognizer.cancelsTouchesInView=NO;
        [self.infoscreen addGestureRecognizer:gestureRecognizer];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    if(self.moviePlayer != nil)
    {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}
- (void)viewDidUnload {
    [self setEnglishBtn:nil];
    [self setHindiBtn:nil];
    [self setHelptextView:nil];
    [self setWelcomelbl:nil];
    [self setBlackView:nil];
    [self setVideoImageView:nil];
    [self setInfoscreen:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
    
}


#pragma Internal Callbacks
-(void)setUI
{

    welcomelbl.font = KABEL_FONT(16);
    helptextView.font = KABEL_FONT(15);
    
    welcomelbl.textColor = [UIColor whiteColor];
    helptextView.textColor = [UIColor whiteColor];

    welcomelbl.backgroundColor = [UIColor clearColor];
    helptextView.backgroundColor = [UIColor clearColor];
    
    welcomelbl.text = @"Hello & Welcome to my Dance Academy!";
    helptextView.indicatorStyle = UIScrollViewIndicatorStyleWhite;

}
- (void) hideHelpInfoScreen {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:1] forKey:LOGIN_HELPINFOSCREEN];
    [defaults synchronize];
    [UIView animateWithDuration:1.0 delay:0.0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
                     {
                         self.infoscreen.alpha = 0.0f;
                     }
                     completion:^(BOOL finished)
                     {
                         self.infoscreen.hidden = YES;
                     }];
}

-(void)playVideo:(NSString *) videoUrl {
    
    PlayVideoViewController *playVideoController = [[PlayVideoViewController alloc] initWithNibName:@"PlayVideoViewController" bundle:nil];
    playVideoController.urlString = videoUrl;
    [self.navigationController pushViewController:playVideoController animated:YES];
    return;
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    self.moviePlayer.view.frame = CGRectMake(26, 64, 269, 150);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    self.moviePlayer.controlStyle = MPMovieControlStyleDefault;
    if (self.infoscreen.hidden) {
        self.moviePlayer.shouldAutoplay = YES;
    }else{
        self.moviePlayer.shouldAutoplay = NO;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    [self.view addSubview:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    [moviePlayer play];
}
- (void) movieEnterFullScreen:(NSNotification*)notification {
    fullscreen = 1;
}
- (void) movieExitFullScreen:(NSNotification*)notification {
    fullscreen = 0;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma UIButton delegate Method
- (void)dmdfbLoginPressed:(id)sender {
    DMDLoginViewController *loginViewComposer = [[DMDLoginViewController alloc] initWithNibName:@"DMDLoginViewController" bundle:nil];
    [self.navigationController presentModalViewController:loginViewComposer animated:YES];
}

- (IBAction)englishBtnPressed:(id)sender {
    [englishBtn setSelected:TRUE];
    [hindiBtn setSelected:FALSE];
    videoImageView.image = [UIImage imageNamed:@"English_Intro.png"];
    //    [self playVideo:VIDEO_ENGLISH_LINK];
}

- (IBAction)hindiBtnPressed:(id)sender {
   
    [englishBtn setSelected:FALSE];
    [hindiBtn setSelected:TRUE];
    videoImageView.image = [UIImage imageNamed:@"Hindi_Intro.png"];

//    [self playVideo:VIDEO_HINDI_LINK];
}

- (IBAction)playVideoBtnClicked:(id)sender {
    
    if (englishBtn.selected) {
        NSDictionary *storeParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         @"English", @"Video_type",
         nil];
        [Flurry logEvent:@"Intro_Video" withParameters:storeParams];
        [[LocalyticsSession shared] tagEvent:@"Intro_Video" attributes:storeParams];
        [self playVideo:VIDEO_ENGLISH_LINK];
    }else{
        NSDictionary *storeParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         @"Hindi", @"Video_type",
         nil];
        [Flurry logEvent:@"Intro_Video" withParameters:storeParams];
        [[LocalyticsSession shared] tagEvent:@"Intro_Video" attributes:storeParams];
        [self playVideo:VIDEO_HINDI_LINK];
    }
}
@end
