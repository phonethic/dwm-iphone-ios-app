//
//  DMDSplashVideoViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 08/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSplashVideoViewController.h"
#import "DMDAppDelegate.h"

@interface DMDSplashVideoViewController ()

@end

@implementation DMDSplashVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self playVideo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
      return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}


#pragma Internal Callbacks
-(void)playVideo
{
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"test1" ofType:@"mp4"]];
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    DebugLog(@"%f",self.view.frame.size.height);
    moviePlayer.view.frame = CGRectMake(0, 0, self.view.bounds.size.height,self.view.bounds.size.width);
    
    moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:moviePlayer];
    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
    {
    }
    else
    {
        DebugLog(@"Got ios 6");
        CGRect frame = [[UIScreen mainScreen] applicationFrame];
        [moviePlayer.view setBounds:CGRectMake(0, 0, self.view.bounds.size.height, self.view.bounds.size.width)];
        [moviePlayer.view setCenter:CGPointMake(frame.size.width/2, (frame.size.height/2)
                                                )];
        
        if(self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            //DebugLog(@"UIInterfaceOrientationLandscapeLeft");
            [moviePlayer.view setTransform:CGAffineTransformMakeRotation(M_PI / -2)];
        }
        if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            //DebugLog(@"UIInterfaceOrientationLandscapeRight");
            [moviePlayer.view setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
        }
    }
    
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = YES;
    [moviePlayer setFullscreen:YES animated:NO];
    [moviePlayer prepareToPlay];
    [self.view addSubview:moviePlayer.view];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(moviePlayer != nil)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerDidExitFullscreenNotification
                                                      object:moviePlayer];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        [moviePlayer stop];
        [moviePlayer.view removeFromSuperview];
        [DMD_APP_DELEGATE animationStop];
    }
}


- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerDidExitFullscreenNotification
                                                  object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    
    [(DMDAppDelegate *)[[UIApplication sharedApplication] delegate] animationStop];
}


@end
