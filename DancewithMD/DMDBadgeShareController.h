//
//  DMDBadgeShareController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 21/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface DMDBadgeShareController : UIViewController <FBUserSettingsDelegate,UITextViewDelegate>

@property (nonatomic, copy) NSString *FBtitle;
@property (nonatomic, copy) NSString *FBtLink;
@property (nonatomic, copy) NSString *FBtCaption;
@property (nonatomic, copy) NSString *FBtPic;

@property (strong, nonatomic) NSMutableDictionary *postParams;

@property (strong, nonatomic) IBOutlet UIButton *facebookLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelFBBtn;

@property (strong, nonatomic) IBOutlet UIView *mainFBView;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *loginFBBtn;
@property (strong, nonatomic) IBOutlet UIButton *postFBBtn;
@property (strong, nonatomic) IBOutlet UITextView *postMessageTextView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;

- (IBAction)facebookLoginBtnClicked:(id)sender;
- (IBAction)cancelFBBtnClicked:(id)sender;
- (IBAction)postFBBtnClicked:(id)sender;
@end
