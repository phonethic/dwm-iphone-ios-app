//
//  DMDAppDelegate.h
//  DancewithMD
//
//  Created by Kirti Nikam on 10/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#import "Reachability.h"
#import <FacebookSDK/FacebookSDK.h>


#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define DMD_APP_DELEGATE (DMDAppDelegate*)[[UIApplication sharedApplication] delegate]

extern NSString *const FBSessionStateChangedNotification;


@class DMDSplashVideoViewController;
@interface DMDAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DMDSplashVideoViewController *splashviewController;

//facebook properties
@property (strong, nonatomic) NSString *loggedInUserID;
@property (strong, nonatomic) FBSession *loggedInSession;


-(NSString*)URLEncode:(NSString*) string;
-(void)animationStop;
-(NSString *)doCapitalFirstLetter:(NSString *)string;
- (NSString *) md5:(NSString *) input;


//facebook callbacks
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void) closeSession;
@end
