//
//  DMDFBViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDFBViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "CommonCallBack.h"

#define FACEBOOK_LINK @"http://m.facebook.com/DanceWithMadhuriOfficial"

@interface DMDFBViewController ()
@end

@implementation DMDFBViewController
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_FaceBook"];
    [[LocalyticsSession shared] tagScreen:@"FaceBook"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];
    [_webviewtoolBar setHidden:TRUE];
    [webView setHidden:TRUE];
    
    if([DMD_APP_DELEGATE networkavailable]) {
        NSURL *url = [NSURL URLWithString:FACEBOOK_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView setDelegate:self];
        [webView loadRequest:requestObj];
        [webView setHidden:FALSE];
        [_webviewtoolBar setHidden:FALSE];
        [_barbackBtn setEnabled:FALSE];
        [_barfwdBtn setEnabled:FALSE];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [webView stopLoading];
} 

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setWebviewtoolBar:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [super viewDidUnload];
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
    
}
#pragma internal methods
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [webView stopLoading];
                     }];
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma webView delegate Methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //DebugLog(@" FaceBook: request : %@",webView.request.URL);
    [self showProgressHudWithCancel:@"Facebook" subtitle:@"Loading"];
    [_barrefreshBtn setEnabled:FALSE];
}

- (void)webViewDidFinishLoad:(UIWebView *)lwebView
{
    [CommonCallBack hideProgressHud];
    if(webView.canGoBack)
    {
        [_barbackBtn setEnabled:TRUE];
    } else {
        [_barbackBtn setEnabled:FALSE];
    }
    if(webView.canGoForward)
    {
        [_barfwdBtn setEnabled:TRUE];
    } else {
        [_barfwdBtn setEnabled:FALSE];
    }
    [_barrefreshBtn setEnabled:TRUE];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [CommonCallBack hideProgressHudWithError];
    [_barrefreshBtn setEnabled:TRUE];
}

#pragma UIButton delegate Method
- (IBAction)webviewbackBtnPressed:(id)sender {
    [webView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [webView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [webView reload];
}
@end
