//
//  DMDBadgeObj.m
//  DancewithMD
//
//  Created by Sagar Mody on 24/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDBadgeObj.h"

@implementation DMDBadgeObj

@synthesize songId;
@synthesize clip_partId;
@synthesize shareId;
@synthesize sharePic;
@synthesize badgeName;
@synthesize shareText;

@end
