//
//  DMDAppDelegate.m
//  DancewithMD
//
//  Created by Kirti Nikam on 10/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDAppDelegate.h"
#import "DMDSplashVideoViewController.h"
#import "DMDHomeViewController.h"
#import "SideMenuViewController.h"
#import "Flurry.h"
#import "Appirater.h"
#import <Parse/Parse.h>
#import <CommonCrypto/CommonDigest.h>
#import "LocalyticsSession.h"

NSString *const FBSessionStateChangedNotification = @"com.facebook.MadhuriDMD:MSessionStateChangedNotification";

@implementation DMDAppDelegate
@synthesize networkavailable;
@synthesize splashviewController;
@synthesize window = _window;
@synthesize loggedInUserID = _loggedInUserID;
@synthesize loggedInSession = _loggedInSession;
/*
 * Callback for session changes.
 */
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // We have a valid session
                //DebugLog(@"User session found");
                [FBRequestConnection
                 startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                                   NSDictionary<FBGraphUser> *user,
                                                   NSError *error) {
                     if (!error) {
                         self.loggedInUserID = user.id;
                         self.loggedInSession = FBSession.activeSession;
                     }
                 }];
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

/*
 * Opens a Facebook session and optionally shows the login UX.
 */
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
     NSArray *permissions = @[@"basic_info", @"email"];
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
}

/*
 *
 */
- (void) closeSession {
    [FBSession.activeSession closeAndClearTokenInformation];
}

/*
 * If we have a valid session at the time of openURL call, we handle
 * Facebook transitions by passing the url argument to handleOpenURL
 */
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

#pragma sidemenu callbacks
- (DMDHomeViewController *)dmdController {
    return [[DMDHomeViewController alloc] initWithNibName:@"DMDHomeViewController" bundle:nil];
}

- (UINavigationController *)navigationController {
    return [[UINavigationController alloc]
            initWithRootViewController:[self dmdController]];
}

- (MFSideMenu *)sideMenu
{
    SideMenuViewController *rightSideMenuController = [[SideMenuViewController alloc] init];
    UINavigationController *navigationController = [self navigationController];
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:nil
                                            rightSideMenuController:rightSideMenuController];
    
    rightSideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}

- (void) setupNavigationControllerApp {
    self.window.rootViewController = [self sideMenu].navigationController;
    [self.window makeKeyAndVisible];
}

#pragma interfaceorienttion callback
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    if (self.window.rootViewController)
    {
        if ([self.window.rootViewController isKindOfClass:[DMDSplashVideoViewController class]])
        {
            orientations = [self.window.rootViewController supportedInterfaceOrientations];
        }
        else {
        //    DebugLog(@"self.window.rootViewController %@",self.window.rootViewController);
        //    DebugLog(@"self.window.rootViewController.modalViewController %@",self.window.rootViewController.modalViewController);
            if([self.window.rootViewController.modalViewController isKindOfClass:MPMoviePlayerViewController.class ]){
         //       DebugLog(@"got MPMoviePlayerViewController");
              orientations = UIInterfaceOrientationMaskAll;
            }else{
                UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
          //      DebugLog(@"presented %@",presented);
                orientations = [presented supportedInterfaceOrientations];
            }
        }
    }
    return orientations;
}

#pragma internal callback
-(void)setNavigationBarProperties
{
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor blackColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(18)
     }];
    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
     //                          UITextAttributeTextShadowColor: [UIColor blackColor],
     //                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(15.0)
     } forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
     //                          UITextAttributeTextShadowColor: [UIColor blackColor],
     //                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: KABEL_FONT(15.0)
     } forState:UIControlStateHighlighted];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -3.0) forBarMetrics:UIBarMetricsDefault];

}
-(void)animationStop
{
    [splashviewController.view removeFromSuperview];
    splashviewController = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self setupNavigationControllerApp];

    //    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
    //                     animations:^(void) {
    //                         BOOL oldState = [UIView areAnimationsEnabled];
    //                         [UIView setAnimationsEnabled:NO];
    //                         [self setupNavigationControllerApp];
    //                         [UIView setAnimationsEnabled:oldState];
    //                     }
    //                     completion:nil];
}
-(NSString*)URLEncode:(NSString*) string
{
    ////DebugLog(@"==BEFORE ENCODE=%@",string);
    NSMutableString *escaped = [[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] mutableCopy];
    [escaped replaceOccurrencesOfString:@"=" withString:@"%3D" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\"" withString:@"%22" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\n" withString:@"%0A" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    ////DebugLog(@"==AFTER ENCODE=%@",escaped);
    return escaped;
}

-(NSString *)doCapitalFirstLetter:(NSString *)string
{
    NSString *firstCapChar = [[string substringToIndex:1] capitalizedString];
    NSString *cappedString = [string stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    return cappedString;
}

- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}
#pragma Appirater Methods
-(void) setAppRateAlert
{
    [Appirater setAppId:APPIRATER_APPID];
    [Appirater setDaysUntilPrompt:2];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
    //[Appirater setDebug:YES];
}
#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    //DebugLog(@"network status = %d",networkavailable);
}


#pragma UIApplication callbacks
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@"rishi@phonethics.in" forKey:LOGIN_USERID];
//    [defaults setObject:@"123456" forKey:LOGIN_PASSWORD];
//    [defaults setObject:@"Rishi" forKey:LOGIN_USERNAME];
//    NSDate *myDate = [NSDate date];
//    [defaults setObject:myDate forKey:LOGIN_EXPIREDDATE];
//    [defaults synchronize];
    
    //Add Reachability
    [self startCheckNetwork];

    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    //[Flurry logEvent:@"Dance With Madhuri App Started"];
    
    [[LocalyticsSession shared] startSession:LOCALYTICS_KEY];
    //Add Appirater
    //[self setAppRateAlert];
    //[Appirater appLaunched:YES];
    
    //Add Parse
    [Parse setApplicationId:PARSE_APPID clientKey:PARSE_CLIENTKEY];
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    
    [self setNavigationBarProperties];

    //Add SlideMenu --- Without video
    //[self setupNavigationControllerApp];
    
    splashviewController = [[DMDSplashVideoViewController alloc] initWithNibName:@"DMDSplashVideoViewController" bundle:nil];
    [self.window addSubview:splashviewController.view];
    [self.window makeKeyAndVisible];
    
    return YES;
}

#pragma Parse Push Notification Methods
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    DebugLog(@"didRegisterForRemoteNotificationsWithDeviceToken : devicetoken %@",deviceToken);
    [PFPush storeDeviceToken:deviceToken];
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            DebugLog(@"Successfully subscribed to broadcast channel!");
        else
            DebugLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DebugLog(@"didFailToRegisterForRemoteNotificationsWithError");
    if (error.code == 3010) {
        DebugLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        DebugLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DebugLog(@"didReceiveRemoteNotification  : userInfo %@",userInfo);
    [PFPush handlePush:userInfo];
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        DebugLog(@"UIApplicationStateInactive");
    } else if (state == UIApplicationStateBackground){
        DebugLog(@"UIApplicationStateBackground");
    } else if (state == UIApplicationStateActive){
        DebugLog(@"UIApplicationStateActive");
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[LocalyticsSession shared] close];
    [[LocalyticsSession shared] upload];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[LocalyticsSession shared] close];
    [[LocalyticsSession shared] upload];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //[Appirater appEnteredForeground:YES];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[LocalyticsSession shared] resume];
    [[LocalyticsSession shared] upload];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // We need to properly handle activation of the application with regards to SSO
    // (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    [FBAppCall handleDidBecomeActive];
    [[LocalyticsSession shared] resume];
    [[LocalyticsSession shared] upload];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // if the app is going away, we close the session object
    [FBSession.activeSession close];
    [[LocalyticsSession shared] close];
    [[LocalyticsSession shared] upload];
}

@end
