//
//  DMDSongDetailViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MediaPlayer/MediaPlayer.h>
 #import <MediaPlayer/MPMoviePlayerViewController.h>

@class DMDSongDetailObj;
@class DMDBadgeObj;
@interface DMDSongDetailViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    DMDSongDetailObj *songDetailObj;

    NSMutableData *responseAsyncData;
    int status;
    
    NSURLConnection *songDetailConnection;
    NSURLConnection *songDetailLockConnection;
    NSURLConnection *songStreamRequestConnection;
    NSURLConnection *unlockSongConnection;
    
    DMDBadgeObj *badgeObj;
    NSURLConnection *badgeListConnection;
    NSURLConnection *setBadgeConnection;

    int small;
    int fullscreen;
    int close;
}
@property (nonatomic, readwrite) int selectedsongindex;

@property (nonatomic, copy) NSString *songID;
@property (nonatomic, copy) NSString *songName;
@property (nonatomic, copy) NSString *styleName;

@property (nonatomic, copy) NSString *seenCount;
@property (nonatomic, copy) NSString *selectedsongclipID;

@property (strong, nonatomic) NSMutableArray *songDetailArray;
@property (strong, nonatomic) NSMutableArray *badgeArray;

@property (strong, nonatomic) MPMoviePlayerViewController *movieController;

@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIButton *closeVideoBtn;

@property (strong, nonatomic) IBOutlet UIView *badgeView;
@property (strong, nonatomic) IBOutlet UIImageView *badgeImageView;
@property (strong, nonatomic) IBOutlet UILabel *badgelbl;
@property (strong, nonatomic) IBOutlet UIButton *fbShareBtn;
@property (strong, nonatomic) IBOutlet UIButton *closeBadgeViewBtn;



- (IBAction)closeVideoBtnClicked:(id)sender;
- (IBAction)fbShareBtnClicked:(id)sender;
- (IBAction)closeBadgeViewBtnClicked:(id)sender;

@end
