//
//  DMDFeedbackViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@class DMDFeedbackObj;
@interface DMDFeedbackViewController : UIViewController <MFMailComposeViewControllerDelegate>{
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *feedBackconnection;
    DMDFeedbackObj *feedbackObj;
}
@property (nonatomic, copy) NSString *dataFilePath;
@property (strong, nonatomic) NSMutableArray *feedbackArray;
@property (strong, nonatomic) IBOutlet UITableView *feedbackTableView;
@property (strong, nonatomic) IBOutlet UIView *feedbackView;
@property (strong, nonatomic) IBOutlet UITextView *feedbackTextView;
@property (strong, nonatomic) IBOutlet UIImageView *feedbackuserImageView;
@property (strong, nonatomic) IBOutlet UILabel *feedbackusernameLabel;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)closeBtnClicked:(id)sender;
@end
