//
//  DMDTweetsViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <Twitter/Twitter.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "DMDTweetsViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDTweetObj.h"
#import "CommonCallBack.h"

@interface DMDTweetsViewController ()

@end

@implementation DMDTweetsViewController
@synthesize tweetsArray;
@synthesize tweetsTableView;
@synthesize bearerToken;
@synthesize dataFilePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{  
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_Tweets"];
    [[LocalyticsSession shared] tagScreen:@"Tweets"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];
    [self settweetDataFilePath];

    UIBarButtonItem *tweetShareButton = [[UIBarButtonItem alloc] initWithTitle:@"Send Tweet" style:UIBarButtonItemStylePlain target:self action:@selector(send_tweet_Clicked)];
    self.navigationItem.leftBarButtonItem = tweetShareButton;
    
    tweetsArray = [[NSMutableArray alloc] init];
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self tweetListAsynchronousCall];
    } else {
        [self parseFromFile];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    if (connection1 != nil) {
        [connection1 cancel];
        connection1 = nil;
    }
    if (connection2 != nil) {
        [connection2 cancel];
        connection2 = nil;
    }
}

- (void)viewDidUnload {
    [self setTweetsTableView:nil];
    [super viewDidUnload];
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}



#pragma Internal Callbacks
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (connection1 != nil) {
                             [connection1 cancel];
                             connection1 = nil;
                         }
                         if (connection2 != nil) {
                             [connection2 cancel];
                             connection2 = nil;
                         }
                     }];
    
}

-(void)tweetListAsynchronousCall
{
//    [CommonCallBack showProgressHud:@"DanceWithMD\nTweets" subtitle:@"Loading"];
    [self showProgressHudWithCancel:@"#DancewithMadhuri\nTweets" subtitle:@"Loading"];

    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/oauth2/token"] cachePolicy:NO timeoutInterval:30.0];
    DebugLog(@"Tweets: %@",urlRequest.URL);
    
    //1. The request must be a HTTP POST request.
    [urlRequest setHTTPMethod:@"POST"];
    
    //2 .The request must include an Authorization header with the value of Basic <base64 encoded value from step 1>.
    NSString *base64EncodedTokens = [self base64EncodedBearerTokenCredentialsWithConsumerKey:@"63xdK9qQ3nORq0WOqiXw" consumerSecret:@"rrIgw30gSajYsPEJJ6Gi6dS9uS8RoS9vzePZlPJq6Qo"];
    
    NSString *authorization = [NSString stringWithFormat:@"Basic %@", base64EncodedTokens];
    [urlRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    
    //3. The request must include a Content-Type header with the value of application/x-www-form-urlencoded;charset=UTF-8.
    [urlRequest setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    
    //4. The body of the request must be grant_type=client_credentials.
    NSString *postParamsString = @"grant_type=client_credentials";
    
    NSData *requestData = [NSData dataWithBytes:[postParamsString UTF8String] length:[postParamsString length]];
    [urlRequest setHTTPBody: requestData];
    
    connection1 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

- (void)invalidateBearerToken
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json?count=10&q=%23dancewithmadhuri&result_type=mixed"] cachePolicy:NO timeoutInterval:30.0];
    DebugLog(@"Tweets: %@",urlRequest.URL);
    
    [urlRequest setHTTPMethod:@"GET"];
    
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", self.bearerToken];
    [urlRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    connection2 = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    
}

-(void)settweetDataFilePath
{
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the data file
    dataFilePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"tweets.archive"]];
}
-(void)parseFromFile
{
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        tweetsArray = [NSKeyedUnarchiver unarchiveObjectWithFile: dataFilePath];
        DebugLog(@"Tweets: from file tweetsArray %@",tweetsArray);
        
        if (tweetsArray.count <= 0) {
            [self showError];
        }else{
            [CommonCallBack hideProgressHud];
            [tweetsTableView reloadData];
        }
    }else
    {
        [self showError];
    }
}
-(void)showError
{
    [CommonCallBack hideProgressHudWithError];
    UIAlertView *errorView = [[UIAlertView alloc]
                              initWithTitle:@"No Network Connection"
                              message:@"Please check your internet connection and try again."
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [errorView show];
}


- (NSString *)base64EncodedBearerTokenCredentialsWithConsumerKey:(NSString *)consumerKey consumerSecret:(NSString *)consumerSecret {
    NSString *encodedConsumerToken = consumerKey ;//[consumerKey st_stringByAddingRFC3986PercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedConsumerSecret = consumerSecret;//[consumerSecret st_stringByAddingRFC3986PercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *bearerTokenCredentials = [NSString stringWithFormat:@"%@:%@", encodedConsumerToken, encodedConsumerSecret];
    NSData *data = [bearerTokenCredentials dataUsingEncoding:NSUTF8StringEncoding];
    return [self base64forData:data];
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)send_tweet_Clicked {
    
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:@"Tweeting from Dance With Madhuri iOS App!!!"];
        tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
            if (result == TWTweetComposeViewControllerResultCancelled){
                [self dismissModalViewControllerAnimated:YES];
            } else if (result == TWTweetComposeViewControllerResultDone) {
                //DebugLog(@" Tweets: Tweet Sent");
            }
            
        };
        [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"Tweets: Error: %@", [error localizedDescription]);
    [self parseFromFile];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    if(connection==connection1)
    {
        if(responseAsyncData != nil)
        {
            NSError *error = nil;
            id json = [NSJSONSerialization JSONObjectWithData:responseAsyncData options:NSJSONReadingMutableLeaves error:&error];
            DebugLog(@"Tweets: connect 1 : json %@",json);
            NSString *tokenType = [json valueForKey:@"token_type"];
            if([tokenType isEqualToString:@"bearer"] == YES) {
                self.bearerToken = [json valueForKey:@"access_token"];
                //DebugLog(@" Tweets: bearer access tokes = %@",self.bearerToken);
                responseAsyncData = nil;
                [self invalidateBearerToken];
            }
        }
    } else if(connection==connection2)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"Tweets: connect2 : json %@",json);
            if(json != nil) {
                [tweetsArray removeAllObjects];
                
                //     NSDictionary *searchMetadata = [json valueForKey:@"search_metadata"];
                NSArray *statuses = [json valueForKey:@"statuses"];
                
                for (NSDictionary* tweetattributeDict in statuses)
                {
                    tweetObj = [[DMDTweetObj alloc] init];
                    tweetObj.tweetText = [tweetattributeDict objectForKey:@"text"];
                    
                    NSDictionary *userDict = [tweetattributeDict valueForKey:@"user"];
                    
                    tweetObj.fromuserName = [userDict objectForKey:@"screen_name"];
                    tweetObj.imageUrl = [userDict objectForKey:@"profile_image_url"];
                    DebugLog(@"Tweets: \n name: %@  , image: %@  , text: %@\n", tweetObj.fromuserName, tweetObj.imageUrl, tweetObj.tweetText);
                    [tweetsArray addObject:tweetObj];
                    tweetObj = nil;
                }
                [NSKeyedArchiver archiveRootObject:tweetsArray toFile:dataFilePath];
                [CommonCallBack hideProgressHud];
                [tweetsTableView reloadData];
            }
        }
        responseAsyncData = nil;
    }
}

#pragma mark Table view methods
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = 180.0f;
    //    if ([self.expandedCellIndexPath isEqual:indexPath]) {
    //        cellHeight = self.expandedCellHeight;
    //    }
    return cellHeight;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tweetsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TweetCell";
    UILabel *lblName;
    UILabel *lblMessage;
    UIView *contentView;
    
    CGFloat cellHeight = 180.0;
    //    CGFloat cellHeight = cell.frame.size.height;
    //    if ([self.expandedCellIndexPath isEqual:indexPath]) {
    //        cellHeight = self.expandedCellHeight;
    //    }
    //    DebugLog(@"Tweets: index %d cellheight %f",indexPath.row,cell.frame.size.height);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.textLabel.textColor = [UIColor whiteColor];
        //        cell.textLabel.font = [UIFont systemFontOfSize:28.0];
        //        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        //        cell.detailTextLabel.font = [UIFont systemFontOfSize:24.0];
        //        cell.detailTextLabel.numberOfLines = 5;
        
        contentView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, cellHeight-20)];
        contentView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        contentView.tag = 11;
        contentView.layer.cornerRadius = 10;
        contentView.layer.masksToBounds = YES;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 80, 80)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.cornerRadius = 25;
        thumbImg.layer.borderColor = [UIColor darkGrayColor].CGColor;
        thumbImg.layer.borderWidth = 4;
        [contentView addSubview:thumbImg];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame) + 5,15,205.0,20.0)];
        lblName.tag = 2;
        //        lblName.shadowColor   = [UIColor blackColor];
        //        lblName.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(18);//[UIFont fontWithName:@"Cochin-Bold" size:24];
        lblMessage.numberOfLines = 2;
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblName];
        
        //Initialize Label with tag 2.(Title Label)
        lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+ 5,CGRectGetMaxY(lblName.frame),205.0,contentView.frame.size.height-CGRectGetMaxY(lblName.frame))];
        lblMessage.tag = 3;
        lblMessage.numberOfLines = 0;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMessage.adjustsFontSizeToFitWidth = TRUE;
        lblMessage.minimumFontSize = 15.0;
        lblMessage.font = KABEL_FONT(15);//[UIFont fontWithName:@"Cochin-Bold" size:20];
        lblMessage.textAlignment = UITextAlignmentLeft;
        lblMessage.textColor = [UIColor lightGrayColor];
        lblMessage.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblMessage];
        
        [cell.contentView addSubview:contentView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0];
        cell.selectedBackgroundView = bgColorView;
    }
    
    cell.userInteractionEnabled = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    DMDTweetObj *temptweetObj  = [tweetsArray objectAtIndex:indexPath.row];
    
    contentView = (UIView *)[cell viewWithTag:11];
    
    lblName = (UILabel *)[contentView viewWithTag:2];
    lblMessage = (UILabel *)[contentView viewWithTag:3];
    lblName.text = temptweetObj.fromuserName;
    lblMessage.text = temptweetObj.tweetText;
    
    UIImageView *thumbImgview = (UIImageView *)[contentView viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:temptweetObj.imageUrl]
                 placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
@end
