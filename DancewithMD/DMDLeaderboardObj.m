//
//  DMDLeaderboardObj.m
//  DancewithMD
//
//  Created by Rishi on 20/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLeaderboardObj.h"

@implementation DMDLeaderboardObj

@synthesize userId;
@synthesize userRank;
@synthesize userName;
@synthesize userPoints;

@end
