//
//  DMDExpertDetailsViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 26/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDExpertDetailsViewController.h"
#import "CommonCallBack.h"
#import "DMDAppDelegate.h"

@interface DMDExpertDetailsViewController ()

@end

@implementation DMDExpertDetailsViewController
@synthesize urlString;
@synthesize webView;
@synthesize videoType;
@synthesize movieController;
@synthesize selectedStarName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     selectedStarName, @"StarName",
     nil];
    [Flurry logEvent:@"View_Star_Speaks" withParameters:storeParams];
    [[LocalyticsSession shared] tagEvent:@"View_Star_Speaks" attributes:storeParams];
    [[LocalyticsSession shared] tagScreen:@"View_Star_Speaks"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([DMD_APP_DELEGATE networkavailable])
    {
        if (videoType == VIDEO_WEBVIEW) {
            [self playVideoInWebView:urlString];
        }else{
            [self playVideofromUrl:urlString];
        }
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    if (webView != nil) {
        [webView stopLoading];
    }
    if(self.movieController != nil)
    {
        [movieController.moviePlayer stop];
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}

#pragma internal methods
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [webView stopLoading];
                     }];
    
}
-(void)playVideoInWebView:(NSString *)link
{
    DebugLog(@"link = %@",link);
    NSString *embedHTML =[NSString stringWithFormat:@"\
                          <html><head>\
                          <style type=\"text/css\">\
                          body {\
                          background-color: transparent;\
                          color: blue;\
                          }\
                          </style>\
                          </head><body style=\"margin:0\">\
                          <center><iframe width=\"300\" height=\"300\" src=\"%@\" frameborder=\"0\" allowfullscreen></iframe></center>\
                          </body></html>",link];
    //DebugLog(@"ExpertView: %@",embedHTML);
    webView.backgroundColor = [UIColor clearColor];
    [webView loadHTMLString:embedHTML baseURL:nil];
    [webView setOpaque:NO];
    [webView setHidden:NO];
    [webView.scrollView setBounces:FALSE];
    [webView.scrollView setScrollEnabled:FALSE];
}

-(void)playVideofromUrl:(NSString *)videoUrl
{
    DebugLog(@"link = %@",videoUrl);
    if(self.movieController != nil) {
        [movieController.moviePlayer stop];
        // Remove the movie player view controller from the "playback did finish" notification observers
        [[NSNotificationCenter defaultCenter] removeObserver:movieController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
    
    // Initialize the movie player view controller with a video URL string
    UIGraphicsBeginImageContext(CGSizeMake(1,1));
    movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    UIGraphicsEndImageContext();
    
    // Register this class as an observer instead
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieController.moviePlayer];
    
    // Set the modal transition style of your choice
    movieController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    // Present the movie player view controller
    //    [self presentModalViewController:movieController animated:YES];
    [self presentMoviePlayerViewControllerAnimated:movieController];
    
    // Start playback
    [movieController.moviePlayer prepareToPlay];
    [movieController.moviePlayer play];
}

#pragma MPMoviePlayerController callbacks
- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    DebugLog(@"moviePlayBackDidFinish");
    // Obtain the reason why the movie playback finished
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([finishReason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
        {
            DebugLog(@"MPMovieFinishReasonPlaybackEnded");
        }
            break;
        case MPMovieFinishReasonPlaybackError:
        {
            DebugLog(@"MPMovieFinishReasonPlaybackError");
            
        }
            break;
        case MPMovieFinishReasonUserExited:
        {
            DebugLog(@"MPMovieFinishReasonUserExited");
            
        }
            break;
        default:
            break;
    }
    // Remove this class from the observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:movieController.moviePlayer];
    
    [self dismissMoviePlayerViewControllerAnimated];
    movieController = nil;
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma webView delegate Methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //DebugLog(@" FaceBook: request : %@",webView.request.URL);
    [self showProgressHudWithCancel:@"Star Speak Clip" subtitle:@"Loading"];
}

- (void)webViewDidFinishLoad:(UIWebView *)lwebView
{
    [CommonCallBack hideProgressHud];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [CommonCallBack hideProgressHudWithError];
}
@end
