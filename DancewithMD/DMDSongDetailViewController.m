//
//  DMDSongDetailViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSongDetailViewController.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DMDSongDetailObj.h"
#import "DMDBadgeObj.h"
#import "DMDBadgeShareController.h"
#import "CommonCallBack.h"

// Get selected song parts
#define SONG_DETAIL_LIST_LINK(SONGID) [NSString stringWithFormat:@"%@/api/retrieve/songs/%@",LIVE_DWM_SERVER,SONGID]

// Get already seen song parts count
#define SONG_SEEN_LINK(USERID,SONGID) [NSString stringWithFormat:@"%@/api/retrieve/points?where={\"User_ID\":\"%@\",\"Song_ID\":%@}",LIVE_DWM_SERVER,USERID,SONGID]

//Get clip detail using clipid
#define SONG_CLIP_ID_LINK(CLIPID) [NSString stringWithFormat:@"%@/api/retrieve/clips/%@",LIVE_DWM_SERVER,CLIPID]

//Unlock seleced clip
#define SONG_PLAYED_LINK(USERID,SONGID,CLIPID) [NSString stringWithFormat:@"%@/point.php?uid=%@&sid=%@&cid=%d", LIVE_DWM_SERVER,USERID,SONGID,CLIPID]

//Get list of all Badges
#define BADGE_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/badge",LIVE_DWM_SERVER]

//Update badge for particular UserId
#define BADGE_SET_LINK [NSString stringWithFormat:@"%@/api/retrieve/setbadge",LIVE_DWM_SERVER]

//Get badge image
#define BADGE_IMAGE_LINK(SHAREPIC) [NSString stringWithFormat:@"%@/images/%@",LIVE_DWM_SERVER,SHAREPIC]

@interface DMDSongDetailViewController ()

@end

@implementation DMDSongDetailViewController
@synthesize songID;
@synthesize songDetailArray;
@synthesize seenCount;
@synthesize carousel;
@synthesize selectedsongindex;
@synthesize badgeArray;
@synthesize selectedsongclipID;
@synthesize closeVideoBtn;
@synthesize songName;
@synthesize movieController;
@synthesize styleName;

@synthesize badgeView,badgeImageView,badgelbl,fbShareBtn,closeBadgeViewBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma view lifeCycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        DebugLog(@"already in Portrait");
//    }
//    else
//    {
//        UIViewController *c = [[UIViewController alloc]init];
//        [self presentModalViewController:c animated:NO];
//        [self dismissModalViewControllerAnimated:NO];
//    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    DebugLog(@"%@",[NSString stringWithFormat:@"%@ - %@",styleName,songName]);
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
    styleName ,@"DanceStyle",
    [NSString stringWithFormat:@"%@ - %@",styleName,songName],@"SongName",
     nil];
    [Flurry logEvent:@"View_Lessons" withParameters:storeParams];
    [[LocalyticsSession shared] tagEvent:@"View_Lessons" attributes:storeParams];
    [[LocalyticsSession shared] tagScreen:@"Lesson_Clips"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    
    fullscreen = 0;
    carousel.type = iCarouselTypeCoverFlow;
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self songDetailListAsynchronousCall];
        [self badgeListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    if (songDetailConnection != nil) {
        [songDetailConnection cancel];
        songDetailConnection = nil;
    }
    if (songDetailLockConnection != nil) {
        [songDetailLockConnection cancel];
        songDetailLockConnection = nil;
    }
    if (songStreamRequestConnection != nil) {
        [songStreamRequestConnection cancel];
        songStreamRequestConnection = nil;
    }
    if (unlockSongConnection != nil) {
        [unlockSongConnection cancel];
        unlockSongConnection = nil;
    }
    if (badgeListConnection != nil) {
        [badgeListConnection cancel];
        badgeListConnection = nil;
    }
    if (setBadgeConnection != nil) {
        [setBadgeConnection cancel];
        setBadgeConnection = nil;
    }
    if(self.movieController != nil)
    {
        [movieController.moviePlayer stop];
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
}
- (void)viewDidUnload {
    [self setCarousel:nil];
    [self setCloseVideoBtn:nil];
    [self setBadgeView:nil];
    [self setBadgeImageView:nil];
    [self setBadgelbl:nil];
    [self setFbShareBtn:nil];
    [self setCloseBadgeViewBtn:nil];
    [super viewDidUnload];
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}

#pragma Internal Callbacks
-(void)setUI{
    badgelbl.textColor = [UIColor whiteColor];
    badgelbl.backgroundColor = [UIColor clearColor];
    badgelbl.font = KABEL_FONT(15);
    
    closeBadgeViewBtn.layer.cornerRadius = 20;
    closeBadgeViewBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    closeBadgeViewBtn.layer.borderWidth = 3.0;
    
    
    badgeView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.9];
    [badgeView setHidden:TRUE];
    [closeBadgeViewBtn setHidden:TRUE];
    
    [closeVideoBtn setHidden:TRUE];
}
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (songDetailConnection != nil) {
                             [songDetailConnection cancel];
                             songDetailConnection = nil;
                         }
                         if (songDetailLockConnection != nil) {
                             [songDetailLockConnection cancel];
                             songDetailLockConnection = nil;
                         }
                         if (songStreamRequestConnection != nil) {
                             [songStreamRequestConnection cancel];
                             songStreamRequestConnection = nil;
                         }
                         if (unlockSongConnection != nil) {
                             [unlockSongConnection cancel];
                             unlockSongConnection = nil;
                         }
                         if (badgeListConnection != nil) {
                             [badgeListConnection cancel];
                             badgeListConnection = nil;
                         }
                         if (setBadgeConnection != nil) {
                             [setBadgeConnection cancel];
                             setBadgeConnection = nil;
                         }
                     }];
}
-(void)songDetailListAsynchronousCall
{
    [self showProgressHudWithCancel:[NSString stringWithFormat:@"%@\n Clips",songName] subtitle:@"Loading"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_DETAIL_LIST_LINK(self.songID)] cachePolicy:NO timeoutInterval:5.0];
    DebugLog(@"LearnDanceDetailView: %@",urlRequest.URL);
    //Get list of all songs
    songDetailConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
-(void)songLockAsynchronousCall
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    NSMutableString *requestString = [NSMutableString stringWithString:[DMD_APP_DELEGATE URLEncode:SONG_SEEN_LINK(userID,self.songID)]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    songDetailLockConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)songUrlAsynchronousCall:(NSString*) clipId
{
    NSString *requestString = [NSMutableString stringWithString:SONG_CLIP_ID_LINK(clipId)];
    //DebugLog(@"LearnDanceDetailView: CLIP LINK=%@",requestString);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    songStreamRequestConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)unlockNextSongToPlayAsynchronousCall:(int)clipID
{
    DebugLog(@"LearnDanceDetailView: unlockNextSongToPlayAsynchronousCall %d",clipID);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:@"userId"];
    NSMutableString *requestString = [NSMutableString stringWithString:SONG_PLAYED_LINK(userID,self.songID,clipID)];
    DebugLog(@"LearnDanceDetailView: SONG_PLAYED_LINK %@",requestString);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:5.0];
    unlockSongConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)badgeListAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:BADGE_LIST_LINK] cachePolicy:NO timeoutInterval:5.0];
    DebugLog(@"LearnDanceDetailView: badgeList %@",urlRequest.URL);
    //Get list of all Badges
    badgeListConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)setBadgeAsynchronousCall:(NSString *)badgeName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:BADGE_SET_LINK] cachePolicy:NO timeoutInterval:30.0];
    
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userID forKey:@"user_id"];
    [postReq setObject:self.songID forKey:@"song_id"];
    [postReq setObject:badgeName forKey:@"badge"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        DebugLog(@"LearnDanceDetailView: Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"LearnDanceDetailView: ---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    
    //Update badge for particular UserId
    setBadgeConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)showbadgeView:(NSString *)songid clipPartId:(NSString *)clipid
{
    for (DMDBadgeObj * tempbadgeObj in badgeArray) {
        if([clipid isEqualToString:tempbadgeObj.clip_partId])
        {
            DebugLog(@"LearnDanceDetailView: %@ %@ %@", tempbadgeObj.clip_partId, BADGE_IMAGE_LINK(tempbadgeObj.sharePic),tempbadgeObj.badgeName);
            [badgeView setHidden:FALSE];
            [closeBadgeViewBtn setHidden:FALSE];
            
            [badgeImageView setImageWithURL:[NSURL URLWithString:BADGE_IMAGE_LINK(tempbadgeObj.sharePic)]
                           placeholderImage:[UIImage imageNamed:@"pop-up.png"]
                                    success:^(UIImage *image) {
                                        //DebugLog(@"success");
                                    }
                                    failure:^(NSError *error) {
                                        //DebugLog(@"write error %@", error);
                                    }];
            
            if ((id)tempbadgeObj.shareText != [NSNull null]) {
                badgelbl.text = tempbadgeObj.shareText;
            }
            if ((id)tempbadgeObj.badgeName != [NSNull null]) {
                [self setBadgeAsynchronousCall:tempbadgeObj.badgeName];
            }
            break;
        } else {
            [badgeView setHidden:TRUE];
            [closeBadgeViewBtn setHidden:TRUE];
        }
    }
}

-(void)playVideofromUrl:(NSString *)videoUrl
{
    DebugLog(@"LearnDanceDetailView: playVideofromUrl %@",videoUrl);
    if(self.movieController != nil) {
        [movieController.moviePlayer stop];
        // Remove the movie player view controller from the "playback did finish" notification observers
        [[NSNotificationCenter defaultCenter] removeObserver:movieController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:movieController.moviePlayer];
        self.movieController = nil;
    }
    
    // Initialize the movie player view controller with a video URL string
    UIGraphicsBeginImageContext(CGSizeMake(1,1));
    movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
    UIGraphicsEndImageContext();
    
    // Register this class as an observer instead
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieController.moviePlayer];
    
    // Set the modal transition style of your choice
    movieController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    // Present the movie player view controller
//    [self presentModalViewController:movieController animated:YES];
    [self presentMoviePlayerViewControllerAnimated:movieController];

    // Start playback
    [movieController.moviePlayer prepareToPlay];
    [movieController.moviePlayer play];
}

#pragma MPMoviePlayerController callbacks
- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    DebugLog(@"LearnDanceDetailView: moviePlayBackDidFinish");
    // Obtain the reason why the movie playback finished
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([finishReason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonPlaybackEnded");
            DebugLog(@"LearnDanceDetailView: sondID=%@ , clipID=%@",self.songID, self.selectedsongclipID);
            [self showbadgeView:self.songID clipPartId:self.selectedsongclipID];
            [self unlockNextSongToPlayAsynchronousCall:selectedsongindex+1]; //Unlock next video
        }
            break;
        case MPMovieFinishReasonPlaybackError:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonPlaybackError");

        }
            break;
        case MPMovieFinishReasonUserExited:
        {
            DebugLog(@"LearnDanceDetailView: MPMovieFinishReasonUserExited");

        }
            break;
        default:
            break;
    }
    // Remove this class from the observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:movieController.moviePlayer];
    
    //       [self dismissModalViewControllerAnimated:NO];
    [self dismissMoviePlayerViewControllerAnimated];
    movieController = nil;
    [self hideVideoView];
    [self.closeVideoBtn setHidden:TRUE];
    
}

-(void)hideVideoView {
//    backgroundImageView.image = [UIImage imageNamed:@"BG_default.jpg"];
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.8f;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade;
//    [backgroundImageView.layer addAnimation:transition forKey:nil];
//    
//    CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//    unshrink.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    unshrink.fromValue = [NSNumber numberWithDouble:1.0];
//    unshrink.toValue = [NSNumber numberWithDouble:1.5];
//    unshrink.duration = 0.8;
//    unshrink.fillMode = kCAFillModeForwards;
//    unshrink.removedOnCompletion = NO;
//    [videoView.layer addAnimation:unshrink forKey:@"shrink"];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.6];
//    [videoView setAlpha:0.0];
//    [UIView commitAnimations];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.5];
//    [lightspotImageView setAlpha:1.0];
//    [UIView commitAnimations];
    
    small = 2;
    [carousel reloadData];
    small = 0;
    //[videoView setHidden:TRUE];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"LearnDanceDetailView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if(connection==songDetailConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"LearnDanceDetailView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (songDetailArray == nil) {
                        songDetailArray = [[NSMutableArray alloc] init];
                    }else{
                        [songDetailArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        songDetailObj = [[DMDSongDetailObj alloc] init];
                        songDetailObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                        songDetailObj.songName = [songattributeDict objectForKey:@"Song_Name"];
                        songDetailObj.songClip_Part_ID = [songattributeDict objectForKey:@"Clip_Part_ID"];
                        songDetailObj.songSequence = [songattributeDict objectForKey:@"Sequence"];
                        songDetailObj.songShare_ID = [songattributeDict objectForKey:@"Share_ID"];
                        songDetailObj.songclip_Title = [songattributeDict objectForKey:@"Clip_Title"];
                        DebugLog(@"LearnDanceDetailView: \n songid: %@ , name: %@  , clipid: %@  , sequence: %@  , shareid: %@ \n", songDetailObj.songId, songDetailObj.songName, songDetailObj.songClip_Part_ID, songDetailObj.songSequence, songDetailObj.songShare_ID);
                        [songDetailArray addObject:songDetailObj];
                        songDetailObj = nil;
                    }
                }
            }
        }
        responseAsyncData = nil;
        songDetailConnection = nil;
        DebugLog(@"LearnDanceDetailView: detail array count: %d", [songDetailArray count]);
        [carousel reloadData];
        [self songLockAsynchronousCall];
    }else if (connection==songDetailLockConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"LearnDanceDetailView:  result=%@", json ) ;
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* lockattributeDict in dataArray)
                    {
                        if([lockattributeDict valueForKey:@"User_ID"] != nil) {
                            //                            NSString *uidString = [lockattributeDict objectForKey:@"User_ID"];
                            //                            NSString *sidString = [lockattributeDict objectForKey:@"Song_ID"];
                            //                            NSString *seenString = [lockattributeDict objectForKey:@"Seen_Clip"];
                            self.seenCount = [lockattributeDict objectForKey:@"Seen_Clips"] ;
                            ////DebugLog(@"LearnDanceDetailView:  uid: %@ , sid: %@  , seen: %@  , seenclips: %@ \n", uidString, sidString, seenString, self.seenCount);
                        } else {
                            NSString *dataString = [lockattributeDict objectForKey:@"data"];
                            DebugLog(@"LearnDanceDetailView: %@",dataString);
                        }
                    }
                }
            }
        }
        songDetailLockConnection = nil;
        responseAsyncData = nil;
        [carousel reloadData];
    }else if (connection==songStreamRequestConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"LearnDanceDetailView: json: %@", json);
            NSString *urlString;
            if (json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* songattributeDict in dataArray)
                    {
                        urlString = [songattributeDict objectForKey:@"Video_URL"];
                        DebugLog(@"LearnDanceDetailView: %@",urlString);
                    }
                    NSString *videourl = [DMD_APP_DELEGATE URLEncode:[NSString stringWithFormat:@"%@",urlString]];
                    [self playVideofromUrl:videourl];
                }
            }
        }
        songStreamRequestConnection = nil;
        responseAsyncData = nil;
        
    }else if (connection==unlockSongConnection)
    {
        if(responseAsyncData != nil)
        {
            NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                        length:[responseAsyncData length] encoding:NSASCIIStringEncoding];
            DebugLog(@"LearnDanceDetailView: unlockSongconnection  result = %@", result ) ;
            if(result!=nil && [result isEqualToString:@"1"])
            {
                self.seenCount = [NSString stringWithFormat:@"%d",[self.seenCount intValue] + 1];
//                [carousel reloadData];
                [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
               
            } else if (result!=nil && [result isEqualToString:@"2"]) {
                [carousel scrollToItemAtIndex:selectedsongindex+1 duration:1.5];
            }
        }
        unlockSongConnection = nil;
        responseAsyncData = nil;
        [carousel reloadData];
    }else if (connection == badgeListConnection)
    {
        if(responseAsyncData != nil)
        {
            //NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            ////DebugLog(@"LearnDanceDetailView: \n result:%@\n\n", result);
            NSError* error;
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"LearnDanceDetailView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    if (badgeArray == nil) {
                        badgeArray = [[NSMutableArray alloc] init];
                    }else{
                        [badgeArray removeAllObjects];
                    }
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* badgeattributeDict in dataArray) {
                        badgeObj = [[DMDBadgeObj alloc] init];
                        badgeObj.songId = [badgeattributeDict objectForKey:@"Song_ID"];
                        badgeObj.clip_partId = [badgeattributeDict objectForKey:@"Clip_Part_ID"];
                        badgeObj.shareId = [badgeattributeDict objectForKey:@"Share_ID"];
                        badgeObj.sharePic = [badgeattributeDict objectForKey:@"Share_Pic"];
                        badgeObj.badgeName = [badgeattributeDict objectForKey:@"Badge_Name"];
                        badgeObj.shareText = [badgeattributeDict objectForKey:@"Share_Text"];
                        ////DebugLog(@"LearnDanceDetailView: \n songid: %@ , clipartid: %@  , shareid: %@  , sharepic: %@  , badgename: %@  , sharetext: %@\n", badgeObj.songId, badgeObj.clip_partId, badgeObj.shareId, badgeObj.sharePic, badgeObj.badgeName, badgeObj.shareText);
                        [badgeArray addObject:badgeObj];
                        badgeObj = nil;
                    }
                }
            }
        }
        badgeListConnection=nil;
        responseAsyncData = nil;
    } else if (connection == setBadgeConnection)
    {
        if(responseAsyncData != nil)
        {
            NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
            DebugLog(@"LearnDanceDetailView: \nsetBadgeConnection result:%@\n\n", result);
        }
        setBadgeConnection=nil;
        responseAsyncData = nil;
    }
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [songDetailArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f,200.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, 200, 20)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = GRAY_TEXT_COLOR;
        label.font = KABEL_FONT(18);
        label.adjustsFontSizeToFitWidth = YES;
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    ////DebugLog(@"LearnDanceDetailView: %f %f",((UIImageView *)view).frame.size.width,((UIImageView *)view).frame.size.height);
    
    ////DebugLog(@"LearnDanceDetailView: irow=%d icount=%d",index,[self.seenCount intValue]);
    if([self.seenCount intValue] >= index)
    {
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_unlock.png"];
        //DebugLog(@"LearnDanceDetailView: unlock");
    } else {
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        //DebugLog(@"LearnDanceDetailView: locked");
    }
    
    if(small==1)
    {
        CABasicAnimation *shrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        shrink.fromValue = [NSNumber numberWithDouble:1.0];
        shrink.toValue = [NSNumber numberWithDouble:0.5];
        shrink.duration = 0.6;
        shrink.fillMode=kCAFillModeForwards;
        shrink.removedOnCompletion=NO;
        //shrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:shrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = FALSE;
    } else if(small==2) {
        CABasicAnimation *unshrink = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        unshrink.fromValue = [NSNumber numberWithDouble:0.5];
        unshrink.toValue = [NSNumber numberWithDouble:1.0];
        unshrink.duration = 0.6;
        unshrink.fillMode=kCAFillModeForwards;
        unshrink.removedOnCompletion=NO;
        //unshrink.delegate = self;
        [((UIImageView *)view).layer addAnimation:unshrink forKey:@"shrink"];
        self.carousel.userInteractionEnabled = TRUE;
    }else if(small==0){
        self.carousel.userInteractionEnabled = TRUE;
    }
    
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:index];
//    NSArray *arr = [tempdetailsongObj.songClip_Part_ID componentsSeparatedByString:@"_"];
//    label.text = [NSString stringWithFormat:@"Lesson %@   Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]];
    label.text = tempdetailsongObj.songclip_Title;
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if([self.seenCount intValue] >= index)
    {
        DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:index];
        selectedsongindex = index;
        selectedsongclipID = tempdetailsongObj.songClip_Part_ID;

        //    [self showVideoView];
        if (small==0 || small==2) {
            small = 1;
        } else if(small==1){
            small = 2;
        }
        [self.carousel reloadData];
        //DebugLog(@"LearnDanceDetailView: ===ID===%@",tempdetailsongObj.songClip_Part_ID);
        [self songUrlAsynchronousCall:tempdetailsongObj.songClip_Part_ID];
        
        NSDictionary *storeParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
        styleName ,@"DanceStyle",
        [NSString stringWithFormat:@"%@ - %@",styleName,songName],@"SongName",
        [NSString stringWithFormat:@"%@ - %@ - %@", styleName,songName,tempdetailsongObj.songclip_Title],@"ClipTitle",
         nil];
        [Flurry logEvent:@"View_Clips" withParameters:storeParams];
        [[LocalyticsSession shared] tagEvent:@"View_Clips" attributes:storeParams];
    }
//        //DebugLog(@"LearnDanceDetailView: index222==%d",index);
//        [self fbsharecloseBtnPressed:nil];
//        DMDSongDetailObj *tempdetailsongObj  = [songDetailArray objectAtIndex:index];
//        selectedsongindex = index;
//        selectedsongclipID = tempdetailsongObj.songClip_Part_ID;
//        selectedsongName =  tempdetailsongObj.songName;
//        //[self showbadgeView:self.songID clipPartId:selectedsongclipID];
//        //DebugLog(@"LearnDanceDetailView: songID=%@ , clipID=%@ songName=%@",self.songID, self.selectedsongclipID,self.selectedsongName);
//        titlelbl.text = [NSString stringWithFormat:@"%@",tempdetailsongObj.songName];
//        NSArray *arr = [tempdetailsongObj.songClip_Part_ID componentsSeparatedByString:@"_"];
//        //DebugLog(@"LearnDanceDetailView: %@", arr);
//        namelbl.text = [NSString stringWithFormat:@"Lesson %@     Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]];
//        NSString * fileName = [NSString stringWithFormat:@"%@#%@.mp4",tempdetailsongObj.songName,[NSString stringWithFormat:@"Lesson %@ Part %@",[arr objectAtIndex:[arr count]-2],[arr objectAtIndex:[arr count]-1]]];
//        fileName = [fileName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
//        //DebugLog(@"LearnDanceDetailView: filename=%@",fileName);
//        
//        NSArray *dirPaths;
//        NSString *fullPath;
//        NSString *docsDir;
//        NSFileManager *filemgr =[NSFileManager defaultManager];
//        // Get the documents directory
//        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        docsDir = [dirPaths objectAtIndex:0];
//        //DebugLog(@"LearnDanceDetailView: song id = %@",self.songID);
//        fullPath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Videos/%@",self.selectedsongName]];
//        NSString *filepath = [fullPath stringByAppendingPathComponent:fileName];
//        //DebugLog(@"LearnDanceDetailView: filepath = %@" , filepath);
//        if ([filemgr fileExistsAtPath:filepath])
//        {
//            //DebugLog(@"LearnDanceDetailView: file exists");
//            progressBar.hidden = FALSE;
//            [progressBar setProgress:1.0];
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateNormal];
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_on.png"] forState:UIControlStateHighlighted];
//            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateNormal];
//            [downloadBtn setTitle:@"Downloaded" forState:UIControlStateHighlighted];
//            downloadBtn.userInteractionEnabled = FALSE;
//            [self showVideoView];
//            if (small==0 || small==2) {
//                small = 1;
//            } else if(small==1){
//                small = 2;
//            }
//            self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            progressBar.hidden = FALSE;
//            [videoView setHidden:FALSE];
//            screenshotView.hidden = FALSE;
//            
//            UIGraphicsBeginImageContext(videoboxImageView.frame.size);
//            [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
//            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//            screenshotView.image = viewImage;
//            UIGraphicsEndImageContext();
//            //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
//            
//            
//            // determine the size of the reflection to create
//            NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
//            
//            // create the reflection image and assign it to the UIImageView
//            self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
//            //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
//            self.reflectionView.alpha = kDefaultReflectionOpacity;
//            
//            screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            screenshotView.hidden = TRUE;
//            
//            [self.carousel reloadData];
//            [self performSelector:@selector(loadLocalDocument:) withObject:filepath afterDelay:0.5];
//            
//        } else {
//            //DebugLog(@"LearnDanceDetailView: file not exists");
//            downloadBtn.userInteractionEnabled = TRUE;
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateNormal];
//            [downloadBtn setBackgroundImage:[UIImage imageNamed:@"dwn_button_default.png"] forState:UIControlStateHighlighted];
//            [downloadBtn setTitle:@"Download" forState:UIControlStateNormal];
//            [downloadBtn setTitle:@"Download" forState:UIControlStateHighlighted];
//            [progressBar setProgress:0];
//            [self showVideoView];
//            if (small==0 || small==2) {
//                small = 1;
//            } else if(small==1){
//                small = 2;
//            }
//            self.reflectionView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            progressBar.hidden = FALSE;
//            [videoView setHidden:FALSE];
//            screenshotView.hidden = FALSE;
//            
//            UIGraphicsBeginImageContext(videoboxImageView.frame.size);
//            [videoView.layer renderInContext:UIGraphicsGetCurrentContext()];
//            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//            screenshotView.image = viewImage;
//            UIGraphicsEndImageContext();
//            //UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
//            
//            
//            // determine the size of the reflection to create
//            NSUInteger reflectionHeight = self.videoboxImageView.bounds.size.height * kDefaultReflectionFraction;
//            
//            // create the reflection image and assign it to the UIImageView
//            self.reflectionView.image = [DMDDelegate reflectedImage:screenshotView withHeight:reflectionHeight];
//            //self.reflectionView.image = [self reflectedImage:screenshotView withHeight:reflectionHeight];
//            self.reflectionView.alpha = kDefaultReflectionOpacity;
//            
//            screenshotView.image = [UIImage imageNamed:@"video_box_transparent.png"];
//            screenshotView.hidden = TRUE;
//            
//            [self.carousel reloadData];
//            //DebugLog(@"LearnDanceDetailView: ===ID===%@",tempdetailsongObj.songClip_Part_ID);
//            [self songUrlAsynchronousCall:tempdetailsongObj.songClip_Part_ID];
//        }
//        //DebugLog(@"LearnDanceDetailView: unlock");
//    } else {
//        //DebugLog(@"LearnDanceDetailView: locked");
//    }
    
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionFadeMin:
        {
            return -0.2;
        }
        case iCarouselOptionFadeMax:
        {
            return 0.2;
        }
        case iCarouselOptionFadeRange:
        {
            return 3.0;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.599131;
        }
        case iCarouselOptionTilt:
        {
            return 0.495158;
        }
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        default:
        {
            return value;
        }
    }
}

#pragma UIButton delegate Method
- (IBAction)closeVideoBtnClicked:(id)sender {
    [self hideVideoView];
    [self.closeVideoBtn setHidden:TRUE];
}

- (IBAction)fbShareBtnClicked:(id)sender {
    NSString *text;
    NSString *pic;
    for (DMDBadgeObj * tempbadgeObj in badgeArray) {
        if([self.selectedsongclipID isEqualToString:tempbadgeObj.clip_partId])
        {
            //DebugLog(@"LearnDanceDetailView: text======%@===========",tempbadgeObj.shareText);
            //DebugLog(@"LearnDanceDetailView: link======%@===========",BADGE_IMAGE_LINK(tempbadgeObj.sharePic));
            text = tempbadgeObj.shareText;
            pic = BADGE_IMAGE_LINK(tempbadgeObj.sharePic);
        }
    }
    DMDBadgeShareController *facebookViewComposer = [[DMDBadgeShareController alloc] initWithNibName:@"DMDBadgeShareController" bundle:nil];
    facebookViewComposer.title = @"FACEBOOK";
    facebookViewComposer.FBtitle = @"Dance with Madhuri";
    facebookViewComposer.FBtCaption = text;
    facebookViewComposer.FBtPic = pic;
    facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
}

- (IBAction)closeBadgeViewBtnClicked:(id)sender {
    [badgeView setHidden:TRUE];
    [closeBadgeViewBtn setHidden:TRUE];
}
@end
