//
//  DMDExpertDetailsViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 26/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#define VIDEO_WEBVIEW 1
#define VIDEO_URL 2

@interface DMDExpertDetailsViewController : UIViewController
@property (nonatomic,copy) NSString *urlString;
@property (nonatomic,copy) NSString *selectedStarName;
@property (nonatomic,readwrite) int videoType;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) MPMoviePlayerViewController *movieController;

@end
