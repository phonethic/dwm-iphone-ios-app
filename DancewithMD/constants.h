
#ifndef _ALERT_MESSAGES_H_
#define _ALERT_MESSAGES_H_
#import "Flurry.h"
#import "Appirater.h"
#import <Parse/Parse.h>
#import "Reachability.h"
#import "LocalyticsSession.h"

//#define FLURRY_APPID @"83KSFSSYXD98WPHR7DHW"
//#define FLURRY_APPID @"HRWPHQW8NZVW5YF4T5GN" //TEST APP Flurry ID

#define APPIRATER_APPID @""

#define PARSE_APPID @"tWsjACyCVqMFJiJmL1PNfHloOk3sGEKMb10Tvkuj"
#define PARSE_CLIENTKEY @"T7rQdUuoPf5fh7VbMg2J7eR7WFFAxoOMkhXmlf2o"

#define CRITTERCISM_APPID @"5195f74ac463c2125800000e"

#define ALERT_TITLE @"Dance With Madhuri"

#define LOGGED_BY @"loggedBy"
#define DWMSERVER @"DWM Server"
#define FACEBOOKSERVER @"Facebook"

#define LOGIN_USERID @"userId"
#define LOGIN_TOKEN @"accessToken"
#define LOGIN_USERNAME @"userName"
#define LOGIN_EMAIL @"userEmail"
#define LOGIN_GENDER @"gender"
#define LOGIN_PASSWORD @"password"
#define LOGIN_EXPIREDDATE @"expireddate"
#define LOGIN_HELPINFOSCREEN @"helpInfoScreen"

#define DEVELOPER_USER @"developerios1@gmail.com"
#define DEVELOPER_PASSWORD @"12345678"

//#define LOCALYTICS_KEY @"8ea969243f0f730f4e68c44-531f2f80-16d2-11e3-935f-009c5fda0a25"
#define LOCALYTICS_KEY @"0a5703909c74e3beb07409e-ea245416-1aa0-11e3-1308-004a77f8b47f"


#ifdef DEBUG
    #define FLURRY_APPID @"HRWPHQW8NZVW5YF4T5GN"
    #define DebugLog( s, ... ) NSLog(@"<%@:%s:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
    #define FLURRY_APPID @"83KSFSSYXD98WPHR7DHW"
    #define DebugLog( s, ... )
#endif

#define BLUE_TEXT_COLOR [UIColor colorWithRed:18/255.0 green:101/255.0 blue:163/255.0 alpha:1.0]
#define BLUE_COLOR [UIColor colorWithRed:40/255.0 green:123/255.0 blue:163/255.0 alpha:0.5]
#define BLACK_COLOR [UIColor blackColor]
#define GRAY_TEXT_COLOR [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:1.0]
#define KABEL_FONT(SIZE) [UIFont fontWithName:@"Kabel Md BT" size:SIZE]


#define JHALAK_NOTIFICATION @"jhalakNotification"
#define JHALAK_KEY @"jhalak"
#define NETWORK_NOTIFICATION @"NetWorkNotification"

#endif
