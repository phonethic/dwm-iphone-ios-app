//
//  DMDMyBadge.h
//  DancewithMD
//
//  Created by Rishi on 22/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDMyBadge : NSObject {
    
}

@property (nonatomic, copy) NSString *mybadge_name;
@property (nonatomic, copy) NSString *mybadge_count;
@property (nonatomic, copy) NSString *mybadge_songs;
@property (nonatomic, copy) NSString *mybadge_icon;

@end
