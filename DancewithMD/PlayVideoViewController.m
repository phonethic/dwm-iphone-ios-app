//
//  PlayVideoViewController.m
//  PlayMovie
//
//  Created by Kirti Nikam on 10/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "PlayVideoViewController.h"
#import "DMDAppDelegate.h"

@interface PlayVideoViewController ()

@end

@implementation PlayVideoViewController
@synthesize urlString;
@synthesize moviePlayerController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIApplication *application = [UIApplication sharedApplication];
	self.statusBarHidden = application.statusBarHidden;
	self.statusBarStyle = application.statusBarStyle;
    if ([DMD_APP_DELEGATE networkavailable]) {
        [self playVideo];
        [self.navigationController setNavigationBarHidden:YES];
    }
    else{
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [self.navigationController setNavigationBarHidden:NO];
    }
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    DebugLog(@"PlayVideo dealloc");
    if(self.moviePlayerController != nil)
    {
        [moviePlayerController stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayerController];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayerController];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayerController];
        if ([self.moviePlayerController respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayerController.view removeFromSuperview];
        }
        self.moviePlayerController = nil;
    }
}

- (void)playVideo {
    moviePlayerController =  [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:urlString]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlaybackComplete:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayerController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:moviePlayerController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:moviePlayerController];
    
    
    moviePlayerController.scalingMode = MPMovieScalingModeAspectFit;
    moviePlayerController.shouldAutoplay = YES;
    moviePlayerController.controlStyle = MPMovieControlStyleFullscreen;
    moviePlayerController.fullscreen = YES;
    
    self.moviePlayerController.view.frame = CGRectMake(0.f, 0.f, self.view.bounds.size.width, self.view.bounds.size.height);
	self.moviePlayerController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:moviePlayerController.view];
    [moviePlayerController prepareToPlay];
    [moviePlayerController play];
}

- (void)moviePlaybackComplete:(NSNotification *)notification
{
    DebugLog(@"moviePlaybackComplete");
//    MPMoviePlayerController *moviePlayerController1 = [notification object];
////    [moviePlayerController1 setFullscreen:FALSE animated:TRUE];
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:MPMoviePlayerPlaybackDidFinishNotification
//                                                  object:moviePlayerController];
//    [moviePlayerController1 stop];
//    [moviePlayerController1.view removeFromSuperview];
    
    UIApplication *application = [UIApplication sharedApplication];
	[application setStatusBarHidden:self.statusBarHidden withAnimation:UIStatusBarAnimationFade];
	[application setStatusBarStyle:self.statusBarStyle animated:YES];
    
  
    [self.navigationController popViewControllerAnimated:YES];

}

- (void) movieEnterFullScreen:(NSNotification*)notification {
    DebugLog(@"movieEnterFullScreen");
    
}
- (void) movieExitFullScreen:(NSNotification*)notification {
    DebugLog(@"movieExitFullScreen");
}

@end
