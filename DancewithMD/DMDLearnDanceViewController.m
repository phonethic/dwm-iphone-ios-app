//
//  DMDLearnDanceViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLearnDanceViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import "DMDSongObj.h"
#import "DMDSongDetailViewController.h"

#import "PlayVideoViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "CommonCallBack.h"

#define SONG_CATEGORY_DETAIL_LIST_LINK(ID) [NSString stringWithFormat:@"%@/api/retrieve/songinfo/%@",LIVE_DWM_SERVER,ID]

#define VIDEO_IMAGE_LINK(IMG_NAME) [NSString stringWithFormat:@"%@/%@",LIVE_DWM_SERVER,IMG_NAME]

@interface DMDLearnDanceViewController ()

@end

@implementation DMDLearnDanceViewController
@synthesize songArray;
@synthesize songsTableView;
@synthesize openCellIndexPath;
@synthesize moviePlayer;
@synthesize selectedStyleID;
@synthesize selectedStyleName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     selectedStyleName, @"DanceStyle",
     nil];
    [Flurry logEvent:@"View_DanceStyle" withParameters:storeParams];
    [[LocalyticsSession shared] tagEvent:@"View_DanceStyle" attributes:storeParams];
    [[LocalyticsSession shared] tagScreen:@"DanceStyle_Lessons"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    songArray = [[NSMutableArray alloc] init];
    
    if([DMD_APP_DELEGATE networkavailable])
    {
        [songsTableView setHidden:YES];
        [self songsListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    if (songsListConnection != nil) {
        [songsListConnection cancel];
        songsListConnection = nil;
    }
    if(self.moviePlayer != nil)
    {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}
- (void)viewDidUnload {
    [self setSongsTableView:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}


#pragma Internal Callbacks
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (songsListConnection != nil) {
                             [songsListConnection cancel];
                             songsListConnection = nil;
                         }
                     }];
    
}

-(void)songsListAsynchronousCall
{
    [self showProgressHudWithCancel:[NSString stringWithFormat:@"%@\n Lessons",selectedStyleName] subtitle:@"Loading"];
	/****************Asynchronous Request**********************/
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_CATEGORY_DETAIL_LIST_LINK(selectedStyleID)] cachePolicy:NO timeoutInterval:15.0];
	songsListConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	//DebugLog(@"LearnDanceView: %@",urlRequest.URL);
}

-(void)playThumnailVideo:(UITableViewCell *)cell index:(NSIndexPath *)lindexPath
{
    DMDSongObj *tempsongObj  = [songArray objectAtIndex:lindexPath.row];
    DebugLog(@"LearnDanceView: VIDEO URL=%@",[DMD_APP_DELEGATE URLEncode:tempsongObj.videoLink]);
    
    PlayVideoViewController *playVideoController = [[PlayVideoViewController alloc] initWithNibName:@"PlayVideoViewController" bundle:nil];
    playVideoController.urlString = [DMD_APP_DELEGATE URLEncode:tempsongObj.videoLink];
    [self.navigationController pushViewController:playVideoController animated:YES];
    return;
    
    
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[DMD_APP_DELEGATE URLEncode:tempsongObj.videoLink]]];
    self.moviePlayer.view.frame = CGRectMake(10, 3, 97, 89);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];
    //working
    moviePlayer.view.tag = 420;
    [cell.contentView addSubview:moviePlayer.view];
    [cell bringSubviewToFront:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

-(void)stopVideo
{
    if(self.moviePlayer != nil) {
        close = 1;
        [moviePlayer stop];
        //DebugLog(@"LearnDanceView: close=%d",close);
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    DebugLog(@"LearnDanceView: moviePlayBackDidFinish close=%d",close);
    [moviePlayer setFullscreen:FALSE animated:TRUE];
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    moviePlayer = nil;
}

- (void) moviePlayEnterFullScreen:(NSNotification*)notification {
    DebugLog(@"LearnDanceView: moviePlayEnterFullScreen");
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
}

- (void) moviePlayExitFullScreen:(NSNotification*)notification {
    DebugLog(@"LearnDanceView: moviePlayExitFullScreen");
    fullscreen = 0;
    [moviePlayer play];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"LearnDanceView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    if (connection == songsListConnection) {
        if(responseAsyncData != nil)
        {
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseAsyncData options:kNilOptions error:&error];
            DebugLog(@"LearnDanceView: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    [songArray removeAllObjects];
                    for (NSDictionary* songattributeDict in dataArray) {
                        songObj = [[DMDSongObj alloc] init];
                        songObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                        songObj.songTitle = [songattributeDict objectForKey:@"Title"];
                        songObj.songMovie = [songattributeDict objectForKey:@"Movie"];
                        songObj.songChoreographer = [songattributeDict objectForKey:@"Choreographer"];
                        songObj.songPic = [songattributeDict objectForKey:@"Thumbnail"];
                        songObj.songLock = [songattributeDict objectForKey:@"Lock_Type"];
                        songObj.songCategory = [songattributeDict objectForKey:@"Category"];
                        songObj.songCategoryName = [songattributeDict objectForKey:@"Category_Name"];
                        songObj.isVideo = [songattributeDict objectForKey:@"Is_Video"];
                        songObj.videoLink = [songattributeDict objectForKey:@"Video_Link"];
                        //                DebugLog(@"LearnDanceView: \nsongid: %@ , title: %@  , movie: %@  , choreographer: %@ , picture: %@\n", songObj.songId, songObj.songTitle, songObj.songMovie, songObj.songChoreographer, songObj.songPic);
                        [songArray addObject:songObj];
                        songObj = nil;
                    }
                }
            }
            responseAsyncData = nil;
            DebugLog(@"LearnDanceView: array count: %d", [songArray count]);
            [songsTableView setHidden:NO];
            [songsTableView reloadData];
        }
        songsListConnection = nil;
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 97;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [songArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UILabel *lblSongTitle;
    UILabel *lblSong;
    UILabel *lblMovieTitle;
    UILabel *lblMovie;
    UILabel *lblChoreoTitle;
    UILabel *lblChoreographer;
    UILabel *lblCredits;
    UIView *swipeView;
    static NSString *CellIdentifier = @"SongCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 97, 89)];
        thumbImg.tag = 1;
        thumbImg.userInteractionEnabled = TRUE;
        thumbImg.multipleTouchEnabled = TRUE;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        [cell.contentView addSubview:thumbImg];
        
        
        UIButton *videobutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [videobutton setFrame:CGRectMake(10, 3, 97, 89)];
        [videobutton setBackgroundColor:[UIColor clearColor]];
        videobutton.tag = 100;
        [videobutton addTarget:self action:@selector(songIntroBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:videobutton];
        
//        lblSongTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+58,27,115,15)];
//        lblSongTitle.text = @"song";
//        lblSongTitle.font = KABEL_FONT(12);
//        lblSongTitle.textAlignment = UITextAlignmentLeft;
//        lblSongTitle.textColor = [UIColor whiteColor];
//        lblSongTitle.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblSongTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblSong = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+56,20,115,60)];
        lblSong.tag = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblSong.numberOfLines = 4;
        lblSong.font = KABEL_FONT(18);
        lblSong.adjustsFontSizeToFitWidth = YES;
        lblSong.textAlignment = UITextAlignmentLeft;
        lblSong.textColor = [UIColor whiteColor];
        lblSong.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblSong];
        
        
        // Create the swipe view
        swipeView = [[UIView alloc] initWithFrame:CGRectMake(288,3,0, 91)];
        swipeView.tag = 50;
        swipeView.clipsToBounds = TRUE;
        swipeView.layer.masksToBounds = YES;
        //swipeView.alpha = 0.3;
        swipeView.userInteractionEnabled = TRUE;
        swipeView.backgroundColor = [UIColor clearColor];
        //[swipeView setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"song_credits_bg.png"]]];
        //[swipeView setBackgroundColor:[UIColor colorWithRed:0.30 green:0.27 blue:0.23 alpha:0.7]];
        
        UIButton *bgbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgbutton setFrame:CGRectMake(0, 0,140, 91)];
        [bgbutton setAlpha:0.8];
//        [bgbutton setBackgroundColor:[UIColor redColor]];
        [bgbutton setTitle:@"" forState:UIControlStateNormal];
        [bgbutton setTitle:@"" forState:UIControlStateHighlighted];
        [bgbutton setBackgroundImage:[UIImage imageNamed:@"song_credits_bg.png"] forState:UIControlStateNormal];
        [bgbutton setBackgroundImage:[UIImage imageNamed:@"song_credits_bg.png"] forState:UIControlStateHighlighted];
        [bgbutton addTarget:self action:@selector(songcCreditsBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [swipeView addSubview:bgbutton];
        
        lblMovieTitle = [[UILabel alloc] initWithFrame:CGRectMake(10.0,5.0,100.0,10.0)];
        lblMovieTitle.text = @"Movie";
        lblMovieTitle.tag = 222;
        lblMovieTitle.font = KABEL_FONT(10);
        lblMovieTitle.adjustsFontSizeToFitWidth = YES;
        lblMovieTitle.textAlignment = UITextAlignmentLeft;
        lblMovieTitle.textColor = GRAY_TEXT_COLOR;
        lblMovieTitle.backgroundColor =  [UIColor clearColor];
        [swipeView addSubview:lblMovieTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblMovie = [[UILabel alloc] initWithFrame:CGRectMake(10.0,15.0,150.0,20.0)];
        lblMovie.tag = 3;
        lblMovie.minimumFontSize = 12;
        lblMovie.adjustsFontSizeToFitWidth = TRUE;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMovie.font = KABEL_FONT(12);
        lblMovie.textAlignment = UITextAlignmentLeft;
        lblMovie.textColor = GRAY_TEXT_COLOR;
        lblMovie.backgroundColor =  [UIColor clearColor];
        lblMovie.numberOfLines = 2;
        //[cell.contentView addSubview:lblMovie];
        [swipeView addSubview:lblMovie];
        
        
        lblChoreoTitle = [[UILabel alloc] initWithFrame:CGRectMake(10.0,40,100,10.0)];
        lblChoreoTitle.text = @"Choreographer";
        lblChoreoTitle.tag = 333;
        lblChoreoTitle.font = KABEL_FONT(10);
        lblChoreoTitle.adjustsFontSizeToFitWidth = YES;
        lblChoreoTitle.textAlignment = UITextAlignmentLeft;
        lblChoreoTitle.textColor = GRAY_TEXT_COLOR;
        lblChoreoTitle.backgroundColor =  [UIColor clearColor];
        [swipeView addSubview:lblChoreoTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblChoreographer = [[UILabel alloc] initWithFrame:CGRectMake(10.0,45,150,20.0)];
        lblChoreographer.tag = 4;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblChoreographer.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblChoreographer.numberOfLines = 2;
        lblChoreographer.font = KABEL_FONT(12);
        lblChoreographer.adjustsFontSizeToFitWidth = YES;
        lblChoreographer.textAlignment = UITextAlignmentLeft;
        lblChoreographer.textColor = GRAY_TEXT_COLOR;
        lblChoreographer.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblChoreographer];
        [swipeView addSubview:lblChoreographer];
        
        [cell.contentView addSubview:swipeView];
        
        
        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
        UIImage *image1 = [UIImage imageNamed:@"song_credits_on.png"];
        //DebugLog(@"LearnDanceView: %f %f",cell.frame.size.width , image.size.width);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(288,3,30, 91);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 5;
        [button setAlpha:0.8];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setBackgroundImage:image1 forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(songcCreditsBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        
        //Initialize Label with tag 2.(Title Label)
        lblCredits = [[UILabel alloc] initWithFrame:CGRectMake(255,30,91,30.0)];
        lblCredits.tag = 6;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblChoreographer.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblCredits.text = @"Song Credits";
        lblCredits.font = KABEL_FONT(12);
        lblCredits.adjustsFontSizeToFitWidth = YES;
        lblCredits.textAlignment = UITextAlignmentCenter;
        lblCredits.textColor = GRAY_TEXT_COLOR;
        lblCredits.backgroundColor =  [UIColor clearColor];
        lblCredits.transform = CGAffineTransformMakeRotation(M_PI/-2);
        [cell.contentView addSubview:lblCredits];
        
        UIImageView *videoImg = [[UIImageView alloc] init];
        videoImg.image = [UIImage imageNamed:@"playbutton.png"];
        videoImg.contentMode = UIViewContentModeScaleAspectFit;
        videoImg.tag = 7;
        [videoImg setHidden:TRUE];
        //DebugLog(@"LearnDanceView: %f %f",bgbutton.center.x+50,bgbutton.center.y-50);
        videoImg.frame = CGRectMake(40.0,18,50.0,50.0);
        [cell.contentView addSubview:videoImg];
        
        UIImageView *arrowImg = [[UIImageView alloc] init];
        arrowImg.image = [UIImage imageNamed:@"arrow.png"];
        arrowImg.contentMode = UIViewContentModeScaleAspectFit;
        arrowImg.tag = 5;
        arrowImg.frame = CGRectMake(270,42,15,17);
        [cell.contentView addSubview:arrowImg];
        
    }
    DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
    lblSong = (UILabel *)[cell viewWithTag:2];
    lblMovie = (UILabel *)[cell viewWithTag:3];
    lblChoreographer = (UILabel *)[cell viewWithTag:4];
    lblSong.text = [NSString stringWithFormat:@"%@",tempsongObj.songTitle];
    
    
    if (tempsongObj.songMovie == nil || [tempsongObj.songMovie isEqualToString:@""]) {
        lblMovieTitle = (UILabel *)[cell viewWithTag:222];
        lblMovieTitle.text = @"";
    }else
    {
        lblMovie.text = [NSString stringWithFormat:@"%@",tempsongObj.songMovie];
    }
    
    if (tempsongObj.songChoreographer == nil || [tempsongObj.songChoreographer isEqualToString:@""]) {
        lblChoreoTitle = (UILabel *)[cell viewWithTag:333];
        lblChoreoTitle.text = @"";
    }else{
        lblChoreographer.text = [NSString stringWithFormat:@"%@",tempsongObj.songChoreographer];
    }
    
    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:[DMD_APP_DELEGATE URLEncode:tempsongObj.songPic]]
                 placeholderImage:nil
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    UIImageView *playImgview = (UIImageView *)[cell viewWithTag:7];
    if([tempsongObj.isVideo isEqualToString:@"1"])
    {
        [playImgview setHidden:FALSE];
    } else {
        [playImgview setHidden:TRUE];
    }
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
    bg.image = [UIImage imageNamed:@"tab_off.png"];
    bgon.image = [UIImage imageNamed:@"tab_on.png"];
    cell.backgroundView = bg;
    cell.selectedBackgroundView = bgon;
    cell.contentView.clipsToBounds = YES;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
    DMDSongDetailViewController *typedetailController = [[DMDSongDetailViewController alloc] initWithNibName:@"DMDSongDetailViewController" bundle:nil] ;
    typedetailController.title = tempsongObj.songTitle;
    typedetailController.songID = tempsongObj.songId;
    typedetailController.songName = tempsongObj.songTitle;
    typedetailController.styleName = selectedStyleName;
    [self.navigationController pushViewController:typedetailController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
    [self stopVideo];
    
    UITableViewCell *cell  = (UITableViewCell *)[songsTableView cellForRowAtIndexPath:indexPath];
    UIView *swipeView = (UIView *)[cell viewWithTag:50];
    UIButton *button = (UIButton *)[cell viewWithTag:5];
    UILabel *label = (UILabel *)[cell viewWithTag:6];
    UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
    [swipeView setFrame:CGRectMake(288, 3, 0, 91)];
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:image forState:UIControlStateHighlighted];
    [label setHidden:FALSE];
}
#pragma mark -
#pragma UIScrollViewDelegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)sender {
//    if ([sender isEqual:[self songsTableView]]) {
//        UITableViewCell *openCell = (UITableViewCell *) [self.songsTableView cellForRowAtIndexPath:openCellIndexPath];
//        UIView *swipeView = (UIView *)[openCell viewWithTag:50];
//        UIButton *button = (UIButton *)[openCell viewWithTag:5];
//        UILabel *label = (UILabel *)[openCell viewWithTag:6];
//        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
//        
//        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//        // Swipe top view right
//        [UIView animateWithDuration:1.0 animations:^{
//            [swipeView setFrame:CGRectMake(320, 1, 0, 120)]; } completion:^(BOOL finished) {
//                [UIView  transitionWithView:button duration:0.5
//                                    options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
//                                 animations:^(void) {
//                                     [button setBackgroundImage:image forState:UIControlStateNormal];
//                                     [button setBackgroundImage:image forState:UIControlStateHighlighted];
//                                     [UIView  transitionWithView:label duration:1.0
//                                                         options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
//                                                      animations:^(void) {
//                                                          [label setHidden:FALSE];
//                                                      }
//                                                      completion:^(BOOL finished) {
//                                                      }];
//                                 }
//                                 completion:^(BOOL finished) {
//                                     openCellIndexPath = nil;
//                                 }];
//            }];
//    }
}

#pragma UIButton delegate Methods
- (void)songcCreditsBtnTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.songsTableView];
	NSIndexPath *indexPath = [self.songsTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.songsTableView cellForRowAtIndexPath:indexPath];
        UIView *swipeView = (UIView *)[cell viewWithTag:50];
        UIButton *button = (UIButton *)[cell viewWithTag:5];
        UILabel *label = (UILabel *)[cell viewWithTag:6];
        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
        UIImage *image1 = [UIImage imageNamed:@"song_credits_on.png"];
        if(swipeView.frame.origin.x > 148) {
            [button setBackgroundImage:image1 forState:UIControlStateNormal];
            [button setBackgroundImage:image1 forState:UIControlStateHighlighted];
            [label setHidden:TRUE];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            // Swipe top view left
            [UIView animateWithDuration:1.0 animations:^{
                [swipeView setFrame:CGRectMake(148, 3,140, 91)]; } completion:^(BOOL finished) {
//                    openCellIndexPath = indexPath;
                }];
        } else {
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            // Swipe top view right
            [UIView animateWithDuration:1.0 animations:^{
                [swipeView setFrame:CGRectMake(288, 3, 0, 91)];
            } completion:^(BOOL finished) {
                    [UIView  transitionWithView:button duration:0.5
                                        options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
                                     animations:^(void) {
                                         [button setBackgroundImage:image forState:UIControlStateNormal];
                                         [button setBackgroundImage:image forState:UIControlStateHighlighted];
                                         [UIView  transitionWithView:label duration:1.0
                                                             options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
                                                          animations:^(void) {
                                                              [label setHidden:FALSE];
                                                          }
                                                          completion:^(BOOL finished) {
                                                          }];
                                     }
                                     completion:^(BOOL finished) {
                                     }];
                }];
        }
    }
}

- (void)songIntroBtnTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.songsTableView];
	NSIndexPath *indexPath = [self.songsTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.songsTableView cellForRowAtIndexPath:indexPath];
        //UIButton *button = (UIButton *)[cell viewWithTag:100];
        //DebugLog(@"LearnDanceView: Button Tap = %d",indexPath.row);
        DMDSongObj *tempsongObj  = [songArray objectAtIndex:indexPath.row];
        //DebugLog(@"LearnDanceView: ----IS VIDEO--->%@",tempsongObj.isVideo);
        if([tempsongObj.isVideo isEqualToString:@"1"])
        {
            [self playThumnailVideo:cell index:indexPath];
        }
    }
}

@end
