//
//  DMDExpertsObj.h
//  DancewithMD
//
//  Created by Sagar Mody on 13/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDExpertsObj : NSObject {
    
}

@property (nonatomic, copy) NSString *clipId;
@property (nonatomic, copy) NSString *clipTitle;
@property (nonatomic, copy) NSString *videoThumb;
@property (nonatomic, copy) NSString *videoType;
@property (nonatomic, copy) NSString *videoLink;
@property (nonatomic, copy) NSString *videoText;
@property (nonatomic, copy) NSString *embedCode;
@end
