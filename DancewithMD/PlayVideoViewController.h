//
//  PlayVideoViewController.h
//  PlayMovie
//
//  Created by Kirti Nikam on 10/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface PlayVideoViewController : UIViewController
@property (nonatomic,strong) NSString *urlString;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (nonatomic, assign) BOOL statusBarHidden;
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;
@end
