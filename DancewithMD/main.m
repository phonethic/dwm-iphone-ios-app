//
//  main.m
//  DancewithMD
//
//  Created by Kirti Nikam on 10/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DMDAppDelegate class]));
    }
}
