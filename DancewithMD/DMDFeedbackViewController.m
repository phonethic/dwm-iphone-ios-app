//
//  DMDFeedbackViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDFeedbackViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"

#import "DMDFeedbackObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CommonCallBack.h"

#define FEEDBACK_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/feedbacks",LIVE_DWM_SERVER]

@implementation UITextView (DisableCopyPaste)
- (BOOL)canBecomeFirstResponder
{
    return NO;
}
@end

@interface DMDFeedbackViewController ()
//@property (nonatomic, retain) NSIndexPath *expandedCellIndexPath;
//@property (nonatomic) CGFloat expandedCellHeight;
@end

@implementation DMDFeedbackViewController
@synthesize feedbackArray;
@synthesize feedbackTableView;
@synthesize dataFilePath;
//@synthesize expandedCellHeight;
//@synthesize expandedCellIndexPath;
@synthesize feedbackView,feedbackTextView,feedbackuserImageView,feedbackusernameLabel,closeBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_FeedBack"];
    [[LocalyticsSession shared] tagScreen:@"FeedBack"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupMenuBarButtonItems];
    [self setfeedDataFilePath];
    [self setUI];
    
    UIBarButtonItem *feedBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Send Feedback" style:UIBarButtonItemStylePlain target:self action:@selector(sendFeedBack_Clicked:)];
    self.navigationItem.leftBarButtonItem = feedBackButton;
    
    feedbackArray = [[NSMutableArray alloc] init];
    if([DMD_APP_DELEGATE networkavailable])
    {
        [self feedbackListAsynchronousCall];
    } else {
        [self parseFromFile];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    if (feedBackconnection != nil) {
        [feedBackconnection cancel];
        feedBackconnection = nil;
    }
}
- (void)viewDidUnload {
    [self setFeedbackTableView:nil];
    [self setFeedbackView:nil];
    [self setFeedbackTextView:nil];
    [self setFeedbackuserImageView:nil];
    [self setFeedbackusernameLabel:nil];
    [self setCloseBtn:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
    
}


#pragma Internal Callbacks
-(void)setUI{
    feedbackView.layer.cornerRadius = 20;
    feedbackView.layer.masksToBounds = YES;
    feedbackView.layer.borderColor = [UIColor whiteColor].CGColor;
    feedbackView.layer.borderWidth = 4.0;
    
    feedbackuserImageView.layer.cornerRadius = 10;
    feedbackuserImageView.layer.masksToBounds = YES;
    
    closeBtn.layer.cornerRadius = 20;
    closeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    closeBtn.layer.borderWidth = 3.0;
    
    feedbackusernameLabel.textColor = [UIColor whiteColor];
    feedbackTextView.textColor = [UIColor whiteColor];
    
    feedbackusernameLabel.backgroundColor = [UIColor clearColor];
    feedbackTextView.backgroundColor = [UIColor clearColor];
    
    feedbackusernameLabel.font = KABEL_FONT(18);
    feedbackTextView.font = KABEL_FONT(15);
    
    feedbackView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.9];
    [self showFeedBackView:NO];
}

-(void)showFeedBackView:(BOOL)value{
    if (value) {
        [feedbackView setHidden:NO];
        [closeBtn setHidden:NO];
        feedbackTableView.userInteractionEnabled = FALSE;
    }else{
        [feedbackView setHidden:YES];
        [closeBtn setHidden:YES];
        feedbackTableView.userInteractionEnabled = TRUE;
    }
}
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (feedBackconnection != nil) {
                             [feedBackconnection cancel];
                             feedBackconnection = nil;
                         }
                     }];
}

-(void)feedbackListAsynchronousCall
{
    [self showProgressHudWithCancel:@"Feedback" subtitle:@"Loading"];
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:FEEDBACK_LIST_LINK] cachePolicy:NO timeoutInterval:10.0];
    //DebugLog(@" FeedBack: %@",urlRequest.URL);
    //Get list of all songs
    feedBackconnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
-(void)setfeedDataFilePath
{
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the data file
    dataFilePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"feedback.archive"]];
}
-(void)parseFromFile
{

    NSFileManager *filemgr = [NSFileManager defaultManager];
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        feedbackArray = [NSKeyedUnarchiver unarchiveObjectWithFile: dataFilePath];
        DebugLog(@"from file feedbackArray %@",feedbackArray);
        
        if (feedbackArray.count <= 0) {
            [self showError];
        }else{
            [CommonCallBack hideProgressHud];
            [feedbackTableView reloadData];
        }
    }else
    {
        [self showError];
    }
}
-(void)showError
{
    [CommonCallBack hideProgressHudWithError];
    UIAlertView *errorView = [[UIAlertView alloc]
                              initWithTitle:@"No Network Connection"
                              message:@"Please check your internet connection and try again."
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [errorView show];
}
#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)sendFeedBack_Clicked:(id)sender {
    @try{
        MFMailComposeViewController *picker=[[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate=self;
        NSString *msgBody = [NSString stringWithFormat:@""];
        picker.navigationBar.tintColor = [UIColor blackColor];
        [picker setToRecipients:[NSArray arrayWithObject:@"feedback@rnmmovingpictures.com"]];
        [picker setSubject:@""];
        [picker setMessageBody:msgBody isHTML:NO];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
    
}

#pragma mark mailComposeController Delegate Methods

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result == MFMailComposeResultSent)
    {
        //DebugLog(@" FeedBack: Mail sent");
    }
    [controller dismissModalViewControllerAnimated:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@" FeedBack: Error: %@", [error localizedDescription]);
    [self connectionDidFinishLoading:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)lconnection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if(lconnection==feedBackconnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@" FeedBack: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    [feedbackArray removeAllObjects];
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* feedbackattributeDict in dataArray)
                    {
                        feedbackObj = [[DMDFeedbackObj alloc] init];
                        feedbackObj.userId = [feedbackattributeDict objectForKey:@"User_ID"];
                        feedbackObj.userName = [feedbackattributeDict objectForKey:@"User_Name"];
                        NSString * picUrl = [feedbackattributeDict objectForKey:@"Picture"];
                        if([picUrl hasPrefix:@"images/"])
                        {
                            //DebugLog(@" FeedBack: %@",[NSString stringWithFormat:@"http://dancewithmadhuri.com/%@",picUrl]);
                            feedbackObj.picture = [NSString stringWithFormat:@"http://dancewithmadhuri.com/%@",picUrl];
                        } else {
                            feedbackObj.picture = picUrl;
                        }
                        feedbackObj.feedbackText = [feedbackattributeDict objectForKey:@"Feedback_Txt"];
                        feedbackObj.name = [feedbackattributeDict objectForKey:@"Name"];
//                        DebugLog(@" FeedBack: \n userId: %@ , name: %@  , picture: %@  , feedbackText: %@  , name: %@ \n", feedbackObj.userId, feedbackObj.userName, feedbackObj.picture, feedbackObj.feedbackText, feedbackObj.name);
                        [feedbackArray addObject:feedbackObj];
                        feedbackObj = nil;
                    }
                }
            }
            responseAsyncData = nil;
            feedBackconnection = nil;
            [NSKeyedArchiver archiveRootObject:feedbackArray toFile:dataFilePath];
            [feedbackTableView reloadData];
        }
        else{
            [self parseFromFile];
        }
    }
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = 100.0f;
//    if ([self.expandedCellIndexPath isEqual:indexPath]) {
//        cellHeight = self.expandedCellHeight;
//    }
    return cellHeight;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [feedbackArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"FeedBackCell";
    UILabel *lblName;
    UILabel *lblMessage;
    UIView *contentView;
    
    CGFloat cellHeight = 100;
//    CGFloat cellHeight = cell.frame.size.height;
    //    if ([self.expandedCellIndexPath isEqual:indexPath]) {
    //        cellHeight = self.expandedCellHeight;
    //    }
//    DebugLog(@" FeedBack: index %d cellheight %f",indexPath.row,cell.frame.size.height);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        contentView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, cellHeight-20)];
        contentView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        contentView.tag = 11;
        contentView.layer.cornerRadius = 10;
        contentView.layer.masksToBounds = YES;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 50, 50)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.cornerRadius = 25;
        thumbImg.layer.borderColor = [UIColor darkGrayColor].CGColor;
        thumbImg.layer.borderWidth = 4;
        [contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame) + 5,10,235.0,20.0)];
        lblName.tag = 2;
        //        lblName.shadowColor   = [UIColor blackColor];
        //        lblName.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblName.font = KABEL_FONT(20);//[UIFont fontWithName:@"Cochin-Bold" size:24];
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.textColor = [UIColor whiteColor];
        lblName.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblName];
        
        //Initialize Label with tag 2.(Title Label)
        lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+ 5,CGRectGetMaxY(lblName.frame),235.0,contentView.frame.size.height-CGRectGetMaxY(lblName.frame))];
        lblMessage.tag = 3;
        lblMessage.numberOfLines = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMessage.adjustsFontSizeToFitWidth = TRUE;
        lblMessage.minimumFontSize = 15.0;
        lblMessage.font = KABEL_FONT(15);//[UIFont fontWithName:@"Cochin-Bold" size:20];
        lblMessage.textAlignment = UITextAlignmentLeft;
        lblMessage.textColor = [UIColor lightGrayColor];
        lblMessage.backgroundColor =  [UIColor clearColor];
        [contentView addSubview:lblMessage];
        
        [cell.contentView addSubview:contentView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0];
        cell.selectedBackgroundView = bgColorView;
    }
    
    cell.userInteractionEnabled = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;

    contentView = (UIView *)[cell viewWithTag:11];
    DMDFeedbackObj *tempfeedObj  = [feedbackArray objectAtIndex:indexPath.row];
    lblName = (UILabel *)[contentView viewWithTag:2];
    lblMessage = (UILabel *)[contentView viewWithTag:3];
    
//    DebugLog(@" FeedBack: cellHeight %f",cellHeight);
//    contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width, cellHeight-20);
//    lblMessage.frame = CGRectMake(lblMessage.frame.origin.x, lblMessage.frame.origin.y, lblMessage.frame.size.width, contentView.frame.size.height-CGRectGetMaxY(lblName.frame));
//    DebugLog(@" FeedBack: contentView %f",contentView.frame.size.height);
//    DebugLog(@" FeedBack: lblMessage %f",lblMessage.frame.size.height);
    
    if(![tempfeedObj.userName isEqualToString:@""])
        lblName.text = tempfeedObj.userName;
    else
        lblName.text = tempfeedObj.name;
    
    lblMessage.text = tempfeedObj.feedbackText;
    
    UIImageView *thumbImgview = (UIImageView *)[contentView viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:tempfeedObj.picture]
                 placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self showFeedBackView:YES];
    DMDFeedbackObj *tempfeedObj   =   (DMDFeedbackObj *)[feedbackArray objectAtIndex:indexPath.row];
    [feedbackuserImageView setImageWithURL:[NSURL URLWithString:tempfeedObj.picture]
                          placeholderImage:[UIImage imageNamed:@"unknown.jpg"]
                                   success:^(UIImage *image) {
                                       //DebugLog(@"success");
                                   }
                                   failure:^(NSError *error) {
                                       //DebugLog(@"write error %@", error);
                                   }];
    if(![tempfeedObj.userName isEqualToString:@""])
        feedbackusernameLabel.text = tempfeedObj.userName;
    else
        feedbackusernameLabel.text = tempfeedObj.name;
    
    feedbackTextView.text    =   tempfeedObj.feedbackText;
    
    //if user taps the expanded cell, just make it unexpanded
   /* if ([self.expandedCellIndexPath isEqual:indexPath])
        self.expandedCellIndexPath = nil;
    else {
        //make cell with this indexpath  expanded
        self.expandedCellIndexPath = indexPath;
        //get the pointer to the cell which was tapped
        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        //define the maximum size for description label as
        //CGSize with width of description label and infinite height
        UIView *contentView = (UIView *)[cell viewWithTag:11];
        UILabel *lblMessage = (UILabel *)[contentView viewWithTag:3];
        
        CGSize maximumSize = CGSizeMake(lblMessage.bounds.size.width, NSIntegerMax);
        //this method returns the expected size of the description label with respect to the amount
        //of text it contains
        CGSize expectedSize = [lblMessage.text sizeWithFont:lblMessage.font constrainedToSize:maximumSize lineBreakMode:lblMessage.lineBreakMode];
        //this defines the height of expanded cell adding the delta between current
        //label height and expected height to  cell content view height.
        self.expandedCellHeight =  cell.contentView.bounds.size.height - lblMessage.bounds.size.height + expectedSize.height;
        
    }
    //this asks tableview to redraw cells
    //actually it invokes reload of cell just if it's height was changed
    //and animates the resize of the cell
    [feedbackTableView beginUpdates];
    [feedbackTableView endUpdates];*/
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

#pragma UIButton delegate Method
- (IBAction)closeBtnClicked:(id)sender {
    [self showFeedBackView:NO];
}
@end
