//
//  DMDLoginViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDLoginViewController.h"
#import "DMDAppDelegate.h"
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import "CommonCallBack.h"

//for DWM User
#define REGISTER_LINK [NSString stringWithFormat:@"%@/api/retrieve/registeruser",LIVE_DWM_SERVER]
#define LOGIN_LINK [NSString stringWithFormat:@"%@/api/retrieve/login",LIVE_DWM_SERVER]
#define USER_DETAIL_LINK(USER_ID,TOKEN) [NSString stringWithFormat:@"%@/api/retrieve/user_profile/%@/%@",LIVE_DWM_SERVER,USER_ID,TOKEN]
#define FORGET_PASSWORD_LINK [NSString stringWithFormat:@"%@/api/retrieve/resetpass",LIVE_DWM_SERVER]

//for facebook User
#define USER_REGISTRATION_LINK(USERID) [NSString stringWithFormat:@"%@/api/retrieve/user_profile/%@/0",LIVE_DWM_SERVER,USERID]
#define USER_ADD_LINK [NSString stringWithFormat:@"%@/api/retrieve/user_profile",LIVE_DWM_SERVER]

//for app
#define JHALAK_CHECK_LINK @"http://dancewithmadhuri.com/api/retrieve/validjhalak"
#define VERSION_CHECK_LINK [NSString stringWithFormat:@"%@/api/retrieve/validapi",LIVE_DWM_SERVER]

//animation durations
#define FLIP_DURATION 1.0
#define DELAY_DURATION 3.0

@interface DMDLoginViewController ()

@end

@implementation DMDLoginViewController
@synthesize MainView,loginProceeedBtn;

@synthesize bgImageView;

@synthesize registerScrollView,registerView,registerBtn,cancelBtn,maleBtn,femaleBtn,registeremailtextfield,registernametextfield,registerpasstextfield,registervpasstextfield;

@synthesize fbLoginBtn,lblOR,loginView,loginusernametextfield,loginpasswordtextfield,loginBtn,forgetPassBtn,signupBtn,lblsignUp;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifeCycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    
    [CommonCallBack hideProgressHud];
    MainView.hidden = FALSE;
    registerView.hidden = FALSE;
    [self.view bringSubviewToFront:MainView];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    [self checkUserStatus];

    if([DMD_APP_DELEGATE networkavailable]) {
        [self checkJhalakAsynchronousCall];
        [self checkApiVersionAsynchronousCall];
    }
//    #ifdef DEBUG
//        loginusernametextfield.text = DEVELOPER_USER;
//        loginpasswordtextfield.text = DEVELOPER_PASSWORD;
//    #endif
}
-(void)viewWillDisappear:(BOOL)animated
{ 
    [super viewWillDisappear:animated];
    DebugLog(@"Login: viewWillDisappear");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    DebugLog(@"Login: dealloc");
    if (loginConnection != nil) {
        [loginConnection cancel];
        loginConnection = nil;
    }
    if (insertIntoDBConnection != nil) {
        [insertIntoDBConnection cancel];
        insertIntoDBConnection = nil;
    }
    if (registrationConnection != nil) {
        [registrationConnection cancel];
        registrationConnection = nil;
    }
    if (userDetailConnection != nil) {
        [userDetailConnection cancel];
        userDetailConnection = nil;
    }
    if (resetPasswordConnection != nil) {
        [resetPasswordConnection cancel];
        resetPasswordConnection = nil;
    }
    if (jhalakConnection != nil) {
        [jhalakConnection cancel];
        jhalakConnection = nil;
    }
    if (apiversionCheckConnection != nil) {
        [apiversionCheckConnection cancel];
        apiversionCheckConnection = nil;
    }
}
- (void)viewDidUnload {
    [self setRegisterView:nil];
    [self setRegisterBtn:nil];
    [self setCancelBtn:nil];
    [self setMaleBtn:nil];
    [self setFemaleBtn:nil];
    [self setRegisterpasstextfield:nil];
    [self setRegisteremailtextfield:nil];
    [self setRegisternametextfield:nil];
    [self setRegistervpasstextfield:nil];
    [self setBgImageView:nil];
    [self setMainView:nil];
    [self setLoginProceeedBtn:nil];
    [self setRegisterScrollView:nil];
    [self setLoginView:nil];
    [self setLoginusernametextfield:nil];
    [self setLoginpasswordtextfield:nil];
    [self setLoginBtn:nil];
    [self setForgetPassBtn:nil];
    [self setSignupBtn:nil];
    [self setLblsignUp:nil];
    [self setFbLoginBtn:nil];
    [self setLblOR:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Internal Callbacks
-(void)setUI{
    
    lblOR.textColor = [UIColor whiteColor];
    lblOR.backgroundColor = [UIColor clearColor];
    lblOR.font = KABEL_FONT(22);
    
    lblsignUp.textColor = [UIColor whiteColor];
    lblsignUp.backgroundColor = [UIColor clearColor];
    lblsignUp.font = KABEL_FONT(22);

    loginusernametextfield.font = KABEL_FONT(16);
    loginpasswordtextfield.font = KABEL_FONT(16);
    registeremailtextfield.font = KABEL_FONT(16);
    registernametextfield.font  = KABEL_FONT(16);
    registerpasstextfield.font  = KABEL_FONT(16);
    registervpasstextfield.font = KABEL_FONT(16);
    
    loginBtn.titleLabel.font        = KABEL_FONT(16);
    forgetPassBtn.titleLabel.font   = KABEL_FONT(16);
    signupBtn.titleLabel.font       = KABEL_FONT(18);
    
    registerBtn.titleLabel.font     = KABEL_FONT(16);
    cancelBtn.titleLabel.font       = KABEL_FONT(16);
    maleBtn.titleLabel.font         = KABEL_FONT(16);
    femaleBtn.titleLabel.font       = KABEL_FONT(16);
    
    maleBtn.selected = TRUE;
}

-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (loginConnection != nil) {
                             [loginConnection cancel];
                             loginConnection = nil;
                         }
                         if (insertIntoDBConnection != nil) {
                             [insertIntoDBConnection cancel];
                             insertIntoDBConnection = nil;
                         }
                         if (registrationConnection != nil) {
                             [registrationConnection cancel];
                             registrationConnection = nil;
                         }
                         if (userDetailConnection != nil) {
                             [userDetailConnection cancel];
                             userDetailConnection = nil;
                         }
                         if (resetPasswordConnection != nil) {
                             [resetPasswordConnection cancel];
                             resetPasswordConnection = nil;
                         }
                         if (jhalakConnection != nil) {
                             [jhalakConnection cancel];
                             jhalakConnection = nil;
                         }
                         if (apiversionCheckConnection != nil) {
                             [apiversionCheckConnection cancel];
                             apiversionCheckConnection = nil;
                         }
                     }];
}

-(void)checkUserStatus{
    
    if([DMD_APP_DELEGATE networkavailable]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *userID = [defaults objectForKey:LOGIN_USERID];
        NSString *userPass = [defaults objectForKey:LOGIN_PASSWORD];
        if(userID != nil && userPass != nil && [userID length] > 0 && [userPass length] > 0)
        {
            DebugLog(@"Login: DWM Login  %@ %@",userID,userPass);
                [self loginAsynchronousCall:userID pass:userPass];
        }else
        {
            DebugLog(@"Login: Facebook Login  %@ %@",userID,userPass);
            // Register for notifications on FB session state changes
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionStateChanged:) name:FBSessionStateChangedNotification object:nil];
            // Check the session for a cached token to show the proper authenticated
            // UI. However, since this is not user intitiated, do not show the login UX.
            [DMD_APP_DELEGATE openSessionWithAllowLoginUI:NO];
        }
    }else{
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

-(void)checkJhalakAsynchronousCall
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:JHALAK_CHECK_LINK] cachePolicy:NO timeoutInterval:10.0];
    DebugLog(@"Login: %@",urlRequest.URL);
    jhalakConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)checkApiVersionAsynchronousCall
{
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:VERSION_CHECK_LINK] cachePolicy:NO timeoutInterval:10.0];
    //DebugLog(@"Login: %@",urlRequest.URL);
    apiversionCheckConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

//Login On DWM Server
-(void)loginAsynchronousCall:(NSString *)userEmail pass:(NSString *)password
{
    [self showProgressHudWithCancel:@"Login" subtitle:@"Processing"];
    NSString *md5Pass = [DMD_APP_DELEGATE md5:password];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGIN_LINK]  cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReqDic = [[NSMutableDictionary alloc] init];
    [postReqDic setObject:userEmail forKey:@"uname"];
    [postReqDic setObject:md5Pass forKey:@"pass"];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReqDic options:NSJSONWritingPrettyPrinted error:&error];
    
    if (jsonData) {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"Login: Login : jsonstring %@",jsonString);
    }
    
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    loginConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)userDetailsAsynchronousCall:(NSString *)userId token:(NSString *)userToken
{
//    [loginprogressbar startAnimating];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:USER_DETAIL_LINK(userId,userToken)] cachePolicy:NO timeoutInterval:15.0];
    //DebugLog(@"Login: link=%@", urlRequest.URL);
    userDetailConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)registerAsynchronousCall:(NSString *)userEmail name:(NSString *)userName gendertype:(NSString *)gender pass:(NSString *)password confirmpass:(NSString *)cpassword
{
    [self showProgressHudWithCancel:@"Register" subtitle:@"Processing"];
    NSString *md5Pass = [DMD_APP_DELEGATE md5:password];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:REGISTER_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    [postReq setObject:userName forKey:@"name"];
    [postReq setObject:gender forKey:@"gender"];
    [postReq setObject:md5Pass forKey:@"pass"];
    [postReq setObject:md5Pass forKey:@"confirm_pass"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //DebugLog(@"Login: Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"Login: ---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    registrationConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)resetpasswordAsynchronousCall:(NSString *)userEmail
{
    [self showProgressHudWithCancel:@"Forgot Password" subtitle:@"Processing"];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:FORGET_PASSWORD_LINK] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userEmail forKey:@"uname"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        //DebugLog(@"Login: Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"Login: ---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    resetPasswordConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)cancelModalView:(id)sender {
    [self.view.layer removeAllAnimations];
    if (![[self modalViewController] isBeingDismissed])
        [self dismissModalViewControllerAnimated:YES];
}

-(BOOL)validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}

-(void) clearTextFields
{
    [loginusernametextfield setText:@""];
    [loginpasswordtextfield setText:@""];
    [registeremailtextfield setText:@""];
    [registernametextfield setText:@""];
    [registerpasstextfield setText:@""];
    [registervpasstextfield setText:@""];
    
    #ifdef DEBUG
        loginusernametextfield.text = DEVELOPER_USER;
        loginpasswordtextfield.text = DEVELOPER_PASSWORD;
    #endif
}

-(void)scrollTobottom:(UIScrollView *)lscrollView
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [lscrollView setContentOffset:bottomOffset animated:YES];
}
-(void)scrollTotop : (int) value scrollView:(UIScrollView *)lscrollView
{
    CGPoint topOffset = CGPointMake(0, value);
    [lscrollView setContentOffset:topOffset animated:YES];
}

-(void)saveUserMetaDataInLocalytics:(NSString *)userid name:(NSString *)uname email:(NSString *)uemail gender:(NSString *)ugender
{
    DebugLog(@"saveUserMetaDataInLocalytics:userid %@ name:%@ email:%@ gender:%@",userid,uname,uemail,ugender);
    [[LocalyticsSession shared] setValueForIdentifier:@"User_ID" value:userid];
    [[LocalyticsSession shared] setValueForIdentifier:@"User_Name" value:uname];
    [[LocalyticsSession shared] setValueForIdentifier:@"User_Email" value:uemail];
    [[LocalyticsSession shared] setValueForIdentifier:@"User_Gender" value:ugender];
}

#pragma Login via Facebook
-(void)registerFbUserIntoDWMServerAsynchronousCall
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [defaults objectForKey:LOGIN_USERID];
	/****************Asynchronous Request**********************/
    NSString *requestString = [NSString stringWithString:USER_REGISTRATION_LINK(userID)];
    //DebugLog(@"Login: %@",requestString);
	/****************Asynchronous Request**********************/
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString] cachePolicy:NO timeoutInterval:10.0];
    
    fbUserRegistrationConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)insertUserInDB
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userId    = [userDefaults stringForKey:LOGIN_USERID];
    NSString *userName  = [userDefaults stringForKey:LOGIN_USERNAME];
    NSString *userEmail = [userDefaults stringForKey:LOGIN_EMAIL];
    NSString *userGender = [userDefaults stringForKey:LOGIN_GENDER];
    NSString *userPic    = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", [userDefaults stringForKey:LOGIN_USERID]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:USER_ADD_LINK]cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:userId forKey:@"User_ID"];
    [postReq setObject:userName forKey:@"User_Name"];
    if(userEmail!=nil)
        [postReq setObject:userEmail forKey:@"Email"];
    else
        [postReq setObject:@"" forKey:@"Email"];
    [postReq setObject:userGender forKey:@"Gender"];
    [postReq setObject:userPic forKey:@"Picture"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (!jsonData) {
        DebugLog(@"Login: Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"Login: ---%@---",jsonString);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    insertIntoDBConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma mark UIGestureRecognizer Methods

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (!registerScrollView.hidden) {
        [self scrollTobottom:registerScrollView];
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        status = [httpResponse statusCode];
        //DebugLog(@"Login: res code=%d",status);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [CommonCallBack hideProgressHudWithError];
    DebugLog(@"Login: %@",error.description);
    
//    [self showAlert:@"The request timed out."];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
    NSError* error;
    if (connection == loginConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"Login: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSDictionary *dataDict = [json objectForKey:@"data"];
                    if([dataDict valueForKey:@"token"] != nil) {
                        NSDictionary *storeParams =
                        [NSDictionary dictionaryWithObjectsAndKeys:
                         @"DWM_Server", @"Login_From",
                         nil];
                        [Flurry logEvent:@"Login" withParameters:storeParams];
                        [[LocalyticsSession shared] tagEvent:@"Login" attributes:storeParams];
                        
                        NSString * accessToken = [dataDict objectForKey:@"token"];
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:DWMSERVER forKey:LOGGED_BY];
                        [defaults setObject:loginusernametextfield.text forKey:LOGIN_USERID];
                        [defaults setObject:loginpasswordtextfield.text forKey:LOGIN_PASSWORD];
                        [defaults setObject:accessToken forKey:LOGIN_TOKEN];
                        NSDate *myDate = [NSDate date];
                        [defaults setObject:myDate forKey:LOGIN_EXPIREDDATE];
                        [defaults setObject:[NSNumber numberWithBool:0] forKey:LOGIN_HELPINFOSCREEN];
                        [defaults synchronize];
                        [self userDetailsAsynchronousCall:loginusernametextfield.text token:accessToken];
                    } else {
                        NSString * result = [dataDict objectForKey:@"data"];
                        DebugLog(@"data:%@",result);
                        [self showAlert:result];
                    }
                } else {
                    NSString * result = [json objectForKey:@"message"];
                    [self showAlert:result];
                }
            }
        }
        responseAsyncData = nil;
        loginConnection = nil;
    }else if (connection == userDetailConnection)
    {
        
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"Login: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                    for (NSDictionary* attributeDict in dataArray)
                    {
                        if([attributeDict valueForKey:@"User_Name"] != nil) {
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setObject:DWMSERVER forKey:LOGGED_BY];
                            [defaults setObject:[attributeDict valueForKey:@"ID"] forKey:LOGIN_USERID];
                            [defaults setObject:[attributeDict valueForKey:@"User_Name"] forKey:LOGIN_USERNAME];
                            [defaults setObject:[attributeDict valueForKey:@"Email"] forKey:LOGIN_EMAIL];
                            [defaults setObject:[attributeDict valueForKey:@"Gender"] forKey:LOGIN_GENDER];
                            [defaults setObject:[NSNumber numberWithBool:0] forKey:LOGIN_HELPINFOSCREEN];
                            [defaults synchronize];
                            //DebugLog(@"%@--%@--%@--%@",[defaults objectForKey:LOGIN_USERID],[defaults objectForKey:LOGIN_USERNAME],[defaults objectForKey:LOGIN_EMAIL],[defaults objectForKey:LOGIN_GENDER]);
                            [FBSession.activeSession closeAndClearTokenInformation];
                             [self cancelModalView:nil];
                            
                        [self saveUserMetaDataInLocalytics:[attributeDict valueForKey:@"ID"] name:[attributeDict valueForKey:@"User_Name"] email:[attributeDict valueForKey:@"Email"] gender:[attributeDict valueForKey:@"Gender"]];
                            
                        } else {
                            NSString * result = [attributeDict objectForKey:@"data"];
                            //DebugLog(@"%@",result);
                            if(result != nil && [result isEqualToString:@"0"])
                                [self showAlert:@"Failed to login.Please try after sometime."];
                        }
                    }
                }
            }
        }
        responseAsyncData = nil;
        userDetailConnection = nil;
    }else if (connection == registrationConnection)
    {
        if(responseAsyncData != nil)
        {

            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"Login: json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSString * result = [json objectForKey:@"data"];
                    if([result hasPrefix:@"Successfully registered"])
                    {
                        [self showAlert:@"User registered successfully"];
                        [self cancelBtnClicked:nil];
                    }
                } else {
                    NSString * result = [json objectForKey:@"message"];
                    if ([result hasPrefix:@"Already registered"]) {
                        [self showAlert:@"User already registered."];
                        [self cancelBtnClicked:nil];
                    } else if ([result hasPrefix:@"Password not matching"]) {
                        [self showAlert:@"Passwords are not matching."];
                    }
                }
            }
        }
        responseAsyncData = nil;
        registrationConnection = nil;
    }else if (connection == resetPasswordConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"Login: json: %@", json);
            if(json != nil) {
                NSString * result = [json objectForKey:@"data"];
                //DebugLog(@"%@",result);
                if([result isEqualToString:@"success"])
                {
                    [self showAlert:@"Password reset email sent. Please check your email for further instructions."];
                } else if ([result hasPrefix:@"Not registered"]) {
                    [self showAlert:@"User not registered."];
                } else if ([result hasPrefix:@"failed"]) {
                    [self showAlert:@"Failed to reset password. Please try again later."];
                }
            }
        }
        responseAsyncData = nil;
        resetPasswordConnection = nil;
    }else if (connection == fbUserRegistrationConnection) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"Login: fbUserRegistrationConnection json: %@", json);
            if(json != nil) {
                NSString *statusValue = [json objectForKey:@"success"];
                if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                    NSArray *dataArray = [json objectForKey:@"data"];
                     DebugLog(@"Login: dataArray %@",dataArray);
                    
                    NSDictionary* attributeDict = [dataArray objectAtIndex:0];
                    if([attributeDict valueForKey:@"ID"] != nil) {
                        DebugLog(@"Login: User exists");
                    }
                    else {
                        DebugLog(@"Login: User NOT exists");
                        [self insertUserInDB];
                    }
                }else{
                    NSString *message = [json objectForKey:@"message"];
                    DebugLog(@"Login: status false :( message %@",message);
                }
            }
        }
        fbUserRegistrationConnection = nil;
        responseAsyncData = nil;
        
    }else if (connection == insertIntoDBConnection) {
        NSString *result = [[NSString alloc] initWithBytes:[responseAsyncData mutableBytes]
                                                    length:[responseAsyncData length] encoding:NSASCIIStringEncoding];
        DebugLog(@"Login: insertIntoDBConnection res string=%@", result);
        insertIntoDBConnection = nil;
        responseAsyncData = nil;
    }else if (connection == jhalakConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"json: %@", json);
            if(json != nil) {
                NSDictionary *dictionary = nil;
                if([json valueForKey:@"success"] != nil && ([[json valueForKey:@"success"] boolValue] == 0 || [[json valueForKey:@"success"] hasPrefix:@"false"])) {
                    //DebugLog(@"NO jhalak");
                    dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:JHALAK_KEY];
                }else{
                    //DebugLog(@"got jhalak");
                    dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:JHALAK_KEY];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:JHALAK_NOTIFICATION object:nil userInfo:dictionary];
            }
        }
        jhalakConnection = nil;
        responseAsyncData = nil;
    }else if (connection == apiversionCheckConnection)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            //DebugLog(@"Login: json: %@", json);
            if(json != nil) {
                if([json valueForKey:@"success"] != nil && ([[json valueForKey:@"success"] boolValue] == 0 || [[json valueForKey:@"success"] hasPrefix:@"false"])) {
                    //DebugLog(@"Login: success false");
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                        message:@"Please update the app to proceed."
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
        }
        apiversionCheckConnection = nil;
        responseAsyncData = nil;
    }
}

#pragma UITextField delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:loginusernametextfield] || [textField isEqual:loginpasswordtextfield] ) {
        return YES;
    }else{
        if(textField.frame.origin.y > registerScrollView.contentOffset.y)
         [registerScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-60) animated:YES];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == loginusernametextfield) {
        [loginpasswordtextfield becomeFirstResponder];
	} else if (textField == loginpasswordtextfield) {
        [loginpasswordtextfield resignFirstResponder];
	} else if (textField == registeremailtextfield) {
        [registernametextfield becomeFirstResponder];
	} else if (textField == registernametextfield) {
        [registerpasstextfield becomeFirstResponder];
	} else if (textField == registerpasstextfield) {
        [registervpasstextfield becomeFirstResponder];
	} else if (textField == registervpasstextfield) {
        [registervpasstextfield resignFirstResponder];
        [self scrollTobottom:registerScrollView];
    }
   	return YES;
}

#pragma UIButton delegate Method
- (IBAction)loginProceeedBtnClicked:(id)sender {
    
    [UIView  transitionWithView:MainView duration:FLIP_DURATION  options:UIViewAnimationOptionTransitionFlipFromLeft |UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         MainView.hidden = TRUE;
                         [UIView  transitionWithView:loginView duration:3.0  options:UIViewAnimationOptionTransitionFlipFromLeft |UIViewAnimationOptionAllowUserInteraction
                                          animations:^(void) {
                                              loginView.hidden = FALSE;
                                           }
                                          completion:^(BOOL finished) {
                                          }];
    
                     }
                     completion:^(BOOL finished){
                     }];
}

- (IBAction)fbLoginBtnClicked:(id)sender {
    DebugLog(@"Login: Facebook Login");
    // The user has initiated a login, so call the openSession method
    // and show the login UX if necessary.
    //[appDelegate openSessionWithAllowLoginUI:YES];
    
    // If the user is authenticated, log out when the button is clicked.
    // If the user is not authenticated, log in when the button is clicked.
//    if (FBSession.activeSession.isOpen) {
//        [DMD_APP_DELEGATE closeSession];
//    } else {
//        // The user has initiated a login, so call the openSession method
//        // and show the login UX if necessary.
        [DMD_APP_DELEGATE openSessionWithAllowLoginUI:YES];
//    }
}

- (IBAction)loginBtnClicked:(id)sender {
    if(![loginusernametextfield.text isEqualToString:@""] || ![loginpasswordtextfield.text isEqualToString:@""])
    {
        if([DMD_APP_DELEGATE networkavailable])
        {
            [self loginAsynchronousCall:loginusernametextfield.text pass:loginpasswordtextfield.text];
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    } else {
        [self showAlert:@"Please fill all the fields."];
    }
}

- (IBAction)forgetPassBtnClicked:(id)sender {
    if(![loginusernametextfield.text isEqualToString:@""])
    {
        [Flurry logEvent:@"Forgot_Password"];
        [[LocalyticsSession shared] tagEvent:@"Forgot_Password"];
        
        if([DMD_APP_DELEGATE networkavailable])
        {
            [self resetpasswordAsynchronousCall:loginusernametextfield.text];
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    } else {
        [self showAlert:@"Please enter valid username."];
    }
}

- (IBAction)signupBtnClicked:(id)sender {
    [[LocalyticsSession shared] tagScreen:@"Sign_Up"];
    [UIView  transitionWithView:loginView duration:FLIP_DURATION  options:UIViewAnimationOptionTransitionCurlUp |UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                        loginView.hidden = TRUE;
                        registerScrollView.hidden = FALSE;
                     }
                     completion:^(BOOL finished){
                     }];
}

- (IBAction)maleBtnClicked:(id)sender {
    [maleBtn setSelected:TRUE];
    [femaleBtn setSelected:FALSE];
}

- (IBAction)femaleBtnClicked:(id)sender {
    [maleBtn setSelected:FALSE];
    [femaleBtn setSelected:TRUE];
}

- (IBAction)registerBtnClicked:(id)sender {
    if([registeremailtextfield.text isEqualToString:@""] || [registernametextfield.text isEqualToString:@""] || [registerpasstextfield.text isEqualToString:@""] || [registervpasstextfield.text isEqualToString:@""])
    {
        [self showAlert:@"Please fill all the fields."];
    }
    else if(![self validateEmail:registeremailtextfield.text])
    {
        [self showAlert:@"Invalid email address.Please check your email address and try again."];
    } else if(([registerpasstextfield.text length] < 8) || ([registervpasstextfield.text length] <8))
    {
        [self showAlert:@"Password should have minimum 8 characters."];
    } else if(![registerpasstextfield.text isEqualToString:registervpasstextfield.text])
    {
        [self showAlert:@"Passwords do not match.Please check your passwords and try again"];
    }
    else
    {
        if([DMD_APP_DELEGATE networkavailable])
        {
            [Flurry logEvent:@"Sign_Up"];
            [[LocalyticsSession shared] tagEvent:@"Sign_Up"];
            
            if([maleBtn isSelected]){
                [self registerAsynchronousCall:registeremailtextfield.text name:registernametextfield.text gendertype:@"Male" pass:registerpasstextfield.text confirmpass:registervpasstextfield.text];
            } else {
                [self registerAsynchronousCall:registeremailtextfield.text name:registernametextfield.text gendertype:@"Female" pass:registerpasstextfield.text confirmpass:registervpasstextfield.text];
            }
        } else {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    }
}

- (IBAction)cancelBtnClicked:(id)sender {
    [UIView  transitionWithView:loginView duration:FLIP_DURATION  options:UIViewAnimationOptionTransitionCurlDown |UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         loginView.hidden = FALSE;
                         registerScrollView.hidden = FALSE;
                     }
                     completion:^(BOOL finished){
                         [self clearTextFields];
                     }];
}

#pragma NSNotificationCenter Callbacks
/*
 * Configure the logged in versus logged out UI
 */
- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        DebugLog(@"Login: User Logged In");
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 DebugLog(@"user ID %@",user.id);
                 DebugLog(@"user name %@",user.first_name);
                 DebugLog(@"user middle name %@",user.middle_name);
                 DebugLog(@"user last name %@",user.last_name);
                 DebugLog(@"user link %@",user.link);
                 DebugLog(@"user name %@",user.username);
                 DebugLog(@"user email %@",[user objectForKey:@"email"]);
                 DebugLog(@"user gender %@",[user objectForKey:@"gender"]);
                 
                     NSDictionary *storeParams =
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      @"Facebook", @"Login_From",
                      nil];
                     [Flurry logEvent:@"Login" withParameters:storeParams];
                    [[LocalyticsSession shared] tagEvent:@"Login" attributes:storeParams];

                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:FACEBOOKSERVER forKey:LOGGED_BY];
                     [defaults setObject:user.id forKey:LOGIN_USERID];
                     [defaults setObject:[FBSession.activeSession.accessTokenData accessToken] forKey:LOGIN_TOKEN];
                     [defaults setObject:[NSString stringWithFormat:@"%@ %@", [user first_name],[user last_name]] forKey:LOGIN_USERNAME];
                     [defaults setObject:@"" forKey:LOGIN_PASSWORD];
                     if ([user objectForKey:@"email"] == nil) {
                         [defaults setObject:@"" forKey:LOGIN_EMAIL];

                     }else{
                         [defaults setObject:[user objectForKey:@"email"] forKey:LOGIN_EMAIL];
                     }
                     [defaults setObject:[user objectForKey:@"gender"] forKey:LOGIN_GENDER];
                     [defaults synchronize];
                 
                 if ([user objectForKey:@"email"] == nil) {
                     [self saveUserMetaDataInLocalytics:user.id name:[NSString stringWithFormat:@"%@ %@", [user first_name],[user last_name]] email:@"" gender:[user objectForKey:@"gender"]];
                 }else{
                     [self saveUserMetaDataInLocalytics:user.id name:[NSString stringWithFormat:@"%@ %@", [user first_name],[user last_name]] email:[user objectForKey:@"email"] gender:[user objectForKey:@"gender"]];
                 }
                 
                 
                     if(fbUserRegistrationConnection == nil) {
                         DebugLog(@"Login: New Connection");
                         [self registerFbUserIntoDWMServerAsynchronousCall];
                     } else {
                         DebugLog(@"Login: Already a  Connection");
                     }
                     [self cancelModalView:nil];
                     return ;
             } 
         }];
    } else {
        DebugLog(@"Login: User Logged Out");
    }
}
@end
