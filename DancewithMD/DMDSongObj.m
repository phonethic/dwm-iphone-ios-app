//
//  DMDSongObj.m
//  DancewithMD
//
//  Created by Rishi on 12/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDSongObj.h"

@implementation DMDSongObj
@synthesize songId;
@synthesize songTitle;
@synthesize songMovie;
@synthesize songChoreographer;
@synthesize songPic;
@synthesize songLock;
@synthesize songCategory;
@synthesize songCategoryName;
@synthesize isVideo;
@synthesize videoLink;
@end
