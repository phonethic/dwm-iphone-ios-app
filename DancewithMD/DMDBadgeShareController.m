//
//  DMDBadgeShareController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 21/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDBadgeShareController.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>

NSString *const kPlaceholderPostMessage = @"Say something about this...";
@implementation UITextView (DisableCopyPaste)
- (BOOL)canBecomeFirstResponder
{
    return YES;
}
@end

@interface DMDBadgeShareController ()

@end

@implementation DMDBadgeShareController
@synthesize facebookLoginBtn,cancelFBBtn;
@synthesize FBtitle,FBtCaption,FBtLink,FBtPic;
@synthesize mainFBView,userProfileImage,userNameLabel,postMessageTextView,postFBBtn,loginFBBtn,cancelButton;
@synthesize postParams;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma view lifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    
    self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"http://www.dancewithmadhuri.com", @"link",
     @"http://madhuridixit-nene.com/uploads/Madhuri_App_Icon.png", @"picture",
     @"Title of Post", @"name",
     @"", @"caption",
     @"A revolutionized way of learning dance with the Bollywood Dance Diva herself. Come Dance with Madhuri.", @"description",
     nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionStateChanged:) name:FBSessionStateChangedNotification object:nil];
    [DMD_APP_DELEGATE openSessionWithAllowLoginUI:NO];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFacebookLoginBtn:nil];
    [self setCancelFBBtn:nil];
    [self setMainFBView:nil];
    [self setCancelButton:nil];
    [self setLoginFBBtn:nil];
    [self setPostFBBtn:nil];
    [self setPostMessageTextView:nil];
    [self setUserNameLabel:nil];
    [self setUserProfileImage:nil];
    [super viewDidUnload];
}
#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Internal Callbacks
-(void)setUI{
    
    facebookLoginBtn.titleLabel.font  = KABEL_FONT(16);
    cancelFBBtn.titleLabel.font       = KABEL_FONT(18);
    
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    
    postMessageTextView.delegate =  self;
    
    postFBBtn.hidden = TRUE;
    loginFBBtn.hidden = FALSE;
    mainFBView.hidden = TRUE;
    
    UIImage *cancelButtonImage;
    cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.postFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
}


/*
 * This sets up the placeholder text.
 */
- (void)resetPostMessage
{
    self.postMessageTextView.text = kPlaceholderPostMessage;
    self.postMessageTextView.textColor = [UIColor lightGrayColor];
}

#pragma mark - UITextViewDelegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
    if ([textView.text isEqualToString:kPlaceholderPostMessage]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        [self resetPostMessage];
    }
}


#pragma UIButton delegate Method
- (IBAction)facebookLoginBtnClicked:(id)sender {
    if([DMD_APP_DELEGATE networkavailable])
    {
        [DMD_APP_DELEGATE openSessionWithAllowLoginUI:YES];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

- (IBAction)cancelFBBtnClicked:(id)sender {
    if (![[self modalViewController] isBeingDismissed])
        [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)postFBBtnClicked:(id)sender {
    if (![self.postMessageTextView.text isEqualToString:kPlaceholderPostMessage] &&
        ![self.postMessageTextView.text isEqualToString:@""]) {
        [self.postParams setObject:self.postMessageTextView.text forKey:@"message"];
    }
    
    [self.postParams setObject:self.FBtitle forKey:@"name"];
    [self.postParams setObject:self.FBtCaption forKey:@"caption"];
    [self.postParams setObject:self.FBtPic forKey:@"picture"];
    
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         NSString *alertText;
         if (error) {
             alertText = [NSString stringWithFormat:
                          @"error: domain = %@, code = %d",
                          error.domain, error.code];
         } else {
             
             alertText = @"Your message has been successfully posted on your facebook wall.";
         }
         // Show the result in an alert
         [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                     message:alertText
                                    delegate:nil
                           cancelButtonTitle:@"OK!"
                           otherButtonTitles:nil]
          show];
         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
         NSString *LoggedBy = [defaults objectForKey:LOGGED_BY];
         //         NSString *LoggedToken = [defaults objectForKey:LOGIN_TOKEN];
         
         if ([LoggedBy isEqualToString:DWMSERVER]) {
             [defaults setObject:@"" forKey:LOGIN_TOKEN];
             [defaults synchronize];
             [FBSession.activeSession closeAndClearTokenInformation];
         }else if ([LoggedBy isEqualToString:FACEBOOKSERVER]){
             DebugLog(@"User Logged by Facebook server");
         }else{
             DebugLog(@"User Logged by Other server -%@-",LoggedBy);
         }
     }];
    [self dismissModalViewControllerAnimated:YES];
}

#pragma NSNotificationCenter Callbacks
/*
 * Configure the logged in versus logged out UI
 */
- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        DebugLog(@"User Logged In");
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 //DebugLog(@"user ID %@",user.id);
                 //DebugLog(@"%@",user.name);
                 self.facebookLoginBtn.hidden = TRUE;
                 self.cancelFBBtn.hidden = TRUE;
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 postFBBtn.enabled =  TRUE;
                 postFBBtn.hidden = FALSE;
                 self.mainFBView.hidden = FALSE;
                 loginFBBtn.hidden = TRUE;
             } else {
                 postFBBtn.enabled =  FALSE;
                 postFBBtn.hidden = TRUE;
                 loginFBBtn.hidden = FALSE;
                 self.facebookLoginBtn.hidden = FALSE;
                 self.cancelFBBtn.hidden = FALSE;
                 self.mainFBView.hidden = TRUE;
             }
         }];
    }
}
@end
