//
//  DMDJhalakViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDSongObject;
@interface DMDJhalakViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    NSMutableData *responseAsyncData;
    
    DMDSongObject *songObj;
    
    NSURLConnection *jhalakSongsListConnection;
    int status;
    
    int close;
    int fullscreen;
}
@property (strong, nonatomic) NSMutableArray *jhalakSongsArray;
@property (strong, nonatomic) IBOutlet UITableView *jhalakTableView;
@end
