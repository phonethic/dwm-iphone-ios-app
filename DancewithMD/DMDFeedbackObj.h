//
//  DMDFeedbackObj.h
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDFeedbackObj : NSObject <NSCoding>

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *picture;
@property (nonatomic, copy) NSString *feedbackText;
@property (nonatomic, copy) NSString *name;

@end
