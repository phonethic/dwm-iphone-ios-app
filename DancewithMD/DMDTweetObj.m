//
//  DMDTweetObj.m
//  DancewithMD
//
//  Created by Rishi on 25/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDTweetObj.h"

@implementation DMDTweetObj

@synthesize fromuserName;
@synthesize imageUrl;
@synthesize tweetText;

#pragma mark NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.fromuserName forKey:@"fromuserName"];
    [coder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [coder encodeObject:self.tweetText forKey:@"tweetText"];
}

- (id)initWithCoder:(NSCoder *)coder {
	self = [super init];
	if (self != nil) {
        self.fromuserName   = [coder decodeObjectForKey:@"fromuserName"];
        self.imageUrl       = [coder decodeObjectForKey:@"imageUrl"];
        self.tweetText      = [coder decodeObjectForKey:@"tweetText"];
	}
	return self;
}
@end
