//
//  DMDLoginViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMDLoginViewController : UIViewController
{
    int status;
    NSMutableData *responseAsyncData;
    
    NSURLConnection *loginConnection;
    NSURLConnection *registrationConnection;
    NSURLConnection *userDetailConnection;
    NSURLConnection *resetPasswordConnection;
    
    NSURLConnection *jhalakConnection;
    NSURLConnection *apiversionCheckConnection;

    NSURLConnection *fbUserRegistrationConnection;
    NSURLConnection *insertIntoDBConnection;
}
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

@property (strong, nonatomic) IBOutlet UIView *MainView;
@property (strong, nonatomic) IBOutlet UIButton *loginProceeedBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *registerScrollView;

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UITextField *loginusernametextfield;
@property (strong, nonatomic) IBOutlet UITextField *loginpasswordtextfield;

@property (strong, nonatomic) IBOutlet UIButton *fbLoginBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblOR;


@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *forgetPassBtn;
@property (strong, nonatomic) IBOutlet UIButton *signupBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblsignUp;

@property (strong, nonatomic) IBOutlet UIView *registerView;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *maleBtn;
@property (strong, nonatomic) IBOutlet UIButton *femaleBtn;
@property (strong, nonatomic) IBOutlet UITextField *registeremailtextfield;
@property (strong, nonatomic) IBOutlet UITextField *registernametextfield;
@property (strong, nonatomic) IBOutlet UITextField *registerpasstextfield;
@property (strong, nonatomic) IBOutlet UITextField *registervpasstextfield;

- (IBAction)loginProceeedBtnClicked:(id)sender;

- (IBAction)fbLoginBtnClicked:(id)sender;

- (IBAction)loginBtnClicked:(id)sender;
- (IBAction)forgetPassBtnClicked:(id)sender;
- (IBAction)signupBtnClicked:(id)sender;

- (IBAction)maleBtnClicked:(id)sender;
- (IBAction)femaleBtnClicked:(id)sender;

- (IBAction)registerBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;

@end
