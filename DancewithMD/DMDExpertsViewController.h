//
//  DMDExpertsViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MediaPlayer/MediaPlayer.h>

@class DMDExpertsObj;
@interface DMDExpertsViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>
{
    int status;
    int small;
    int fullscreen;
    NSMutableData *responseAsyncData;
    NSURLConnection *expertConnection;
    DMDExpertsObj *expvideosObj;
}

@property (strong, nonatomic) NSMutableArray *videoDetailArray;

@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIView *descriptionView;
@property (strong, nonatomic) IBOutlet UIImageView *descriptionImageView;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIButton *closeVideoBtn;
- (IBAction)closeVideoBtnClicked:(id)sender;
@end
