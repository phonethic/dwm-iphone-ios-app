//
//  DMDJhalakViewController.m
//  DancewithMD
//
//  Created by Kirti Nikam on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "DMDJhalakViewController.h"
#import "MFSideMenu.h"
#import "constants.h"
#import "DMDAppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DMDSongObject.h"
#import "DMDJhalakDetailViewController.h"
#import "CommonCallBack.h"

#define SONG_LIST_LINK [NSString stringWithFormat:@"%@/api/retrieve/songinfo/5",LIVE_DWM_SERVER]

@interface DMDJhalakViewController ()

@end

@implementation DMDJhalakViewController
@synthesize jhalakSongsArray;
@synthesize jhalakTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma view lifeCycle
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [Flurry logEvent:@"Tab_Jhalak"];
    [[LocalyticsSession shared] tagScreen:@"Jhalak"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Set NavBarButtonItems
    [self setupMenuBarButtonItems];
    if([DMD_APP_DELEGATE networkavailable])
    {
        [jhalakTableView setHidden:YES];
        [self jhalakSongsListAsynchronousCall];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallBack hideProgressHudWithError];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    if (jhalakSongsListConnection != nil) {
        [jhalakSongsListConnection cancel];
        jhalakSongsListConnection = nil;
    }
}

- (void)viewDidUnload {
    [self setJhalakTableView:nil];
    [super viewDidUnload];
}

#pragma orientation CallBacks
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}


#pragma Internal Callbacks
-(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         if (jhalakSongsListConnection != nil) {
                             [jhalakSongsListConnection cancel];
                             jhalakSongsListConnection = nil;
                         }
                     }];
}

-(void)jhalakSongsListAsynchronousCall
{
    [self showProgressHudWithCancel:@"Jhalak" subtitle:@"Loading"];
	/****************Asynchronous Request**********************/
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:SONG_LIST_LINK] cachePolicy:NO timeoutInterval:15.0];
	jhalakSongsListConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //DebugLog(@"JhalakView: Error: %@", [error localizedDescription]);
    [CommonCallBack hideProgressHudWithError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [CommonCallBack hideProgressHud];
	if(responseAsyncData != nil)
	{
		//NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		////DebugLog(@"JhalakView: \n result:%@\n\n", result);
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
        DebugLog(@"JhalakView: json: %@", json);
        if(json != nil) {
            NSString *statusValue = [json objectForKey:@"success"];
            if ([statusValue boolValue] == 1 || [statusValue hasPrefix:@"true"]) {
                NSArray *dataArray = [json objectForKey:@"data"];
                if (jhalakSongsArray == nil) {
                    jhalakSongsArray = [[NSMutableArray alloc] init];
                }else{
                    [jhalakSongsArray removeAllObjects];
                }
                for (NSDictionary* songattributeDict in dataArray) {
                    songObj = [[DMDSongObject alloc] init];
                    songObj.songId = [songattributeDict objectForKey:@"Song_ID"];
                    songObj.songTitle = [songattributeDict objectForKey:@"Title"];
                    songObj.songMovie = [songattributeDict objectForKey:@"Movie"];
                    songObj.songChoreographer = [songattributeDict objectForKey:@"Choreographer"];
                    songObj.songPic = [songattributeDict objectForKey:@"Thumbnail"];
                    songObj.songLock = [songattributeDict objectForKey:@"Lock_Type"];
                    songObj.songCategory = [songattributeDict objectForKey:@"Category"];
                    songObj.songCategoryName = [songattributeDict objectForKey:@"Category_Name"];
                    ////DebugLog(@"JhalakView: \nsongid: %@ , title: %@  , movie: %@  , choreographer: %@  , style: %@  , picture: %@\n", songObj.songId, songObj.songTitle, songObj.songMovie, songObj.songChoreographer, songObj.songStyle, songObj.songPic);
                    [jhalakSongsArray addObject:songObj];
                    songObj = nil;
                }
            }
        }
	}
    jhalakSongsListConnection = nil;
    responseAsyncData = nil;
    DebugLog(@"JhalakView: array count: %d", [jhalakSongsArray count]);
    [jhalakTableView setHidden:NO];
    [jhalakTableView reloadData];
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 97;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [jhalakSongsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UILabel *lblSongTitle;
    UILabel *lblSong;
    UILabel *lblMovieTitle;
    UILabel *lblMovie;
    UILabel *lblChoreoTitle;
    UILabel *lblChoreographer;
    UILabel *lblCredits;
    UIView *swipeView;
    
    static NSString *CellIdentifier = @"jhalakCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 97, 89)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.masksToBounds = YES;
        [cell.contentView addSubview:thumbImg];
        
//        lblSongTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+58,27,115,15)];
//        lblSongTitle.text = @"dance style";
//        lblSongTitle.tag = 111;
//        lblSongTitle.font = KABEL_FONT(12);
//        lblSongTitle.textAlignment = UITextAlignmentLeft;
//        lblSongTitle.textColor = [UIColor whiteColor];
//        lblSongTitle.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblSongTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblSong = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+56,20,115,60)];
        lblSong.tag = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblSong.numberOfLines = 4;
        lblSong.font = KABEL_FONT(18);
        lblSong.adjustsFontSizeToFitWidth = YES;
        lblSong.textAlignment = UITextAlignmentLeft;
        lblSong.textColor = [UIColor whiteColor];
        lblSong.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblSong];
        
        
        // Create the swipe view
        swipeView = [[UIView alloc] initWithFrame:CGRectMake(288,3,0, 91)];
        swipeView.tag = 50;
        swipeView.clipsToBounds = TRUE;
        //swipeView.alpha = 0.3;
        swipeView.userInteractionEnabled = TRUE;
        swipeView.backgroundColor = [UIColor clearColor];
        //[swipeView setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"song_credits_bg.png"]]];
        //[swipeView setBackgroundColor:[UIColor colorWithRed:0.30 green:0.27 blue:0.23 alpha:0.7]];
        
        UIButton *bgbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgbutton setFrame:CGRectMake(0, 0,140, 91)];
        [bgbutton setAlpha:1];
        [bgbutton setTitle:@"" forState:UIControlStateNormal];
        [bgbutton setTitle:@"" forState:UIControlStateHighlighted];
        [bgbutton setBackgroundImage:[UIImage imageNamed:@"song_credits_bg.png"] forState:UIControlStateNormal];
        [bgbutton setBackgroundImage:[UIImage imageNamed:@"song_credits_bg.png"] forState:UIControlStateHighlighted];
        [bgbutton addTarget:self action:@selector(songcCreditsBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [swipeView addSubview:bgbutton];
        
        lblMovieTitle = [[UILabel alloc] initWithFrame:CGRectMake(10.0,5.0,100.0,10.0)];
        lblMovieTitle.text = @"Movie";
        lblMovieTitle.tag = 222;
        lblMovieTitle.font = KABEL_FONT(10);
        lblMovieTitle.textAlignment = UITextAlignmentLeft;
        lblMovieTitle.textColor = GRAY_TEXT_COLOR;
        lblMovieTitle.backgroundColor =  [UIColor clearColor];
        [swipeView addSubview:lblMovieTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblMovie = [[UILabel alloc] initWithFrame:CGRectMake(10.0,15.0,150.0,20.0)];
        lblMovie.tag = 3;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblMovie.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblMovie.font = KABEL_FONT(12);
        lblMovie.adjustsFontSizeToFitWidth = YES;
        lblMovie.textAlignment = UITextAlignmentLeft;
        lblMovie.textColor = GRAY_TEXT_COLOR;
        lblMovie.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblMovie];
        [swipeView addSubview:lblMovie];
        
        
        lblChoreoTitle = [[UILabel alloc] initWithFrame:CGRectMake(10.0,40,100,10.0)];
        lblChoreoTitle.text = @"Choreographer";
        lblChoreoTitle.tag = 333;
        lblChoreoTitle.font = KABEL_FONT(10);
        lblChoreoTitle.adjustsFontSizeToFitWidth = YES;
        lblChoreoTitle.textAlignment = UITextAlignmentLeft;
        lblChoreoTitle.textColor = GRAY_TEXT_COLOR;
        lblChoreoTitle.backgroundColor =  [UIColor clearColor];
        [swipeView addSubview:lblChoreoTitle];
        
        //Initialize Label with tag 2.(Title Label)
        lblChoreographer = [[UILabel alloc] initWithFrame:CGRectMake(10.0,45,150,20.0)];
        lblChoreographer.tag = 4;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblChoreographer.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblChoreographer.font = KABEL_FONT(12);
        lblChoreographer.adjustsFontSizeToFitWidth = YES;
        lblChoreographer.textAlignment = UITextAlignmentLeft;
        lblChoreographer.textColor = GRAY_TEXT_COLOR;
        lblChoreographer.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblChoreographer];
        [swipeView addSubview:lblChoreographer];
        
        [cell.contentView addSubview:swipeView];
        
        
        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
        UIImage *image1 = [UIImage imageNamed:@"song_credits_on.png"];
        //DebugLog(@"JhalakView: %f %f",cell.frame.size.width , image.size.width);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(288,3,30, 91);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 5;
        [button setAlpha:0.8];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setBackgroundImage:image1 forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(songcCreditsBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button];
        
        
        //Initialize Label with tag 2.(Title Label)
        lblCredits = [[UILabel alloc] initWithFrame:CGRectMake(255,30,91,30.0)];
        lblCredits.tag = 6;
        //lblTitle.shadowColor   = [UIColor blackColor];
        //lblChoreographer.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblCredits.text = @"Song Credits";
        lblCredits.font = KABEL_FONT(15);
        lblCredits.adjustsFontSizeToFitWidth = YES;
        lblCredits.textAlignment = UITextAlignmentCenter;
        lblCredits.textColor = GRAY_TEXT_COLOR;
        lblCredits.backgroundColor =  [UIColor clearColor];
        lblCredits.transform = CGAffineTransformMakeRotation(M_PI/-2);
        [cell.contentView addSubview:lblCredits];
        
        UIImageView *arrowImg = [[UIImageView alloc] init];
        arrowImg.image = [UIImage imageNamed:@"arrow.png"];
        arrowImg.contentMode = UIViewContentModeScaleAspectFit;
        arrowImg.tag = 5;
        arrowImg.frame = CGRectMake(270,44,15,17);
        [cell.contentView addSubview:arrowImg];
        
    }
    DMDSongObject *tempsongObj  = [jhalakSongsArray objectAtIndex:indexPath.row];
    lblSong = (UILabel *)[cell viewWithTag:2];
    lblMovie = (UILabel *)[cell viewWithTag:3];
    lblChoreographer = (UILabel *)[cell viewWithTag:4];
    lblSong.text = [NSString stringWithFormat:@"%@",tempsongObj.songTitle];
    
    
    lblMovie.text = [NSString stringWithFormat:@"%@",tempsongObj.songMovie];
    lblChoreographer.text = [NSString stringWithFormat:@"%@",tempsongObj.songChoreographer];
    
    
    //DebugLog(@"JhalakView: pic=====%@",[DMDDelegate URLEncode:tempsongObj.songPic]);
    UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:1];
    [thumbImgview setImageWithURL:[NSURL URLWithString:[DMD_APP_DELEGATE URLEncode:tempsongObj.songPic]]
                 placeholderImage:nil
                          success:^(UIImage *image) {
                              //DebugLog(@"success");
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                          }];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
    bg.image = [UIImage imageNamed:@"tab_off.png"];
    bgon.image = [UIImage imageNamed:@"tab_on.png"];
    cell.backgroundView = bg;
    cell.selectedBackgroundView = bgon;
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  = (UITableViewCell *)[self.jhalakTableView cellForRowAtIndexPath:indexPath];
    UIView *swipeView = (UIView *)[cell viewWithTag:50];
    UIButton *button = (UIButton *)[cell viewWithTag:5];
    UILabel *label = (UILabel *)[cell viewWithTag:6];
    UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
    [swipeView setFrame:CGRectMake(288, 3, 0, 91)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:image forState:UIControlStateHighlighted];
    [label setHidden:FALSE];
    DMDSongObject *tempsongObj  = [jhalakSongsArray objectAtIndex:indexPath.row];
    DMDJhalakDetailViewController *typedetailController = [[DMDJhalakDetailViewController alloc] initWithNibName:@"DMDJhalakDetailViewController" bundle:nil] ;
    //DebugLog(@"JhalakView: %@  !!  %@" , tempsongObj.songTitle, tempsongObj.songId);
    typedetailController.title = tempsongObj.songTitle;
    typedetailController.songID = tempsongObj.songId;
    typedetailController.songLockType = tempsongObj.songLock;
    typedetailController.songName = tempsongObj.songTitle;
    [self.navigationController pushViewController:typedetailController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

#pragma UIButton delegate Methods

- (void)songcCreditsBtnTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.jhalakTableView];
	NSIndexPath *indexPath = [self.jhalakTableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.jhalakTableView cellForRowAtIndexPath:indexPath];
        UIView *swipeView = (UIView *)[cell viewWithTag:50];
        UIButton *button = (UIButton *)[cell viewWithTag:5];
        UILabel *label = (UILabel *)[cell viewWithTag:6];
        UIImage *image = [UIImage imageNamed:@"song_credits_off.png"];
        UIImage *image1 = [UIImage imageNamed:@"song_credits_on.png"];
        if(swipeView.frame.origin.x > 148) {
            [button setBackgroundImage:image1 forState:UIControlStateNormal];
            [button setBackgroundImage:image1 forState:UIControlStateHighlighted];
            [label setHidden:TRUE];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            // Swipe top view left
            [UIView animateWithDuration:1.0 animations:^{
                [swipeView setFrame:CGRectMake(148, 3,140, 91)]; } completion:^(BOOL finished) {
                }];
        } else {
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            // Swipe top view right
            [UIView animateWithDuration:1.0 animations:^{
                [swipeView setFrame:CGRectMake(288, 3, 0, 91)]; } completion:^(BOOL finished) {
                    [UIView  transitionWithView:button duration:0.5
                                        options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
                                     animations:^(void) {
                                         [button setBackgroundImage:image forState:UIControlStateNormal];
                                         [button setBackgroundImage:image forState:UIControlStateHighlighted];
                                         [UIView  transitionWithView:label duration:1.0
                                                             options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction
                                                          animations:^(void) {
                                                              [label setHidden:FALSE];
                                                          }
                                                          completion:^(BOOL finished) {
                                                          }];
                                     }
                                     completion:^(BOOL finished) {
                                     }];
                }];
        }
    }
}

@end
