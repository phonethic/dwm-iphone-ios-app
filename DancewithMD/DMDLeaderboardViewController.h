//
//  DMDLeaderboardViewController.h
//  DancewithMD
//
//  Created by Kirti Nikam on 07/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMDLeaderboardObj;
@interface DMDLeaderboardViewController : UIViewController
{
    int status;
    NSMutableData *responseAsyncData;
    NSURLConnection *leaderBoardConnection;

    DMDLeaderboardObj *boardObj;
}

@property (copy, nonatomic) NSString *dancersString;
@property (copy, nonatomic) NSString *rankString;
@property (copy, nonatomic) NSString *pointsString;
@property (strong, nonatomic) NSMutableArray *leaderboardArray;

@property (strong, nonatomic) IBOutlet UILabel *userNamelbl;
@property (strong, nonatomic) IBOutlet UILabel *dancerslbl;
@property (strong, nonatomic) IBOutlet UILabel *ranklbl;
@property (strong, nonatomic) IBOutlet UILabel *pointslbl;

@property (strong, nonatomic) IBOutlet UITableView *leaderBoardTableView;

@property (strong, nonatomic) IBOutlet UIImageView *dancerslblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *ranklblBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *pointslblBackImageView;


@end
